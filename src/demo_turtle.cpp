
#include <Slum/Application.h>
#include <Slum/Configuration.h>
#include <Slum/UI/ImGuiLayer.h>
#include <Slum/Demo/Turtle/TurtleDemo.h>

#include <cstdio>
#include <stdexcept>

int main(int argc, const char** argv) {
  try {
    Slum::Configuration config(argc, argv);
    Slum::Application app(config, "slum - turtle demo");
    Slum::UI::ImGuiLayer gui(app);
    Slum::Demo::Turtle::TurtleDemo demo(app);
    app.run();
    return 0;
  } catch (const std::exception& e) {
    fprintf(stderr, "%s\n", e.what());
    return 1;
  }
}
