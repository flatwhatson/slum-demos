
#include <Slum/Application.h>
#include <Slum/Configuration.h>
#include <Slum/UI/ImGuiLayer.h>
#include <Slum/Demo/Noise/NoiseDemo.h>

#include <cstdio>
#include <stdexcept>

int main(int argc, const char** argv) {
  try {
    Slum::Configuration config(argc, argv);
    Slum::Application app(config, "slum - noise demo");
    Slum::UI::ImGuiLayer gui(app);
    Slum::Demo::Noise::NoiseDemo demo(app);
    app.run();
    return 0;
  } catch (const std::exception& e) {
    fprintf(stderr, "%s\n", e.what());
    return 1;
  }
}
