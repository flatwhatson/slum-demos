#pragma once

#include <boost/simd/config.hpp>
#include <boost/simd/range/segmented_aligned_range.hpp>

#include <utility>

namespace boost { namespace simd {

template<class Iterator, size_t N, class UnaryOp>
BOOST_FORCEINLINE
void for_each(const segmented_aligned_range_t<Iterator, N>& range, const UnaryOp& func) {
  for (auto value : range.head) func(value);
  for (auto value : range.body) func(value);
  for (auto value : range.tail) func(value);
}

template<class Range, class UnaryOp,
         class = segmented_aligned_range_t<detail::range_iterator<Range>>>
BOOST_FORCEINLINE
void for_each(Range&& range, const UnaryOp& func) {
  for_each(segmented_aligned_range(std::forward<Range>(range)), func);
}

template<class Iterator, class UnaryOp,
         class = segmented_aligned_range_t<Iterator>>
BOOST_FORCEINLINE
void for_each(Iterator first, Iterator last, const UnaryOp& func) {
  for_each(segmented_aligned_range(first, last), func);
}

}} // namespace boost::simd
