#pragma once

#include "../external/boost.simd/include/boost/simd/algorithm/transform.hpp"

#include <boost/simd/config.hpp>
#include <boost/simd/detail/is_aligned.hpp>
#include <boost/simd/function/aligned_store.hpp>
#include <boost/simd/function/store.hpp>
#include <boost/simd/range/segmented_aligned_range.hpp>

#include <iterator>
#include <type_traits>
#include <utility>

namespace boost { namespace simd {

template<class Iterator, class Out, size_t N, class UnaryOp>
BOOST_FORCEINLINE
Out* transform(const segmented_aligned_range_t<Iterator, N>& range, Out* out, const UnaryOp& func) {
  using In = typename std::iterator_traits<Iterator>::value_type;
  using vIn = pack<In, N>;
  using vOut = pack<Out>;

  static_assert(cardinal_of<vIn>::value == cardinal_of<vOut>::value,
                "SIMD cardinal mismatch between In and Out");

  for (In value : range.head) {
    *out++ = func(value);
  }

  if (detail::is_aligned(out, vOut::alignment)) {
    for (vIn values : range.body) {
      aligned_store(func(values), out);
      out += vOut::static_size;
    }
  } else {
    for (vIn values : range.body) {
      store(func(values), out);
      out += vOut::static_size;
    }
  }

  for (In value : range.tail) {
    *out++ = func(value);
  }

  return out;
}

template<class Container, class UnaryOp,
         class Out = std::decay_t<decltype(*std::declval<Container>().data())>,
         class DataT_ = decltype(std::declval<Container>().data()),
         class SizeT_ = decltype(std::declval<Container>().size()),
         class = std::enable_if_t< std::is_pointer<DataT_>::value && std::is_integral<SizeT_>::value >>
BOOST_FORCEINLINE
Out* transform(Container& cont, const UnaryOp& func) {
  return transform(segmented_aligned_range(cont.data(), cont.data() + cont.size()), cont.data(), func);
}

template<class Range, class Out, class UnaryOp,
         class = segmented_aligned_range_t<detail::range_iterator<Range>>>
BOOST_FORCEINLINE
Out* transform(Range&& range, Out* out, const UnaryOp& func) {
  return transform(segmented_aligned_range(std::forward<Range>(range)), out, func);
}

template<class Iterator, class Out, class UnaryOp,
         class = segmented_aligned_range_t<Iterator>>
BOOST_FORCEINLINE
Out* transform(Iterator first, Iterator last, Out* out, const UnaryOp& func) {
  return transform(segmented_aligned_range(first, last), out, func);
}

}} // namespace boost::simd
