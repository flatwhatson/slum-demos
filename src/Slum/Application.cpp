#include "Application.h"
#include "Configuration.h"

#include "GL/API.h"
#include "GL/Context.h"
#include "Layer.h"

#include <algorithm>
#include <cstdio>
#include <stdexcept>

#include <SDL.h>

namespace Slum {

Application::Application(Configuration& config, const std::string& title)
    : config_(config), title_(title) {

  if (SDL_Init(SDL_INIT_EVERYTHING) != 0)
    throw std::runtime_error(SDL_GetError());

  Uint32 flags = SDL_WINDOW_OPENGL;
  if (config_.fullscreen()) flags |= SDL_WINDOW_FULLSCREEN;
  if (config_.borderless()) flags |= SDL_WINDOW_BORDERLESS;
  if (config_.resizeable()) flags |= SDL_WINDOW_RESIZABLE;

  window_ = SDL_CreateWindow(
    title.c_str(),
    SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED,
    config_.screenWidth(), config_.screenHeight(), flags);

  if (!window_)
    throw std::runtime_error(SDL_GetError());

  SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
  SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 3);
  SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);

  context_ = SDL_GL_CreateContext(window_);
  if (!context_)
    throw std::runtime_error(SDL_GetError());

  glewExperimental = GL_TRUE;
  GLenum glewStatus = glewInit();
  if (glewStatus != 0)
    throw std::runtime_error((const char*)glewGetErrorString(glewStatus));

  glClearColor(114/255.0f, 144/255.0f, 154/255.0f, 0.0f);
}

Application::~Application() {
  layers_.clear();
  SDL_GL_DeleteContext(context_);
  SDL_DestroyWindow(window_);
  SDL_Quit();
}

void Application::setTitle(const std::string& title) {
  title_ = title;
  SDL_SetWindowTitle(window_, title.c_str());
}

void Application::setConfig(const Configuration& config) {
  if (!config.fullscreen())
    SDL_SetWindowFullscreen(window_, 0);

  SDL_SetWindowSize(window_, config.screenWidth(), config.screenHeight());
  SDL_SetWindowBordered(window_, config.borderless() ? SDL_FALSE : SDL_TRUE);

  if (config.fullscreen())
    SDL_SetWindowFullscreen(window_, SDL_WINDOW_FULLSCREEN);

  config_ = config;
}

void Application::toggleFullscreen() {
  if (config_.fullscreen()) {
    config_.setFullscreen(false);
    SDL_SetWindowFullscreen(window_, 0);
    SDL_SetWindowSize(window_, lastWindowWidth_, lastWindowHeight_);
    SDL_SetWindowPosition(window_, lastWindowPosX_, lastWindowPosY_);
  } else {
    config_.setFullscreen(true);
    SDL_GetWindowSize(window_, &lastWindowWidth_, &lastWindowHeight_);
    SDL_GetWindowPosition(window_, &lastWindowPosX_, &lastWindowPosY_);
    SDL_SetWindowSize(window_, config_.screenWidth(), config_.screenHeight());
    SDL_SetWindowFullscreen(window_, SDL_WINDOW_FULLSCREEN);
  }
}

void Application::run() {
  if (running_) return;
  running_ = true;

  int lastReport = SDL_GetTicks();
  int frameCount = 0;

  while (running_) {
    int frameStart = SDL_GetTicks();

    processEvents();
    if (!running_) break;

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);

    for (auto layer : layers_)
      layer->prepare();

    for (auto it = rbegin(layers_); it != rend(layers_); ++it)
      (*it)->render();

    SDL_GL_SwapWindow(window_);

    int frameTicks = SDL_GetTicks() - frameStart;
    int minFrameTicks = config_.frameTicks();
    if (frameTicks < minFrameTicks)
      SDL_Delay(minFrameTicks - frameTicks);

    ++frameCount;
    int now = SDL_GetTicks();
    int ticks = now - lastReport;
    if (ticks >= 1000) {
      logFrameRate(1000.0f*frameCount/ticks);
      lastReport = now;
      frameCount = 0;
    }
  }
}

void Application::addLayer(Layer* layer) {
  layers_.push_back(layer);
}

void Application::removeLayer(Layer* layer) {
  auto found = find(begin(layers_), end(layers_), layer);
  if (found != end(layers_)) layers_.erase(found);
}

void Application::processEvents() {
  SDL_Event event;
  while (running_ && SDL_PollEvent(&event)) {
    switch (event.type) {
      case SDL_WINDOWEVENT:
        switch (event.window.event) {
          case SDL_WINDOWEVENT_SIZE_CHANGED: onResize(event); break;
        }
        break;
      case SDL_TEXTINPUT:       onTextInput(event);    break;
      case SDL_KEYDOWN:         onKeyPress(event);     break;
      case SDL_KEYUP:           onKeyRelease(event);   break;
      case SDL_MOUSEBUTTONDOWN: onMousePress(event);   break;
      case SDL_MOUSEBUTTONUP:   onMouseRelease(event); break;
      case SDL_MOUSEWHEEL:      onMouseScroll(event);  break;
      case SDL_MOUSEMOTION:     onMouseMove(event);    break;
      case SDL_QUIT:            running_ = false;      break;
    }
  }
}

void Application::onResize(SDL_Event& event) {
  glViewport(0, 0, event.window.data1, event.window.data2);
  for (auto layer : layers_)
    layer->onResize(event.window.data1, event.window.data2);
}

void Application::onTextInput(SDL_Event& event) {
  bool captured = false;
  for (auto layer : layers_)
    layer->onTextInput(event.text.text, captured);
}

void Application::onKeyPress(SDL_Event& event) {
  int key = event.key.keysym.sym & ~SDLK_SCANCODE_MASK;
  int mod = SDL_GetModState();
  bool captured = false;
  if (key == SDLK_RETURN && mod & KMOD_ALT) {
    toggleFullscreen();
    captured = true;
  }
  for (auto layer : layers_)
    layer->onKeyPress(key, mod, captured);
}

void Application::onKeyRelease(SDL_Event& event) {
  int key = event.key.keysym.sym & ~SDLK_SCANCODE_MASK;
  int mod = SDL_GetModState();
  bool captured = false;
  for (auto layer : layers_)
    layer->onKeyRelease(key, mod, captured);
}

void Application::onMousePress(SDL_Event& event) {
  bool captured = false;
  for (auto layer : layers_)
    layer->onMousePress(event.button.button, captured);
}

void Application::onMouseRelease(SDL_Event& event) {
  bool captured = false;
  for (auto layer : layers_)
    layer->onMouseRelease(event.button.button, captured);
}

void Application::onMouseScroll(SDL_Event& event) {
  bool captured = false;
  for (auto layer : layers_)
    layer->onMouseScroll(event.wheel.x, event.wheel.y, captured);
}

void Application::onMouseMove(SDL_Event& event) {
  bool captured = false;
  for (auto layer : layers_)
    layer->onMouseMove(event.motion.x, event.motion.y, captured);
}

void Application::logFrameRate(float fps) {
  fprintf(stderr, "FPS: %.3f/s (%.3fms)\n", fps, 1000/fps);
}

} // namespace Slum
