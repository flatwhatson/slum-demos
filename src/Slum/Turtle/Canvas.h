#pragma once

#include "XiaolinWuLine.h"

#include <cassert>
#include <cmath>
#include <vector>

namespace {
const float PI = std::acos(-1);
const float DEG2RAD = PI / 180.0f;
}

namespace Slum { namespace Turtle {

class Canvas {
 public:
  Canvas(int width, int height)
      : width_(width), height_(height),
        pixels_(width * height, 1.0f),
        positionX_(width / 2.0f),
        positionY_(height / 2.0f),
        direction_(270.0f) {}

  int width() const { return width_; }
  int height() const { return height_; }
  size_t size() const { return pixels_.size(); }
  const float* data() const { return pixels_.data(); }
  float positionX() const { return positionX_; }
  float positionY() const { return positionY_; }
  float direction() const { return direction_; }

  float at(int x, int y) const {
    assert(x >= 0);
    assert(x < width_);
    assert(y >= 0);
    assert(y < height_);

    return pixels_[x + y * width_];
  }

  float& at(int x, int y) {
    assert(x >= 0);
    assert(x < width_);
    assert(y >= 0);
    assert(y < height_);

    return pixels_[x + y * width_];
  }

  void clear() {
    std::fill(begin(pixels_), end(pixels_), 1.0f);
  }

  void warp(float x, float y) {
    setPositionX(x);
    setPositionY(y);
  }

  void warp(float x, float y, float d) {
    setPositionX(x);
    setPositionY(y);
    setDirection(d);
  }

  void move(float distance) {
    float radians = direction_ * DEG2RAD;
    setPositionX(positionX_ + distance * std::cos(radians));
    setPositionY(positionY_ + distance * std::sin(radians));
  }

  void draw(float distance) {
    float previousX = positionX_;
    float previousY = positionY_;
    move(distance);

    XiaolinWuLine(previousX, previousY, positionX_, positionY_,
                  [&](int x, int y, float c) {
                    at(x, y) = 1.0f - c;
                  });
  }

  void turn(float degrees) {
    setDirection(direction_ + degrees);
  }

 private:

  void setPositionX(float x) {
    if (x < 0.0)
      x = 0.0f;
    else if (x >= width_)
      x = width_ - 1.0f;

    positionX_ = x;
  }

  void setPositionY(float y) {
    if (y < 0.0f)
      y = 0.0f;
    else if (y >= height_)
      y = height_ - 1.0f;

    positionY_ = y;
  }

  void setDirection(float d) {
    while (d < 0.0f)
      d += 360.0f;
    while (d >= 360.0f)
      d -= 360.0f;

    direction_ = d;
  }

 private:
  int width_;
  int height_;
  std::vector<float> pixels_;

  float positionX_;
  float positionY_;
  float direction_;
};

}} // namsepace Slum::Turtle
