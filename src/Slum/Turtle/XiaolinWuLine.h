#pragma once

#include <algorithm>
#include <cmath>
#include <functional>

namespace Slum { namespace Turtle {

inline void XiaolinWuLine(float x0, float y0, float x1, float y1, const std::function<void(int,int,float)>& plot) {
  auto ipart  = [ ](const float n) { return std::floor(n);     };
  auto round  = [&](const float n) { return ipart(n + 0.5);    };
  auto fpart  = [ ](const float n) { return n - std::floor(n); };
  auto rfpart = [&](const float n) { return 1 - fpart(n);      };

  bool steep = std::abs(y1 - y0) > std::abs(x1 - x0);

  if (steep) {
    std::swap(x0, y0);
    std::swap(x1, y1);
  }
  if (x0 > x1) {
    std::swap(x0, x1);
    std::swap(y0, y1);
  }

  float dx = x1 - x0;
  float dy = y1 - y0;
  float gradient = dx == 0 ? 1 : dy / dx;

  float xend, yend, xgap;
  int xpxl1, ypxl1;
  int xpxl2, ypxl2;
  float intery;

  // handle first endpoint
  xend = round(x0);
  yend = y0 + gradient * (xend - x0);
  xgap = rfpart(x0 + 0.5);
  xpxl1 = xend; // this will be used in the main loop
  ypxl1 = ipart(yend);
  if (steep) {
    plot(ypxl1,   xpxl1, rfpart(yend) * xgap);
    plot(ypxl1+1, xpxl1,  fpart(yend) * xgap);
  } else {
    plot(xpxl1, ypxl1,  rfpart(yend) * xgap);
    plot(xpxl1, ypxl1+1, fpart(yend) * xgap);
  }
  intery = yend + gradient; // first y-intersection for the main loop

  // handle second endpoint
  xend = round(x1);
  yend = y1 + gradient * (xend - x1);
  xgap = fpart(x1 + 0.5);
  xpxl2 = xend; //this will be used in the main loop
  ypxl2 = ipart(yend);
  if (steep) {
    plot(ypxl2,   xpxl2, rfpart(yend) * xgap);
    plot(ypxl2+1, xpxl2,  fpart(yend) * xgap);
  } else {
    plot(xpxl2, ypxl2,  rfpart(yend) * xgap);
    plot(xpxl2, ypxl2+1, fpart(yend) * xgap);
  }

  // main loop
  if (steep) {
    for (int x = xpxl1 + 1; x < xpxl2; ++x) {
      plot(ipart(intery),   x, rfpart(intery));
      plot(ipart(intery)+1, x,  fpart(intery));
      intery = intery + gradient;
    }
  } else {
    for (int x = xpxl1 + 1; x < xpxl2; ++x) {
      plot(x, ipart(intery),  rfpart(intery));
      plot(x, ipart(intery)+1, fpart(intery));
      intery = intery + gradient;
    }
  }
}

}} // namespace Slum::Turtle
