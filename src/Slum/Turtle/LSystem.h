#pragma once

#include <functional>
#include <sstream>
#include <unordered_map>

namespace Slum { namespace Turtle {

class LSystem {
 public:

  LSystem() = default;

  LSystem(
      std::unordered_map<char, std::string> mapping,
      std::unordered_map<char, std::function<void()>> actions)
      : map_(std::move(mapping)),
        act_(std::move(actions)) {}

  std::string iterate(const std::string& string, int n = 1) const {
    if (n < 1)
      return string;

    std::stringstream ss;
    for (char c : string) {
      auto mapping = map_.find(c);
      if (mapping != map_.end()) {
        ss << mapping->second;
      } else {
        ss << c;
      }
    }

    return iterate(ss.str(), n - 1);
  }

  void interpret(const std::string& string) const {
    for (char c : string) {
      auto action = act_.find(c);
      if (action != act_.end()) {
        action->second();
      }
    }
  }

 private:
  std::unordered_map<char, std::string> map_;
  std::unordered_map<char, std::function<void()>> act_;
};

}} // namespace Slum::Turtle
