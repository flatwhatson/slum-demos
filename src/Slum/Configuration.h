#pragma once

#include <string>
#include <utility>

namespace Slum {

class Configuration {
 public:
  Configuration(int argc, const char** argv);

  std::pair<int,int> resolution() const { return resolution_; }
  int screenWidth() const { return resolution_.first; }
  int screenHeight() const { return resolution_.second; }
  void setResolution(std::pair<int,int> resolution) { resolution_ = resolution; }

  bool fullscreen() const { return fullscreen_; }
  void setFullscreen(bool fullscreen) { fullscreen_ = fullscreen; }

  bool borderless() const { return borderless_; }
  void setBorderless(bool borderless) { borderless_ = borderless; }

  bool resizeable() const { return resizeable_; }
  void setResizeable(bool resizeable) { resizeable_ = resizeable; }

  int frameLimit() const { return frameLimit_; }
  int frameTicks() const { return frameTicks_; }
  void setFrameLimit(int frameLimit) {
    frameLimit_ = frameLimit;
    frameTicks_ = 1000/frameLimit_;
  }

 private:
  std::pair<int,int> resolution_ = { 1280, 720 };
  bool fullscreen_ = false;
  bool borderless_ = false;
  bool resizeable_ = true;
  int frameLimit_ = 62;
  int frameTicks_ = 1000/frameLimit_;
};

} // namespace Slum
