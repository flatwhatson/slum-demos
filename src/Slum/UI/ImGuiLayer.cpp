#include "ImGuiLayer.h"

#include <Slum/Application.h>
#include <Slum/Configuration.h>
#include <Slum/GL/Context.h>
#include <Slum/GL/Shader.h>

#include <imgui.h>
#include <SDL.h>
#include <glm/glm.hpp>

#include <stdexcept>

namespace Slum { namespace UI {

ImGuiLayer::ImGuiLayer(Application& parent)
    : Layer(parent), io_(ImGui::GetIO()) {

  io_.KeyMap[ImGuiKey_Tab] = SDLK_TAB;
  io_.KeyMap[ImGuiKey_LeftArrow] = SDL_SCANCODE_LEFT;
  io_.KeyMap[ImGuiKey_RightArrow] = SDL_SCANCODE_RIGHT;
  io_.KeyMap[ImGuiKey_UpArrow] = SDL_SCANCODE_UP;
  io_.KeyMap[ImGuiKey_DownArrow] = SDL_SCANCODE_DOWN;
  io_.KeyMap[ImGuiKey_PageUp] = SDL_SCANCODE_PAGEUP;
  io_.KeyMap[ImGuiKey_PageDown] = SDL_SCANCODE_PAGEDOWN;
  io_.KeyMap[ImGuiKey_Home] = SDL_SCANCODE_HOME;
  io_.KeyMap[ImGuiKey_End] = SDL_SCANCODE_END;
  io_.KeyMap[ImGuiKey_Delete] = SDLK_DELETE;
  io_.KeyMap[ImGuiKey_Backspace] = SDLK_BACKSPACE;
  io_.KeyMap[ImGuiKey_Enter] = SDLK_RETURN;
  io_.KeyMap[ImGuiKey_Escape] = SDLK_ESCAPE;
  io_.KeyMap[ImGuiKey_A] = SDLK_a;
  io_.KeyMap[ImGuiKey_C] = SDLK_c;
  io_.KeyMap[ImGuiKey_V] = SDLK_v;
  io_.KeyMap[ImGuiKey_X] = SDLK_x;
  io_.KeyMap[ImGuiKey_Y] = SDLK_y;
  io_.KeyMap[ImGuiKey_Z] = SDLK_z;

  io_.GetClipboardTextFn = &GetClipboardText;
  io_.SetClipboardTextFn = &SetClipboardText;

  io_.DisplaySize.x = app().config().screenWidth();
  io_.DisplaySize.y = app().config().screenHeight();

  initFonts();
  initShader();
}

ImGuiLayer::~ImGuiLayer() {
  io_.Fonts->TexID = 0;
  ImGui::Shutdown();
}

void ImGuiLayer::onResize(int width, int height) {
  io_.DisplaySize.x = width;
  io_.DisplaySize.y = height;
}

void ImGuiLayer::onTextInput(const char* text, bool& captured) {
  if (io_.WantTextInput) {
    io_.AddInputCharactersUTF8(text);
    captured = true;
  }
}

void ImGuiLayer::onKeyPress(int key, int mod, bool& captured) {
  io_.KeysDown[key] = true;
  io_.KeyAlt = mod & KMOD_ALT;
  io_.KeyCtrl = mod & KMOD_CTRL;
  io_.KeyShift = mod & KMOD_SHIFT;
  if (io_.WantCaptureKeyboard) captured = true;
}

void ImGuiLayer::onKeyRelease(int key, int mod, bool& captured) {
  io_.KeysDown[key] = false;
  io_.KeyAlt = mod & KMOD_ALT;
  io_.KeyCtrl = mod & KMOD_CTRL;
  io_.KeyShift = mod & KMOD_SHIFT;
  if (io_.WantCaptureKeyboard) captured = true;
}

void ImGuiLayer::onMousePress(int button, bool& captured) {
  if (button == SDL_BUTTON_LEFT) mouseIsPressed_[0] = mouseWasPressed_[0] = true;
  if (button == SDL_BUTTON_RIGHT) mouseIsPressed_[1] = mouseWasPressed_[1] = true;
  if (button == SDL_BUTTON_MIDDLE) mouseIsPressed_[2] = mouseWasPressed_[2] = true;
  if (io_.WantCaptureMouse) captured = true;
}

void ImGuiLayer::onMouseRelease(int button, bool& captured) {
  if (button == SDL_BUTTON_LEFT) mouseIsPressed_[0] = false;
  if (button == SDL_BUTTON_RIGHT) mouseIsPressed_[1] = false;
  if (button == SDL_BUTTON_MIDDLE) mouseIsPressed_[2] = false;
  if (io_.WantCaptureMouse) captured = true;
}

void ImGuiLayer::onMouseScroll(int xscroll, int yscroll, bool& captured) {
  if (yscroll > 0) io_.MouseWheel = 1.0f;
  if (yscroll < 0) io_.MouseWheel = -1.0f;
  if (io_.WantCaptureMouse) captured = true;
}

void ImGuiLayer::onMouseMove(int xpos, int ypos, bool& captured) {
  io_.MousePos.x = xpos;
  io_.MousePos.y = ypos;
  if (io_.WantCaptureMouse) captured = true;
}

void ImGuiLayer::prepare() {
  io_.MouseDown[0] = mouseIsPressed_[0] || mouseWasPressed_[0];
  io_.MouseDown[1] = mouseIsPressed_[1] || mouseWasPressed_[1];
  io_.MouseDown[2] = mouseIsPressed_[2] || mouseWasPressed_[2];
  mouseWasPressed_ = {{ false, false, false }};

  GLuint frameTime = SDL_GetTicks();
  io_.DeltaTime = lastFrameTime_ ? (frameTime - lastFrameTime_)/1000.0f : 1/60.0f;
  lastFrameTime_ = frameTime;

  SDL_ShowCursor(io_.MouseDrawCursor ? 0 : 1);

  ImGui::NewFrame();
}

void ImGuiLayer::render() {
  ImGui::Render();

  auto data = ImGui::GetDrawData();
  float height = io_.DisplaySize.y * io_.DisplayFramebufferScale.y;
  data->ScaleClipRects(io_.DisplayFramebufferScale);

  GL::Context::setCapabilities({GL::BLEND, GL::SCISSOR_TEST});
  GL::Context::setBlendEquation(GL::FUNC_ADD);
  GL::Context::setBlendFunction(GL::SRC_ALPHA, GL::ONE_MINUS_SRC_ALPHA);

  shaderProgram_
    .setUniform(uniformTexture_, 0)
    .setUniform(uniformProjMatrix_, glm::mat4({
      { 2.0f/io_.DisplaySize.x, 0.0f,                    0.0f, 0.0f },
      { 0.0f,                   2.0f/-io_.DisplaySize.y, 0.0f, 0.0f },
      { 0.0f,                   0.0f,                   -1.0f, 0.0f },
      {-1.0f,                   1.0f,                    0.0f, 1.0f },
    }));

  vertexArray_.bind();

  for (int n = 0; n < data->CmdListsCount; ++n) {
    const ImDrawList* list = data->CmdLists[n];
    const ImDrawIdx* offset = 0;

    vertexBuffer_.setData({list->VtxBuffer.Data, list->VtxBuffer.Size * sizeof(ImDrawVert)});
    indexBuffer_.setData({list->IdxBuffer.Data, list->IdxBuffer.Size * sizeof(ImDrawIdx)});

    for (const ImDrawCmd* cmd = list->CmdBuffer.begin(); cmd != list->CmdBuffer.end(); ++cmd) {
      if (cmd->UserCallback) {
        cmd->UserCallback(list, cmd);
      } else {
        GL::Context::bindTexture(0, GL::TEXTURE_2D, (GLuint)(intptr_t)cmd->TextureId);
        GL::Context::setScissor(
          (int)cmd->ClipRect.x,
          (int)(height - cmd->ClipRect.w),
          (int)(cmd->ClipRect.z - cmd->ClipRect.x),
          (int)(cmd->ClipRect.w - cmd->ClipRect.y));
        vertexArray_
          .setCount(cmd->ElemCount)
          .setOffset(offset)
          .draw();
      }
      offset += cmd->ElemCount;
    }
  }

  GL::Context::disable(GL::SCISSOR_TEST);
}

void ImGuiLayer::initFonts() {
  unsigned char* pixels;
  int width, height;
  io_.Fonts->GetTexDataAsAlpha8(&pixels, &width, &height);

  fontTexture_ = GL::Texture(GL::TEXTURE_2D, GL::R8)
    .setMinFilter(GL::LINEAR)
    .setMagFilter(GL::LINEAR)
    .setImage(0, {GL::RED, GL::UNSIGNED_BYTE, width, height, pixels})
    .setSwizzle(GL::ONE, GL::ONE, GL::ONE, GL::RED);

  io_.Fonts->TexID = reinterpret_cast<void*>(fontTexture_.id());
  io_.Fonts->ClearInputData();
  io_.Fonts->ClearTexData();
}

void ImGuiLayer::initShader() {
  shaderProgram_ = GL::Program(
    GL::Shader(GL::VERTEX_SHADER, R"(#version 330
      uniform mat4 ProjMtx;
      in vec2 Position;
      in vec2 UV;
      in vec4 Color;
      out vec2 Frag_UV;
      out vec4 Frag_Color;

      void main() {
        Frag_UV = UV;
        Frag_Color = Color;
        gl_Position = ProjMtx * vec4(Position.xy, 0, 1);
      }
    )"),
    GL::Shader(GL::FRAGMENT_SHADER, R"(#version 330
      uniform sampler2D Texture;
      in vec2 Frag_UV;
      in vec4 Frag_Color;
      out vec4 Out_Color;

      void main() {
        Out_Color = Frag_Color * texture(Texture, Frag_UV.st);
      }
    )"));

  uniformTexture_ = shaderProgram_.getUniformLocation("Texture");
  uniformProjMatrix_ = shaderProgram_.getUniformLocation("ProjMtx");
  GLint attributePosition = shaderProgram_.getAttributeLocation("Position");
  GLint attributeTexCoord = shaderProgram_.getAttributeLocation("UV");
  GLint attributeColor = shaderProgram_.getAttributeLocation("Color");

  GLsizei stride = sizeof(ImDrawVert);
  GLvoid* offsetPosition = reinterpret_cast<GLvoid*>(offsetof(ImDrawVert, pos));
  GLvoid* offsetTexCoord = reinterpret_cast<GLvoid*>(offsetof(ImDrawVert, uv));
  GLvoid* offsetColor = reinterpret_cast<GLvoid*>(offsetof(ImDrawVert, col));

  vertexBuffer_ = GL::Buffer(GL::ARRAY_BUFFER, GL::STREAM_DRAW);
  indexBuffer_ = GL::Buffer(GL::ELEMENT_ARRAY_BUFFER, GL::STREAM_DRAW);

  vertexArray_ = GL::VertexArray(GL::TRIANGLES)
    .setAttributePointer(attributePosition, vertexBuffer_, 2, GL::FLOAT, false, stride, offsetPosition)
    .setAttributePointer(attributeTexCoord, vertexBuffer_, 2, GL::FLOAT, false, stride, offsetTexCoord)
    .setAttributePointer(attributeColor, vertexBuffer_, 4, GL::UNSIGNED_BYTE, true, stride, offsetColor)
    .setIndexBuffer(indexBuffer_, GL::UNSIGNED_SHORT);
}

const char* ImGuiLayer::GetClipboardText(void*) {
  return SDL_GetClipboardText();
}

void ImGuiLayer::SetClipboardText(void*, const char* text) {
  SDL_SetClipboardText(text);
}

}} // namespace Slum::UI
