#pragma once

#include <Slum/Layer.h>
#include <Slum/GL/Buffer.h>
#include <Slum/GL/Program.h>
#include <Slum/GL/Texture.h>
#include <Slum/GL/Types.h>
#include <Slum/GL/VertexArray.h>

#include <array>

struct ImGuiIO;

namespace Slum { namespace UI {

class ImGuiLayer : public Layer {
 public:
  ImGuiLayer(Application& parent);
  ~ImGuiLayer();

 private:
  void onResize(int width, int height) override;
  void onTextInput(const char* text, bool& captured) override;
  void onKeyPress(int key, int mod, bool& captured) override;
  void onKeyRelease(int key, int mod, bool& captured) override;

  void onMousePress(int button, bool& captured) override;
  void onMouseRelease(int button, bool& captured) override;
  void onMouseScroll(int xscroll, int yscroll, bool& captured) override;
  void onMouseMove(int xpos, int ypos, bool& captured) override;

  void prepare() override;
  void render() override;

  void initFonts();
  void initShader();

 private:
  static const char* GetClipboardText(void*);
  static void SetClipboardText(void*, const char* text);

  ImGuiIO& io_;
  std::array<bool,3> mouseIsPressed_ = {{ false, false, false }};
  std::array<bool,3> mouseWasPressed_ = {{ false, false, false }};
  GLuint lastFrameTime_ = 0;

  GL::Program shaderProgram_;
  GL::Buffer vertexBuffer_, indexBuffer_;
  GL::VertexArray vertexArray_;
  GL::Texture fontTexture_;
  GLint uniformTexture_, uniformProjMatrix_;
};

}} // namespace Slum::UI
