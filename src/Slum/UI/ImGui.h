#pragma once

#include <Slum/Util/LookupTable.h>

#include <imgui.h>

namespace ImGui {

template<class T, size_t Size>
bool Combo(const char* label, int* current_item, const Slum::Util::LookupTable<const char*, T, Size>& items) {
  return Combo(label, current_item, items.keys().data(), items.size());
}

} // namespace ImGui
