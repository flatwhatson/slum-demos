#pragma once

#include <string>
#include <vector>

union SDL_Event;
struct SDL_Window;
typedef void* SDL_GLContext;

namespace Slum {

class Configuration;
class Layer;

class Application {
 public:
  Application(Configuration& config, const std::string& title = "slum");
  ~Application();

  Application(Application&&) = delete;
  Application(const Application&) = delete;
  Application& operator=(Application&&) = delete;
  Application& operator=(const Application&) = delete;

  const std::string& title() const { return title_; }
  void setTitle(const std::string& title);

  const Configuration& config() const { return config_; }
  void setConfig(const Configuration& config);

  void toggleFullscreen();

  void run();

 private:
  friend Layer;

  void addLayer(Layer* layer);
  void removeLayer(Layer* layer);

  void processEvents();
  void onResize(SDL_Event& event);
  void onTextInput(SDL_Event& event);
  void onKeyPress(SDL_Event& event);
  void onKeyRelease(SDL_Event& event);
  void onMousePress(SDL_Event& event);
  void onMouseRelease(SDL_Event& event);
  void onMouseScroll(SDL_Event& event);
  void onMouseMove(SDL_Event& event);

  void logFrameRate(float fps);

 private:
  Configuration& config_;
  std::string title_;
  SDL_Window* window_ = nullptr;
  SDL_GLContext context_ = nullptr;
  std::vector<Layer*> layers_;
  bool running_ = false;

  int lastWindowWidth_, lastWindowHeight_;
  int lastWindowPosX_, lastWindowPosY_;
};

} // namespace Slum
