#include "CubeDemo.h"

#include <Slum/Application.h>
#include <Slum/Configuration.h>
#include <Slum/GL/Context.h>
#include <Slum/GL/Shader.h>

#define GLM_ENABLE_EXPERIMENTAL
#include <glm/glm.hpp>
#include <glm/gtx/transform.hpp>

#include <FastNoise.h>
#include <imgui.h>
#include <SDL.h>

#include <vector>

namespace Slum { namespace Demo { namespace Cube {

CubeDemo::CubeDemo(Application& parent)
    : Layer(parent) {

  std::vector<glm::vec3> positions {
    {  1,  1,  1 }, { -1,  1,  1 }, { -1, -1,  1 }, {  1, -1,  1 }, // front face
    {  1,  1, -1 }, { -1,  1, -1 }, { -1,  1,  1 }, {  1,  1,  1 }, // top face
    {  1,  1, -1 }, {  1,  1,  1 }, {  1, -1,  1 }, {  1, -1, -1 }, // right face
    {  1, -1, -1 }, { -1, -1, -1 }, { -1,  1, -1 }, {  1,  1, -1 }, // back face
    {  1, -1,  1 }, { -1, -1,  1 }, { -1, -1, -1 }, {  1, -1, -1 }, // bottom face
    { -1,  1,  1 }, { -1,  1, -1 }, { -1, -1, -1 }, { -1, -1,  1 }, // left face
  };

  std::vector<glm::vec3> normals {
    {  0,  0,  1 }, {  0,  0,  1 }, {  0,  0,  1 }, {  0,  0,  1 },
    {  0,  1,  0 }, {  0,  1,  0 }, {  0,  1,  0 }, {  0,  1,  0 },
    {  1,  0,  0 }, {  1,  0,  0 }, {  1,  0,  0 }, {  1,  0,  0 },
    {  0,  0, -1 }, {  0,  0, -1 }, {  0,  0, -1 }, {  0,  0, -1 },
    {  0, -1,  0 }, {  0, -1,  0 }, {  0, -1,  0 }, {  0, -1,  0 },
    { -1,  0,  0 }, { -1,  0,  0 }, { -1,  0,  0 }, { -1,  0,  0 },
  };

  std::vector<glm::vec2> texCoords {
    { 1.0/3.0, 0.0/3.0 }, { 0.0/3.0, 0.0/3.0 }, { 0.0/3.0, 1.0/3.0 }, { 1.0/3.0, 1.0/3.0 },
    { 2.0/3.0, 0.0/3.0 }, { 1.0/3.0, 0.0/3.0 }, { 1.0/3.0, 1.0/3.0 }, { 2.0/3.0, 1.0/3.0 },
    { 3.0/3.0, 0.0/3.0 }, { 2.0/3.0, 0.0/3.0 }, { 2.0/3.0, 1.0/3.0 }, { 3.0/3.0, 1.0/3.0 },
    { 1.0/3.0, 1.0/3.0 }, { 0.0/3.0, 1.0/3.0 }, { 0.0/3.0, 2.0/3.0 }, { 1.0/3.0, 2.0/3.0 },
    { 2.0/3.0, 1.0/3.0 }, { 1.0/3.0, 1.0/3.0 }, { 1.0/3.0, 2.0/3.0 }, { 2.0/3.0, 2.0/3.0 },
    { 3.0/3.0, 1.0/3.0 }, { 2.0/3.0, 1.0/3.0 }, { 2.0/3.0, 2.0/3.0 }, { 3.0/3.0, 2.0/3.0 },
  };

  std::vector<GLushort> indices {
     0,  1,  2,  2,  3,  0,
     4,  5,  6,  6,  7,  4,
     8,  9, 10, 10, 11,  8,
    12, 13, 14, 14, 15, 12,
    16, 17, 18, 18, 19, 16,
    20, 21, 22, 22, 23, 20,
  };

  std::vector<glm::vec3> texture;
  FastNoise noise;
  noise.SetSeed(777);
  noise.SetFrequency(0.5);
  noise.SetNoiseType(FastNoise::Simplex);
  int texSize = 256;
  float texScale = 1 / 3.75;
  texture.reserve(texSize * texSize);
  for (GLsizei y = 0; y < texSize; ++y) {
    for (GLsizei x = 0; x < texSize; ++x) {
      glm::vec3 color;
      color.r = (noise.GetNoise(x*texScale, y*texScale, 1) + 1) * 0.5f;
      color.g = (noise.GetNoise(x*texScale, y*texScale, 2) + 1) * 0.5f;
      color.b = (noise.GetNoise(x*texScale, y*texScale, 3) + 1) * 0.5f;
      texture.push_back(color);
    }
  }

  noiseTexture_ = GL::Texture(GL::TEXTURE_2D, GL::RGB)
    .setMagFilter(GL::LINEAR)
    .setMinFilter(GL::LINEAR_MIPMAP_LINEAR)
    .setImage(0, {GL::RGB, GL::FLOAT, texSize, texSize, texture.data()})
    .generateMipmap();

  positionBuffer_ = GL::Buffer(GL::ARRAY_BUFFER, GL::STATIC_DRAW, positions);
  normalBuffer_ = GL::Buffer(GL::ARRAY_BUFFER, GL::STATIC_DRAW, normals);
  texCoordBuffer_ = GL::Buffer(GL::ARRAY_BUFFER, GL::STATIC_DRAW, texCoords);
  indexBuffer_ = GL::Buffer(GL::ELEMENT_ARRAY_BUFFER, GL::STATIC_DRAW, indices);

  vertexArray_
    .setPrimitive(GL::TRIANGLES)
    .setAttributePointer(CubeShader::POSITION, positionBuffer_, 3, GL::FLOAT, false, 0, 0)
    .setAttributePointer(CubeShader::NORMAL, normalBuffer_, 3, GL::FLOAT, false, 0, 0)
    .setAttributePointer(CubeShader::TEXCOORD, texCoordBuffer_, 2, GL::FLOAT, false, 0, 0)
    .setIndexBuffer(indexBuffer_, GL::UNSIGNED_SHORT)
    .setCount(indices.size());

  shaderProgram_
    .setDiffuseColor({ 1.0, 1.0, 1.0 })
    .setAmbientColor({ 0.75, 0.75, 0.75 })
    .setSpecularColor({ 0.005, 0.005, 0.005 })
    .setLightColor({ 1.0, 1.0, 1.0 })
    .setLightPower(10)
    .setNoiseTexture(0);

  viewport_.x = app().config().screenWidth();
  viewport_.y = app().config().screenHeight();

  camera_.x = 15.0f;
  camera_.y = 15.0f;
  camera_.z = 5.0f;
  cameraDirty_ = true;
  cameraMoving_ = false;
}

void CubeDemo::onResize(int width, int height) {
  viewport_.x = width;
  viewport_.y = height;
  cameraDirty_ = true;
}

void CubeDemo::onMousePress(int button, bool& captured) {
  if (!captured && button == SDL_BUTTON_LEFT)
    cameraMoving_ = true;
}

void CubeDemo::onMouseRelease(int button, bool& captured) {
  if (button == SDL_BUTTON_LEFT)
    cameraMoving_ = false;
}

void CubeDemo::onMouseScroll(int xscroll, int yscroll, bool& captured) {
  if (!captured && yscroll != 0) {
    camera_.z -= yscroll;
    cameraDirty_ = true;
  }
}

void CubeDemo::onMouseMove(int xpos, int ypos, bool& captured) {
  if (cameraMoving_) {
    camera_.x = glm::mod(camera_.x + ((xpos - mouse_.x) / viewport_.x) * 180, 360.0f);
    camera_.y = glm::mod(camera_.y + ((ypos - mouse_.y) / viewport_.y) * 180, 360.0f);
    cameraDirty_ = true;
  }
  mouse_.x = xpos;
  mouse_.y = ypos;
}

void CubeDemo::render() {
  ImGui::ShowTestWindow();

  GL::Context::setCapabilities({GL::CULL_FACE, GL::DEPTH_TEST, GL::DITHER, GL::MULTISAMPLE});
  GL::Context::setDepthFunction(GL::LESS);

  if (cameraDirty_) {
    glm::vec3 eye(
      glm::rotate(glm::radians(-camera_.x), glm::vec3(0, 1, 0)) *
      glm::rotate(glm::radians(-camera_.y), glm::vec3(1, 0, 0)) *
      glm::translate(glm::vec3(0, 0, camera_.z)) *
      glm::vec4(0, 0, 0, 1));

    glm::mat4 mvp(
      glm::perspective(glm::radians(45.0f), viewport_.x / viewport_.y, 0.1f, 100.0f) *
      glm::lookAt(eye, glm::vec3(0, 0, 0), glm::vec3(0, 1, 0)));

    shaderProgram_
      .setModelViewProjection(mvp)
      .setLightPosition(eye)
      .setCameraPosition(eye);

    cameraDirty_ = false;
  }

  noiseTexture_.bind(0);
  shaderProgram_.use();
  vertexArray_.draw();
}

}}} // namespace Slum::Demo::Cube
