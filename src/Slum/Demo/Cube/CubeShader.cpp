#include "CubeShader.h"

#include <Slum/GL/Shader.h>

namespace Slum { namespace Demo { namespace Cube {

const GLint CubeShader::POSITION = 0;
const GLint CubeShader::NORMAL = 1;
const GLint CubeShader::TEXCOORD = 2;

CubeShader::CubeShader() {
  GL::Shader vertexShader(GL::VERTEX_SHADER, R"(#version 330 core
    uniform mat4 ModelViewProjection;

    in vec3 Position;
    in vec3 Normal;
    in vec2 TexCoord;

    out vec3 FragPosition;
    out vec3 FragNormal;
    out vec2 FragTexCoord;

    void main() {
      FragPosition = Position;
      FragNormal = Normal;
      FragTexCoord = TexCoord;
      gl_Position = ModelViewProjection * vec4(Position, 1.0f);
    }
  )");

  GL::Shader fragmentShader(GL::FRAGMENT_SHADER, R"(#version 330 core
    uniform vec3 DiffuseColor;
    uniform vec3 AmbientColor;
    uniform vec3 SpecularColor;
    uniform vec3 LightColor;
    uniform float LightPower;
    uniform vec3 LightPosition;
    uniform vec3 CameraPosition;

    uniform sampler2D NoiseTexture;

    in vec3 FragPosition;
    in vec3 FragNormal;
    in vec2 FragTexCoord;

    out vec3 Color;

    void main() {
      vec3 normal = normalize(FragNormal);
      vec3 light = normalize(LightPosition - FragPosition);
      vec3 camera = normalize(CameraPosition - FragPosition);
      vec3 reflection = reflect(-light, normal);

      float distance = length(LightPosition - FragPosition);
      float cosTheta = clamp(dot(normal, light), 0, 1);
      float cosAlpha = clamp(dot(camera, reflection), 0, 1);

      vec3 diffuse = DiffuseColor * texture(NoiseTexture, FragTexCoord).rgb;

      Color =
        diffuse * AmbientColor +
        diffuse * LightColor * LightPower * cosTheta / (distance * distance) +
        SpecularColor * LightColor * LightPower * pow(cosAlpha, 5) / (distance * distance);
    }
  )");

  program_
    .attachShader(vertexShader)
    .attachShader(fragmentShader)
    .bindAttribute(POSITION, "Position")
    .bindAttribute(NORMAL, "Normal")
    .bindAttribute(TEXCOORD, "TexCoord")
    .link()
    .validate();

  uModelViewProjection_ = program_.getUniformLocation("ModelViewProjection");
  uDiffuseColor_ = program_.getUniformLocation("DiffuseColor");
  uAmbientColor_ = program_.getUniformLocation("AmbientColor");
  uSpecularColor_ = program_.getUniformLocation("SpecularColor");
  uLightColor_ = program_.getUniformLocation("LightColor");
  uLightPower_ = program_.getUniformLocation("LightPower");
  uLightPosition_ = program_.getUniformLocation("LightPosition");
  uCameraPosition_ = program_.getUniformLocation("CameraPosition");
  uNoiseTexture_ = program_.getUniformLocation("NoiseTexture");
}

CubeShader& CubeShader::use() {
  program_.use();
  return *this;
}

CubeShader& CubeShader::setModelViewProjection(const glm::mat4& matrix) {
  program_.setUniform(uModelViewProjection_, matrix);
  return *this;
}

CubeShader& CubeShader::setDiffuseColor(const glm::vec3& color) {
  program_.setUniform(uDiffuseColor_, color);
  return *this;
}

CubeShader& CubeShader::setAmbientColor(const glm::vec3& color) {
  program_.setUniform(uAmbientColor_, color);
  return *this;
}

CubeShader& CubeShader::setSpecularColor(const glm::vec3& color) {
  program_.setUniform(uSpecularColor_, color);
  return *this;
}

CubeShader& CubeShader::setLightColor(const glm::vec3& color) {
  program_.setUniform(uLightColor_, color);
  return *this;
}

CubeShader& CubeShader::setLightPower(GLfloat power) {
  program_.setUniform(uLightPower_, power);
  return *this;
}

CubeShader& CubeShader::setLightPosition(const glm::vec3& position) {
  program_.setUniform(uLightPosition_, position);
  return *this;
}

CubeShader& CubeShader::setCameraPosition(const glm::vec3& position) {
  program_.setUniform(uCameraPosition_, position);
  return *this;
}

CubeShader& CubeShader::setNoiseTexture(GLint unit) {
  program_.setUniform(uNoiseTexture_, unit);
  return *this;
}

}}} // namespace Slum::Demo::Cube
