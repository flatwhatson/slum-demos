#pragma once

#include <Slum/GL/Program.h>
#include <Slum/GL/Types.h>

#include <glm/fwd.hpp>

namespace Slum { namespace Demo { namespace Cube {

class CubeShader {
 public:
  static const GLint POSITION;
  static const GLint NORMAL;
  static const GLint TEXCOORD;

  CubeShader();

  CubeShader& use();

  CubeShader& setModelViewProjection(const glm::mat4& matrix);
  CubeShader& setDiffuseColor(const glm::vec3& color);
  CubeShader& setAmbientColor(const glm::vec3& color);
  CubeShader& setSpecularColor(const glm::vec3& color);
  CubeShader& setLightColor(const glm::vec3& color);
  CubeShader& setLightPower(GLfloat power);
  CubeShader& setLightPosition(const glm::vec3& position);
  CubeShader& setCameraPosition(const glm::vec3& position);
  CubeShader& setNoiseTexture(GLint unit);

 private:
  GL::Program program_;
  GLint uModelViewProjection_;
  GLint uDiffuseColor_;
  GLint uAmbientColor_;
  GLint uSpecularColor_;
  GLint uLightColor_;
  GLint uLightPower_;
  GLint uLightPosition_;
  GLint uCameraPosition_;
  GLint uNoiseTexture_;
};

}}} // namespace Slum::Demo::Cube
