#pragma once

#include "CubeShader.h"

#include <Slum/Layer.h>
#include <Slum/GL/Buffer.h>
#include <Slum/GL/Program.h>
#include <Slum/GL/Texture.h>
#include <Slum/GL/VertexArray.h>

#include <glm/glm.hpp>

namespace Slum { namespace Demo { namespace Cube {

class CubeDemo final : public Layer {
 public:
  CubeDemo(Application& parent);

 private:
  void onResize(int width, int height) override;
  void onMousePress(int button, bool& captured) override;
  void onMouseRelease(int button, bool& captured) override;
  void onMouseScroll(int xscroll, int yscroll, bool& captured) override;
  void onMouseMove(int xpos, int ypos, bool& captured) override;
  void render() override;

 private:
  CubeShader shaderProgram_;
  GL::VertexArray vertexArray_;
  GL::Texture noiseTexture_;
  GL::Buffer positionBuffer_, normalBuffer_, texCoordBuffer_, indexBuffer_;

  glm::vec2 viewport_, mouse_;
  glm::vec3 camera_;
  bool cameraDirty_, cameraMoving_;
};

}}} // namespace Slum::Demo::Cube
