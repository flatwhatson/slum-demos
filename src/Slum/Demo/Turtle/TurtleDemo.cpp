#include "TurtleDemo.h"

#include <Slum/UI/ImGui.h>

namespace Slum { namespace Demo { namespace Turtle {

TurtleDemo::TurtleDemo(Application& parent)
    : Layer(parent),
      canvas_(320, 240),
      texture_(GL::Texture(GL::TEXTURE_2D, GL::R8)
          .setMinFilter(GL::NEAREST)
          .setMagFilter(GL::NEAREST)
          .setSwizzle(GL::RED, GL::RED, GL::RED, GL::ONE)) {

  dirty_ = true;
  distance_ = 50.0f;
  degrees_ = 30.0f;

  axiom_ = "F-F-F-F";
  lsystem_ = Slum::Turtle::LSystem(
    {{'F', "F-F+F+FF-F-F+F"}},
    {{'F', [&]{ canvas_.draw(distance_); }},
     {'f', [&]{ canvas_.move(distance_); }},
     {'+', [&]{ canvas_.turn(-degrees_); }},
     {'-', [&]{ canvas_.turn(degrees_); }}});
}

void TurtleDemo::render() {
  if (dirty_) {
    dirty_ = false;
    turtle_[0] = canvas_.positionX();
    turtle_[1] = canvas_.positionY();
    turtle_[2] = canvas_.direction();
    texture_.setImage(0, {GL::RED, GL::FLOAT, canvas_.width(), canvas_.height(), canvas_.data()});
  }

  if (ImGui::Begin("Turtle", nullptr, ImGuiWindowFlags_AlwaysAutoResize)) {
    ImGui::InputFloat3("Turtle X/Y/D", turtle_);
    ImGui::InputFloat("Move/Draw Distance", &distance_);
    ImGui::InputFloat("Left/Right Degrees", &degrees_);

    if (ImGui::Button("Clear")) {
      canvas_.clear();
      dirty_ = true;
    }
    if (ImGui::Button("Warp")) {
      canvas_.warp(turtle_[0], turtle_[1], turtle_[2]);
      dirty_ = true;
    }
    ImGui::SameLine();
    if (ImGui::Button("Move")) {
      canvas_.move(distance_);
      dirty_ = true;
    }
    ImGui::SameLine();
    if (ImGui::Button("Draw")) {
      canvas_.draw(distance_);
      dirty_ = true;
    }
    ImGui::SameLine();
    if (ImGui::Button("Left")) {
      canvas_.turn(-degrees_);
      dirty_ = true;
    }
    ImGui::SameLine();
    if (ImGui::Button("Right")) {
      canvas_.turn(degrees_);
      dirty_ = true;
    }
    ImGui::SameLine();
    if (ImGui::Button("Step")) {
      axiom_ = lsystem_.iterate(axiom_);

      canvas_.clear();
      lsystem_.interpret(axiom_);

      dirty_ = true;
    }

    ImGui::Image((ImTextureID)(intptr_t)texture_.id(), {(float)canvas_.width(), (float)canvas_.height()});
  }
  ImGui::End();
}

}}} // namespace Slum::Demo::Turtle
