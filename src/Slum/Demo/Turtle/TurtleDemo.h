#pragma once

#include <Slum/Layer.h>
#include <Slum/GL/Texture.h>
#include <Slum/Turtle/Canvas.h>
#include <Slum/Turtle/LSystem.h>

#include <string>
#include <vector>

namespace Slum { namespace Demo { namespace Turtle {

class TurtleDemo final : public Layer {
 public:
  TurtleDemo(Application& parent);

 private:
  void render() override;

 private:
  bool dirty_;
  float turtle_[3];
  float direction_;
  float distance_, degrees_;

  std::string axiom_;
  Slum::Turtle::LSystem lsystem_;
  Slum::Turtle::Canvas canvas_;
  GL::Texture texture_;
};

}}} // namespace Slum::Demo::Turtle
