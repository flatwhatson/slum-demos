#include "NoiseData.h"
#include "Constants.h"

#include <Slum/Util/Timer.h>
#include <Slum/Noise/Factory.h>

#include <FastNoise.h>
#include <FastNoiseSIMD.h>

#include <boost/simd/algorithm/for_each.hpp>
#include <boost/simd/algorithm/transform.hpp>
#include <boost/simd/function/maximum.hpp>
#include <boost/simd/function/minimum.hpp>
#include <boost/simd/function/sum.hpp>

#include <algorithm>
#include <limits>
#include <mutex>

namespace {
std::mutex FASTNOISESIMD_MUTEX;
} // anonymous namespace

namespace Slum { namespace Demo { namespace Noise {

NoiseData::NoiseData() = default;
NoiseData::~NoiseData() = default;

void NoiseData::generate() {
  xSize_ = std::max(config_.imageSize[0], 1);
  ySize_ = std::max(config_.imageSize[1], 1);
  xOffset_ = config_.imageOffset[0] - xSize_*0.5f;
  yOffset_ = config_.imageOffset[1] - ySize_*0.5f;

  size_t buffSize = xSize_ * ySize_;
  size_t vecSize = boost::simd::pack<float>::static_size;
  if (buffSize % vecSize != 0) {
    buffSize = vecSize * (1 + buffSize/vecSize);
  }
  if (buffer_.size() < buffSize) {
    buffer_.resize(buffSize);
  }

  auto timer = Slum::Util::Timer().start();

  switch (NOISE_ENGINE[config_.noiseEngine]) {
    case NoiseEngine::FastNoise:
      generateFastNoise();
      break;
    case NoiseEngine::FastNoiseSIMD:
      generateFastNoiseSIMD();
      break;
    case NoiseEngine::SlumNoise2D:
      generateSlumNoise2D();
      break;
    case NoiseEngine::SlumNoise3D:
      generateSlumNoise3D();
      break;
  }

  postProcessNoise();

  time_ = timer.mark().milliseconds();
}

void NoiseData::generateFastNoise() {
  if (!FastNoise_)
    FastNoise_ = std::make_unique<FastNoise>();

  auto& generator = *FastNoise_;

  generator.SetNoiseType(NOISE_TYPE[config_.noiseType]);
  generator.SetInterp(INTERP_TYPE[config_.noiseInterp]);
  generator.SetSeed(config_.noiseSeed);
  generator.SetFrequency(config_.noiseFreq);

  generator.SetFractalType(FRACTAL_TYPE[config_.fractType]);
  generator.SetFractalOctaves(config_.fractOctaves);
  generator.SetFractalLacunarity(config_.fractLacun);
  generator.SetFractalGain(config_.fractGain);

  generator.SetCellularDistanceFunction(CELLULAR_DISTANCE_FUNC[config_.cellDistanceFunc]);
  generator.SetCellularReturnType(CELLULAR_RETURN_TYPE[config_.cellReturnType]);
  generator.SetCellularDistance2Indices(config_.cellDistanceIndex[0], config_.cellDistanceIndex[1]);
  generator.SetCellularJitter(config_.cellJitter);

  if (CELLULAR_RETURN_TYPE[config_.cellReturnType] == FastNoise::NoiseLookup) {
    if (!lookup_) lookup_ = new FastNoise;
    lookup_->SetNoiseType(NOISE_TYPE[config_.cellNoiseType]);
    lookup_->SetFrequency(config_.cellNoiseFreq);
    generator.SetCellularNoiseLookup(lookup_);
  }

  size_t i = 0;
  for (int y = 0; y < ySize_; ++y) {
    for (int x = 0; x < xSize_; ++x) {
      float coordX = x + xOffset_;
      float coordY = y + yOffset_;

      switch (PERTURB_TYPE[config_.perturbType]) {
        case PerturbType::None:
          break;
        case PerturbType::Gradient:
          generator.GradientPerturb(coordX, coordY);
          break;
        case PerturbType::GradientFractal:
          generator.GradientPerturbFractal(coordX, coordY);
          break;
      }

      buffer_[i++] = generator.GetNoise(coordX, coordY);
    }
  }
}

void NoiseData::generateFastNoiseSIMD() {
  if (!FastNoiseSIMD_) {
    std::lock_guard<std::mutex> lock(FASTNOISESIMD_MUTEX);
    FastNoiseSIMD_.reset(FastNoiseSIMD::NewFastNoiseSIMD());
  }

  auto& generator = *FastNoiseSIMD_;

  generator.SetNoiseType(NOISE_TYPE_SIMD[config_.noiseType]);
  generator.SetSeed(config_.noiseSeed);
  generator.SetFrequency(config_.noiseFreq);

  generator.SetPerturbType(PERTURB_TYPE_SIMD[config_.perturbType]);
  generator.SetPerturbAmp(config_.perturbAmp);
  generator.SetPerturbFrequency(config_.perturbFreq);
  generator.SetPerturbFractalOctaves(config_.perturbOctaves);
  generator.SetPerturbFractalLacunarity(config_.perturbLacun);
  generator.SetPerturbFractalGain(config_.perturbGain);
  generator.SetPerturbNormaliseLength(config_.perturbLength);

  generator.SetFractalType(FRACTAL_TYPE_SIMD[config_.fractType]);
  generator.SetFractalOctaves(config_.fractOctaves);
  generator.SetFractalLacunarity(config_.fractLacun);
  generator.SetFractalGain(config_.fractGain);

  generator.SetCellularDistanceFunction(CELLULAR_DISTANCE_FUNC_SIMD[config_.cellDistanceFunc]);
  generator.SetCellularReturnType(CELLULAR_RETURN_TYPE_SIMD[config_.cellReturnType]);
  generator.SetCellularDistance2Indicies(config_.cellDistanceIndex[0], config_.cellDistanceIndex[1]);
  generator.SetCellularJitter(config_.cellJitter);

  generator.SetCellularNoiseLookupType(NOISE_TYPE_SIMD[config_.cellNoiseType]);
  generator.SetCellularNoiseLookupFrequency(config_.cellNoiseFreq);

  generator.FillNoiseSet(buffer_.data(), 0, yOffset_, xOffset_, 1, ySize_, xSize_);
}

namespace {

std::unique_ptr<Slum::Noise::Engine> buildSlumNoiseEngine(const ConfigData& config) {
  Slum::Noise::NoiseType noiseType;
  bool fractal = false;

  switch (NOISE_TYPE_SIMD[config.noiseType]) {
    case FastNoiseSIMD::WhiteNoise:     noiseType = Slum::Noise::NoiseType::WhiteNoise; break;
    case FastNoiseSIMD::ValueFractal:   fractal = true; [[fallthrough]];
    case FastNoiseSIMD::Value:          noiseType = Slum::Noise::NoiseType::ValueNoise; break;
    case FastNoiseSIMD::CubicFractal:   fractal = true; [[fallthrough]];
    case FastNoiseSIMD::Cubic:          noiseType = Slum::Noise::NoiseType::CubicNoise; break;
    case FastNoiseSIMD::PerlinFractal:  fractal = true; [[fallthrough]];
    case FastNoiseSIMD::Perlin:         noiseType = Slum::Noise::NoiseType::PerlinNoise; break;
    case FastNoiseSIMD::SimplexFractal: fractal = true; [[fallthrough]];
    case FastNoiseSIMD::Simplex:        noiseType = Slum::Noise::NoiseType::SimplexNoise; break;
    default:
      return nullptr;
  }

  if (!fractal) {
    auto noise = Slum::Noise::MakeNoiseEngine(noiseType);
    noise->setSeed(config.noiseSeed);
    noise->setFrequency(config.noiseFreq);
    return noise;
  }

  Slum::Noise::FractalType fractalType;

  switch (FRACTAL_TYPE_SIMD[config.fractType]) {
    case FastNoiseSIMD::FBM:        fractalType = Slum::Noise::FractalType::FbmFractal; break;
    case FastNoiseSIMD::Billow:     fractalType = Slum::Noise::FractalType::BillowFractal; break;
    case FastNoiseSIMD::RigidMulti: fractalType = Slum::Noise::FractalType::RigidMultiFractal; break;
    default:
      return nullptr;
  }

  auto noise = Slum::Noise::MakeFractalEngine(noiseType, fractalType);
  noise->setSeed(config.noiseSeed);
  noise->setFrequency(config.noiseFreq);
  noise->setOctaves(config.fractOctaves);
  noise->setLacunarity(config.fractLacun);
  noise->setGain(config.fractGain);
  return noise;
}

}

void NoiseData::generateSlumNoise2D() {
  Slum::Noise::IntVec2 offset = {int(yOffset_), int(xOffset_)};
  Slum::Noise::IntVec2 dimensions = {ySize_, xSize_};

  auto slumnoise = buildSlumNoiseEngine(config_);
  if (slumnoise) {
    slumnoise->fill(buffer_, offset, dimensions);
  } else {
    std::fill(begin(buffer_), end(buffer_), 0.0f);
  }
}

void NoiseData::generateSlumNoise3D() {
  Slum::Noise::IntVec3 offset = {0, int(yOffset_), int(xOffset_)};
  Slum::Noise::IntVec3 dimensions = {1, ySize_, xSize_};

  auto slumnoise = buildSlumNoiseEngine(config_);
  if (slumnoise) {
    slumnoise->fill(buffer_, offset, dimensions);
  } else {
    std::fill(begin(buffer_), end(buffer_), 0.0f);
  }
}

void NoiseData::postProcessNoise() {
  float minValue = std::numeric_limits<float>::max();
  float maxValue = std::numeric_limits<float>::min();
  float sumValue = 0.0f;

  auto noop = [](...){};

  auto analyze = [&minValue, &maxValue, &sumValue](const auto f) {
    return [f, &minValue, &maxValue, &sumValue](const auto values) {
      minValue = std::min(minValue, boost::simd::minimum(values));
      maxValue = std::max(maxValue, boost::simd::maximum(values));
      sumValue += boost::simd::sum(values);
      return f(values);
    };
  };

  auto invert = [](const auto f) {
    return [f](const auto values) {
      return 1.0f - f(values);
    };
  };

  auto rescale = [](const float min, const float max) {
    const float scale = 1.0f / (max - min);
    return [min, scale](const auto values) {
      return (values - min) * scale;
    };
  };

  if (config_.normalize) {
    boost::simd::for_each(buffer_, analyze(noop));
    if (!config_.invert) {
      boost::simd::transform(buffer_, rescale(minValue, maxValue));
    } else {
      boost::simd::transform(buffer_, invert(rescale(minValue, maxValue)));
    }
  } else if (!config_.invert) {
    boost::simd::transform(buffer_, analyze(rescale(-1.0f, 1.0f)));
  } else {
    boost::simd::transform(buffer_, analyze(invert(rescale(-1.0f, 1.0f))));
  }

  minValue_ = minValue;
  maxValue_ = maxValue;
  meanValue_ = sumValue / buffer_.size();
}

}}} // namespace Slum::Demo::Noise
