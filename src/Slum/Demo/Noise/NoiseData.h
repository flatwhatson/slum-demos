#pragma once

#include "ConfigData.h"

#include <boost/simd/memory/allocator.hpp>

#include <memory>
#include <vector>

class FastNoise;
class FastNoiseSIMD;

namespace Slum { namespace Demo { namespace Noise {

class NoiseData {
 public:
  NoiseData();
  ~NoiseData();

  void setConfig(const ConfigData& config) { config_ = config; }

  int width() const { return xSize_; }
  int height() const { return ySize_; }
  float minValue() const { return minValue_; }
  float maxValue() const { return maxValue_; }
  float meanValue() const { return meanValue_; }
  double time() const { return time_; }

  int size() const { return buffer_.size(); }
  const float* data() const { return buffer_.data(); }

  void generate();

 private:
  void generateFastNoise();
  void generateFastNoiseSIMD();
  void generateSlumNoise2D();
  void generateSlumNoise3D();
  void postProcessNoise();

 private:
  ConfigData config_ = {};

  int xSize_ = 0;
  int ySize_ = 0;
  float xOffset_ = 0.0f;
  float yOffset_ = 0.0f;

  float minValue_ = 0.0f;
  float maxValue_ = 0.0f;
  float meanValue_ = 0.0f;
  double time_ = 0.0f;

  std::unique_ptr<FastNoise> FastNoise_;
  std::unique_ptr<FastNoiseSIMD> FastNoiseSIMD_;
  FastNoise* lookup_;

  std::vector<float, boost::simd::allocator<float>> buffer_;
};

}}} // namespace Slum::Demo::Noise
