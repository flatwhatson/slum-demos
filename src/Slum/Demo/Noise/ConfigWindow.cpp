#include "ConfigWindow.h"
#include "Constants.h"
#include "NoiseData.h"

#include <Slum/UI/ImGui.h>

#include <FastNoise.h>
#include <FastNoiseSIMD.h>

#include <algorithm>

namespace Slum { namespace Demo { namespace Noise {

void ConfigWindow::configure(NoiseData& noise) {
  noise.setConfig(config_);
}

void ConfigWindow::render() {
  if (ImGui::Begin("Configuration", nullptr, ImGuiWindowFlags_AlwaysAutoResize)) {
    showImageSettings();
    showNoiseSettings();
    showPerturbSettings();
    showFractalSettings();
    showCellularSettings();

    ImGui::Separator();
    ImGui::Spacing();
    if (ImGui::Button("Generate")) {
      dirty_ = true;
    }
  }
  ImGui::End();
}

void ConfigWindow::showImageSettings() {
  ImGui::PushID("Image Settings");
  ImGui::Text("Image Settings:");
  dirty_ |= ImGui::InputInt2("Image Size", config_.imageSize);
  dirty_ |= ImGui::InputFloat2("Image Offset", config_.imageOffset, 0);
  dirty_ |= ImGui::Checkbox("Normalize", &config_.normalize);
  ImGui::SameLine();
  dirty_ |= ImGui::Checkbox("Invert", &config_.invert);
  ImGui::Spacing();
  ImGui::PopID();
}

void ConfigWindow::showNoiseSettings() {
  ImGui::PushID("Noise Settings");
  ImGui::Text("Noise Settings:");
  dirty_ |= ImGui::Combo("Noise Engine", &config_.noiseEngine, NOISE_ENGINE);
  dirty_ |= ImGui::Combo("Noise Type", &config_.noiseType, NOISE_TYPE);
  if (NOISE_ENGINE[config_.noiseEngine] == NoiseEngine::FastNoise) {
    dirty_ |= ImGui::Combo("Interpolation", &config_.noiseInterp, INTERP_TYPE);
  }
  dirty_ |= ImGui::InputInt("Seed", &config_.noiseSeed);
  dirty_ |= ImGui::InputFloat("Frequency", &config_.noiseFreq);
  ImGui::Spacing();
  ImGui::PopID();
}

void ConfigWindow::showPerturbSettings() {
  ImGui::PushID("Perturb Settings");
  ImGui::Text("Perturb Settings:");
  if (NOISE_ENGINE[config_.noiseEngine] == NoiseEngine::FastNoise) {
    dirty_ |= ImGui::Combo("Perturb Type", &config_.perturbType, PERTURB_TYPE);
  } else {
    dirty_ |= ImGui::Combo("Perturb Type", &config_.perturbType, PERTURB_TYPE_SIMD);
  }
  if (PERTURB_TYPE[config_.perturbType] != PerturbType::None) {
    dirty_ |= ImGui::InputFloat("Amplitude", &config_.perturbAmp);
    dirty_ |= ImGui::InputFloat("Frequency", &config_.perturbFreq);
    if (NOISE_ENGINE[config_.noiseEngine] != NoiseEngine::FastNoise) {
      if (PERTURB_TYPE_SIMD[config_.perturbType] == FastNoiseSIMD::GradientFractal ||
          PERTURB_TYPE_SIMD[config_.perturbType] == FastNoiseSIMD::GradientFractal_Normalise) {
        dirty_ |= ImGui::InputInt("Octaves", &config_.perturbOctaves);
        dirty_ |= ImGui::InputFloat("Lacunarity", &config_.perturbLacun);
        dirty_ |= ImGui::InputFloat("Gain", &config_.perturbGain);
      }
      if (PERTURB_TYPE_SIMD[config_.perturbType] == FastNoiseSIMD::Normalise ||
          PERTURB_TYPE_SIMD[config_.perturbType] == FastNoiseSIMD::Gradient_Normalise ||
          PERTURB_TYPE_SIMD[config_.perturbType] == FastNoiseSIMD::GradientFractal_Normalise) {
        dirty_ |= ImGui::InputFloat("Normalize Length", &config_.perturbLength);
      }
    }
  }
  ImGui::Spacing();
  ImGui::PopID();
}

void ConfigWindow::showFractalSettings() {
  if (NOISE_TYPE[config_.noiseType] != FastNoise::ValueFractal &&
      NOISE_TYPE[config_.noiseType] != FastNoise::PerlinFractal &&
      NOISE_TYPE[config_.noiseType] != FastNoise::SimplexFractal &&
      NOISE_TYPE[config_.noiseType] != FastNoise::CubicFractal)
    return;

  ImGui::PushID("Fractal Settings");
  ImGui::Text("Fractal Settings:");
  dirty_ |= ImGui::Combo("Fractal Type", &config_.fractType, FRACTAL_TYPE);
  dirty_ |= ImGui::InputInt("Octaves", &config_.fractOctaves);
  dirty_ |= ImGui::InputFloat("Lacunarity", &config_.fractLacun);
  dirty_ |= ImGui::InputFloat("Gain", &config_.fractGain);
  ImGui::Spacing();
  ImGui::PopID();
}

void ConfigWindow::showCellularSettings() {
  if (NOISE_TYPE[config_.noiseType] != FastNoise::Cellular)
    return;

  ImGui::PushID("Cellular Settings");
  ImGui::Text("Cellular Settings:");
  dirty_ |= ImGui::Combo("Distance Function", &config_.cellDistanceFunc, CELLULAR_DISTANCE_FUNC);
  if (NOISE_ENGINE[config_.noiseEngine] == NoiseEngine::FastNoise) {
    dirty_ |= ImGui::Combo("Return Type", &config_.cellReturnType, CELLULAR_RETURN_TYPE);
  } else {
    dirty_ |= ImGui::Combo("Return Type", &config_.cellReturnType, CELLULAR_RETURN_TYPE_SIMD);
  }
  dirty_ |= ImGui::InputFloat("Jitter", &config_.cellJitter);
  if (CELLULAR_RETURN_TYPE[config_.cellReturnType] == FastNoise::NoiseLookup) {
    dirty_ |= ImGui::Combo("Noise Type", &config_.cellNoiseType, NOISE_TYPE);
    dirty_ |= ImGui::InputFloat("Frequency", &config_.cellNoiseFreq);
  }
  if (CELLULAR_RETURN_TYPE_SIMD[config_.cellReturnType] == FastNoiseSIMD::Distance2 ||
      CELLULAR_RETURN_TYPE_SIMD[config_.cellReturnType] == FastNoiseSIMD::Distance2Add ||
      CELLULAR_RETURN_TYPE_SIMD[config_.cellReturnType] == FastNoiseSIMD::Distance2Sub ||
      CELLULAR_RETURN_TYPE_SIMD[config_.cellReturnType] == FastNoiseSIMD::Distance2Mul ||
      CELLULAR_RETURN_TYPE_SIMD[config_.cellReturnType] == FastNoiseSIMD::Distance2Div ||
      CELLULAR_RETURN_TYPE_SIMD[config_.cellReturnType] == FastNoiseSIMD::Distance2Cave) {
    dirty_ |= ImGui::InputInt2("Distance Indices", config_.cellDistanceIndex);
  }
  ImGui::Spacing();
  ImGui::PopID();
}

}}} // namespace Slum::Demo::Noise
