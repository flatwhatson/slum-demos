#pragma once

#include <Slum/Util/LookupTable.h>

#include <FastNoise.h>
#include <FastNoiseSIMD.h>

namespace {

struct NoiseEngine {
  enum { FastNoise, FastNoiseSIMD, SlumNoise2D, SlumNoise3D };
};

const auto NOISE_ENGINE = Slum::Util::LookupTable(
  { "FastNoise", //"FastNoiseSIMD", // FIXME: this engine crashes
    "SlumNoise2D", "SlumNoise3D" },
  { NoiseEngine::FastNoise, //NoiseEngine::FastNoiseSIMD,
    NoiseEngine::SlumNoise2D, NoiseEngine::SlumNoise3D });

////////////////////////////////////////

const auto NOISE_TYPE = Slum::Util::LookupTable(
  { "Value", "Value (Fractal)",
    "Perlin", "Perlin (Fractal)",
    "Simplex", "Simplex (Fractal)",
    "Cubic", "Cubic (Fractal)",
    "Cellular", "White Noise" },
  { FastNoise::Value, FastNoise::ValueFractal,
    FastNoise::Perlin, FastNoise::PerlinFractal,
    FastNoise::Simplex, FastNoise::SimplexFractal,
    FastNoise::Cubic, FastNoise::CubicFractal,
    FastNoise::Cellular, FastNoise::WhiteNoise });

const auto NOISE_TYPE_SIMD = Slum::Util::LookupTable(
  { "Value", "Value (Fractal)",
    "Perlin", "Perlin (Fractal)",
    "Simplex", "Simplex (Fractal)",
    "Cubic", "Cubic (Fractal)",
    "Cellular", "White Noise" },
  { FastNoiseSIMD::Value, FastNoiseSIMD::ValueFractal,
    FastNoiseSIMD::Perlin, FastNoiseSIMD::PerlinFractal,
    FastNoiseSIMD::Simplex, FastNoiseSIMD::SimplexFractal,
    FastNoiseSIMD::Cubic, FastNoiseSIMD::CubicFractal,
    FastNoiseSIMD::Cellular, FastNoiseSIMD::WhiteNoise });

////////////////////////////////////////

const auto INTERP_TYPE = Slum::Util::LookupTable(
  { "Linear", "Hermite", "Quintic" },
  { FastNoise::Linear, FastNoise::Hermite, FastNoise::Quintic });

////////////////////////////////////////

struct PerturbType {
  enum { None, Gradient, GradientFractal };
};

const auto PERTURB_TYPE = Slum::Util::LookupTable(
  { "None", "Gradient", "Gradient (Fractal)" },
  { PerturbType::None, PerturbType::Gradient, PerturbType::GradientFractal });

const auto PERTURB_TYPE_SIMD = Slum::Util::LookupTable(
  { "None", "Gradient", "Gradient (Fractal)",
    "Normalize", "Gradient Normalize",
    "Gradient Normalize (Fractal)" },
  { FastNoiseSIMD::None, FastNoiseSIMD::Gradient, FastNoiseSIMD::GradientFractal,
    FastNoiseSIMD::Normalise, FastNoiseSIMD::Gradient_Normalise,
    FastNoiseSIMD::GradientFractal_Normalise });

////////////////////////////////////////

const auto FRACTAL_TYPE = Slum::Util::LookupTable(
  { "FBM", "Billow", "Rigid Multi" },
  { FastNoise::FBM, FastNoise::Billow, FastNoise::RigidMulti });

const auto FRACTAL_TYPE_SIMD = Slum::Util::LookupTable(
  { "FBM", "Billow", "Rigid Multi" },
  { FastNoiseSIMD::FBM, FastNoiseSIMD::Billow, FastNoiseSIMD::RigidMulti });

////////////////////////////////////////

const auto CELLULAR_DISTANCE_FUNC = Slum::Util::LookupTable(
  { "Euclidean", "Manhattan", "Natural" },
  { FastNoise::Euclidean, FastNoise::Manhattan, FastNoise::Natural });

const auto CELLULAR_DISTANCE_FUNC_SIMD = Slum::Util::LookupTable(
  { "Euclidean", "Manhattan", "Natural" },
  { FastNoiseSIMD::Euclidean, FastNoiseSIMD::Manhattan, FastNoiseSIMD::Natural });

////////////////////////////////////////

const auto CELLULAR_RETURN_TYPE = Slum::Util::LookupTable(
  { "Cell value", "Noise lookup", "Distance",
    "Distance2", "Distance2Add", "Distance2Sub",
    "Distance2Mul", "Distance2Div" },
  { FastNoise::CellValue, FastNoise::NoiseLookup, FastNoise::Distance,
    FastNoise::Distance2, FastNoise::Distance2Add, FastNoise::Distance2Sub,
    FastNoise::Distance2Mul, FastNoise::Distance2Div });

const auto CELLULAR_RETURN_TYPE_SIMD = Slum::Util::LookupTable(
  { "Cell value", "Noise lookup", "Distance",
    "Distance2", "Distance2Add", "Distance2Sub",
    "Distance2Mul", "Distance2Div", "Distance2Cave" },
  { FastNoiseSIMD::CellValue, FastNoiseSIMD::NoiseLookup, FastNoiseSIMD::Distance,
    FastNoiseSIMD::Distance2, FastNoiseSIMD::Distance2Add, FastNoiseSIMD::Distance2Sub,
    FastNoiseSIMD::Distance2Mul, FastNoiseSIMD::Distance2Div, FastNoiseSIMD::Distance2Cave });

} // anonymous namespace
