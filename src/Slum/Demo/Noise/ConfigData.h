#pragma once

#include "Constants.h"

namespace Slum { namespace Demo { namespace Noise {

struct ConfigData {
  // Image settings
  int imageSize[2] = { 512, 512 };
  float imageOffset[2] = { 0.0f, 0.0f };
  bool normalize = true;
  bool invert = false;

  // Noise settings
  int noiseEngine = NOISE_ENGINE[NoiseEngine::FastNoise];
  int noiseType = 0;
  int noiseInterp = INTERP_TYPE[FastNoise::Quintic];
  int noiseSeed = 0;
  float noiseFreq = 0.01f;

  // Perturb settings
  int perturbType = 0;
  float perturbAmp = 1.0f;
  float perturbFreq = 0.5f;
  int perturbOctaves = 3;
  float perturbLacun = 2.0f;
  float perturbGain = 0.5f;
  float perturbLength = 1.0f;

  // Fractal settings
  int fractType = 0;
  int fractOctaves = 5;
  float fractLacun = 2.0f;
  float fractGain = 0.5f;

  // Cellular settings
  int cellDistanceFunc = 0;
  int cellReturnType = 0;
  float cellJitter = 0.45f;
  int cellNoiseType = 0;
  float cellNoiseFreq = 0.2f;
  int cellDistanceIndex[2] = { 0, 1 };
};

}}} // namespace Slum::Demo::Noise
