#include "NoiseDemo.h"
#include "NoiseData.h"

#include <SDL.h>

#include <algorithm>
#include <functional>
#include <memory>

namespace Slum { namespace Demo { namespace Noise {

NoiseDemo::NoiseDemo(Application& parent)
    : Layer(parent) {
  for (unsigned int i = 0, n = std::thread::hardware_concurrency(); i < n; ++i) {
    workers_.emplace_back(std::bind(&NoiseDemo::workerThread, this));
  }
  for (int i = 0; i < 2; ++i) {
    toRecycle_.push(std::make_unique<NoiseData>());
  }
}

NoiseDemo::~NoiseDemo() {
  {
    std::lock_guard<std::mutex> lock(mutex_);
    running_ = false;
    ready_.notify_all();
  }
  for (auto& worker : workers_) {
    worker.join();
  }
}

void NoiseDemo::render() {
  output_.render();

  if (output_.dirty()) {
    if (output_.width() != config_.width() ||
        output_.height() != config_.height()) {
      config_.setWidth(output_.width());
      config_.setHeight(output_.height());
      config_.setDirty(true);
    }
    output_.setDirty(false);
  }

  config_.render();

  std::lock_guard<std::mutex> lock(mutex_);

  if (config_.dirty()) {
    if (!toGenerate_) {
      if (!toRecycle_.empty()) {
        toGenerate_ = std::move(toRecycle_.front());
        toRecycle_.pop();
      } else {
        toGenerate_ = std::make_unique<NoiseData>();
      }
      ready_.notify_one();
    }

    config_.configure(*toGenerate_);
    config_.setDirty(false);
  }

  if (!toDisplay_.empty()) {
    output_.display(*toDisplay_.back());

    while (!toDisplay_.empty()) {
      toRecycle_.push(std::move(toDisplay_.front()));
      toDisplay_.pop();
    }
  }
}

void NoiseDemo::workerThread() {
  for (;;) {
    std::unique_ptr<NoiseData> noise;
    {
      std::unique_lock<std::mutex> lock(mutex_);
      ready_.wait(lock, [&]{ return !running_ || toGenerate_; });
      if (!running_) break;

      noise = std::move(toGenerate_);
      toGenerate_.reset();
    }
    if (noise) {
      noise->generate();

      std::lock_guard<std::mutex> lock(mutex_);
      toDisplay_.push(std::move(noise));
    }
  }
}

void NoiseDemo::onMousePress(int button, bool& captured) {
  if (output_.hovered() && button == SDL_BUTTON_LEFT) {
    output_.setDragging(true);

    outputDragX_ = mousePosX_;
    outputDragY_ = mousePosY_;
  }
}

void NoiseDemo::onMouseRelease(int button, bool& captured) {
  if (output_.dragging() && button == SDL_BUTTON_LEFT) {
    output_.setDragging(false);
  }
}

void NoiseDemo::onMouseScroll(int xscroll, int yscroll, bool& captured) {
  if (output_.hovered()) {
    float prevFreq = config_.frequency();
    float nextFreq = std::max(prevFreq - yscroll * 0.001f, 0.001f);

    if (nextFreq != prevFreq) {
      config_.setFrequency(nextFreq);
      config_.setOffsetX(config_.offsetX() * prevFreq/nextFreq);
      config_.setOffsetY(config_.offsetY() * prevFreq/nextFreq);
      config_.setDirty(true);
    }
  }
}

void NoiseDemo::onMouseMove(int xpos, int ypos, bool& captured) {
  mousePosX_ = xpos;
  mousePosY_ = ypos;

  if (output_.dragging()) {
    config_.setOffsetX(config_.offsetX() - (mousePosX_ - outputDragX_));
    config_.setOffsetY(config_.offsetY() - (mousePosY_ - outputDragY_));
    config_.setDirty(true);

    outputDragX_ = mousePosX_;
    outputDragY_ = mousePosY_;
  }
}

}}} // namespace Slum::Demo::Noise
