#pragma once

#include "ConfigWindow.h"
#include "OutputWindow.h"

#include <Slum/Layer.h>

#include <condition_variable>
#include <mutex>
#include <queue>
#include <thread>
#include <vector>

namespace Slum { namespace Demo { namespace Noise {

class NoiseDemo final : public Layer {
 public:
  NoiseDemo(Application& parent);
  ~NoiseDemo();

 private:
  void render() override;

  void workerThread();

  void onMousePress(int button, bool& captured) override;
  void onMouseRelease(int button, bool& captured) override;
  void onMouseScroll(int xscroll, int yscroll, bool& captured) override;
  void onMouseMove(int xpos, int ypos, bool& captured) override;

 private:
  ConfigWindow config_;
  OutputWindow output_;

  int mousePosX_, mousePosY_;
  int outputDragX_, outputDragY_;

  bool running_ = true;
  std::mutex mutex_;
  std::condition_variable ready_;
  std::vector<std::thread> workers_;

  std::unique_ptr<NoiseData> toGenerate_;
  std::queue<std::unique_ptr<NoiseData>> toDisplay_;
  std::queue<std::unique_ptr<NoiseData>> toRecycle_;
};

}}} // namespace Slum::Demo::Noise
