#include "OutputWindow.h"
#include "NoiseData.h"

#include <imgui.h>

namespace Slum { namespace Demo { namespace Noise {

OutputWindow::OutputWindow()
    : texture_(GL::Texture(GL::TEXTURE_2D, GL::R8)
        .setMinFilter(GL::LINEAR)
        .setMagFilter(GL::LINEAR)
        .setSwizzle(GL::RED, GL::RED, GL::RED, GL::ONE)) {}

void OutputWindow::display(const NoiseData& noise) {
  if (noiseWidth_ == 0 && noiseHeight_ == 0)
    resize_ = true;

  noiseWidth_ = noise.width();
  noiseHeight_ = noise.height();

  noiseMin_ = noise.minValue();
  noiseMax_ = noise.maxValue();
  noiseMean_ = noise.meanValue();
  noiseTime_ = noise.time();

  texture_.setImage(0, {GL::RED, GL::FLOAT, noiseWidth_, noiseHeight_, noise.data()});
}

void OutputWindow::render() {
  int flags = 0;
  flags |= ImGuiWindowFlags_NoScrollbar;
  flags |= ImGuiWindowFlags_NoScrollWithMouse;
  if (dragging_) flags |= ImGuiWindowFlags_NoMove;

  if (resize_) {
    ImGui::SetNextWindowSize({0.0f, 0.0f});
  }

  if (ImGui::Begin("Output", nullptr, flags)) {
    ImGui::Text("Min: %f", noiseMin_);
    ImGui::SameLine();
    ImGui::Text("Max: %f", noiseMax_);
    ImGui::SameLine();
    ImGui::Text("Mean: %f", noiseMean_);
    ImGui::SameLine();
    ImGui::Text("Time: %fms", noiseTime_);
    ImGui::Image((ImTextureID)(intptr_t)texture_.id(), {(float)noiseWidth_, (float)noiseHeight_});
    hovered_ = ImGui::IsItemHovered();

    if (!resize_ && noiseWidth_ != 0 && noiseHeight_ != 0) {
      auto windowSize = ImGui::GetWindowSize();
      if (windowSize.x != windowWidth_ || windowSize.y != windowHeight_) {
        windowWidth_ = windowSize.x;
        windowHeight_ = windowSize.y;
        if (windowPadX_ == 0 && windowPadY_ == 0) {
          windowPadX_ = windowWidth_ - noiseWidth_;
          windowPadY_ = windowHeight_ - noiseHeight_;
        } else {
          nextWidth_ = windowWidth_ - windowPadX_;
          nextHeight_ = windowHeight_ - windowPadY_;
          dirty_ = true;
        }
      }
    }
  }
  ImGui::End();

  if (resize_) {
    resize_ = false;
  }
}

}}} // namespace Slum::Demo::Noise
