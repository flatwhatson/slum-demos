#pragma once

#include "ConfigData.h"

namespace Slum { namespace Demo { namespace Noise {

class NoiseData;

class ConfigWindow {
 public:
  ConfigWindow() = default;

  int width() const { return config_.imageSize[0]; }
  int height() const { return config_.imageSize[1]; }
  float offsetX() const { return config_.imageOffset[0]; }
  float offsetY() const { return config_.imageOffset[1]; }
  float frequency() const { return config_.noiseFreq; }
  bool dirty() const { return dirty_; }

  void setWidth(int width) { config_.imageSize[0] = width; }
  void setHeight(int height) { config_.imageSize[1] = height; }
  void setOffsetX(float offsetX) { config_.imageOffset[0] = offsetX; }
  void setOffsetY(float offsetY) { config_.imageOffset[1] = offsetY; }
  void setFrequency(float freq) { config_.noiseFreq = freq; }
  void setDirty(bool dirty) { dirty_ = dirty; }

  void configure(NoiseData& noise);

  void render();

 private:
  void showImageSettings();
  void showNoiseSettings();
  void showPerturbSettings();
  void showFractalSettings();
  void showCellularSettings();

 private:
  bool dirty_ = true;
  ConfigData config_ = {};
};

}}} // namespace Slum::Demo::Noise
