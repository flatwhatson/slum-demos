#pragma once

#include <Slum/GL/Texture.h>

namespace Slum { namespace Demo { namespace Noise {

class NoiseData;

class OutputWindow {
 public:
  OutputWindow();

  int width() const { return nextWidth_; }
  int height() const { return nextHeight_; }
  bool hovered() const { return hovered_; }
  bool dragging() const { return dragging_; }
  bool dirty() const { return dirty_; }

  void setDragging(bool dragging) { dragging_ = dragging; }
  void setDirty(bool dirty) { dirty_ = dirty; }

  void display(const NoiseData& noise);

  void render();

 private:
  bool dirty_ = false;
  bool hovered_ = false;
  bool dragging_ = false;
  bool resize_ = false;
  float windowWidth_ = 0, windowHeight_ = 0;
  float windowPadX_ = 0, windowPadY_ = 0;
  int noiseWidth_ = 0, noiseHeight_ = 0;
  int nextWidth_ = 0, nextHeight_ = 0;
  float noiseMin_, noiseMax_, noiseMean_;
  double noiseTime_;
  GL::Texture texture_;
};

}}} // namespace Slum::Demo::Noise
