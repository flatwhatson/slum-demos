#pragma once

#include <algorithm>
#include <array>

namespace Slum { namespace Util {

template<class Key, class Value, size_t Size>
class LookupTable {
 public:
  using KeyArray = std::array<Key, Size>;
  using ValueArray = std::array<Value, Size>;

  LookupTable(const KeyArray& keys, const ValueArray& values)
      : keys_(keys), values_(values) {}

  LookupTable(const Key (&keys)[Size], const Value (&values)[Size]) {
    std::copy_n(keys, Size, begin(keys_));
    std::copy_n(values, Size, begin(values_));
  }

  const KeyArray& keys() const {
    return keys_;
  }

  const ValueArray& values() const {
    return values_;
  }

  int size() const {
    return Size;
  }

  const Key& key(int index) const {
    if (index < 0 || size_t(index) >= Size)
      return keys_[0];
    return keys_[index];
  }

  const Value& value(int index) const {
    if (index < 0 || size_t(index) >= Size)
      return values_[0];
    return values_[index];
  }

  const Value& operator[](int index) const {
    return value(index);
  }

 private:
  std::array<Key, Size> keys_;
  std::array<Value, Size> values_;
};

}} // namespace Slum::Util
