#pragma once

#include <chrono>
#include <functional>
#include <utility>

namespace Slum { namespace Util {

class Timer {
 public:
  using clock = std::chrono::steady_clock;
  using seconds_t = std::chrono::duration<double>;
  using milliseconds_t = std::chrono::duration<double, std::milli>;
  using microseconds_t = std::chrono::duration<double, std::micro>;
  using nanoseconds_t = std::chrono::duration<double, std::nano>;

  Timer() = default;

  Timer&& start() {
    start_ = clock::now();
    return std::move(*this);
  }

  Timer&& mark() {
    time_ = clock::now() - start_;
    return std::move(*this);
  }

  Timer&& lap() {
    auto now = clock::now();
    time_ = now - start_;
    start_ = now;
    return std::move(*this);
  }

  void benchmark(std::function<void()> func) {
    start_ = clock::now();
    func();
    time_ = clock::now() - start_;
  }

  void benchmark(size_t repeat, std::function<void()> func) {
    start_ = clock::now();
    func();
    time_ = clock::now() - start_;
    for (size_t i = 1; i < repeat; ++i) {
      auto start = clock::now();
      func();
      auto time = clock::now() - start;
      if (time_ > time) time_ = time;
    }
  }

  static double now() {
    return std::chrono::duration_cast<seconds_t>(clock::now().time_since_epoch()).count();
  }

  double seconds() const {
    return std::chrono::duration_cast<seconds_t>(time_).count();
  }

  double milliseconds() const {
    return std::chrono::duration_cast<milliseconds_t>(time_).count();
  }

  double microseconds() const {
    return std::chrono::duration_cast<microseconds_t>(time_).count();
  }

  double nanoseconds() const {
    return std::chrono::duration_cast<nanoseconds_t>(time_).count();
  }

 private:
  clock::time_point start_;
  clock::duration time_;
};

}} // namespace Slum::Util
