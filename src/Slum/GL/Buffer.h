#pragma once

#include "Enums.h"
#include "Types.h"
#include "Util/ArrayView.h"
#include "Util/Handle.h"

namespace Slum { namespace GL {

class Buffer {
 public:
  Buffer(BufferTarget target, BufferUsage usage);
  Buffer(BufferTarget target, BufferUsage usage, const ArrayView<void>& data);
  ~Buffer();

  Buffer() = default;
  Buffer(Buffer&&) noexcept = default;
  Buffer& operator=(Buffer&&) noexcept = default;

  GLuint id() const { return id_; }
  BufferTarget target() const { return target_; }
  BufferUsage usage() const { return usage_; }

  Buffer&& create();
  Buffer&& setTarget(BufferTarget target);
  Buffer&& setUsage(BufferUsage usage);

  Buffer&& bind();
  Buffer&& setData(const ArrayView<void>& data);
  Buffer&& setSubData(GLintptr offset, const ArrayView<void>& data);

 private:
  Handle<GLuint> id_;
  BufferTarget target_;
  BufferUsage usage_;
};

}} // namespace Slum::GL
