#pragma once

#include "Enums.h"
#include "Types.h"
#include "Util/Handle.h"

namespace Slum { namespace GL {

class Buffer;

class VertexArray {
 public:
  explicit VertexArray(DrawPrimitive primitive);
  ~VertexArray();

  VertexArray() = default;
  VertexArray(VertexArray&&) noexcept = default;
  VertexArray& operator=(VertexArray&&) noexcept = default;

  GLuint id() const { return id_; }

  VertexArray&& create();
  VertexArray&& setPrimitive(DrawPrimitive primitive);
  VertexArray&& setOffset(const GLvoid* offset);
  VertexArray&& setOffset(GLsizei offset);
  VertexArray&& setCount(GLsizei count);

  VertexArray&& bind();
  VertexArray&& setAttributePointer(GLint location, Buffer& buffer, GLint size, VertexDataType type, bool normalized, GLsizei stride, const GLvoid* offset);
  VertexArray&& setIndexBuffer(Buffer& buffer, IndexDataType type);
  VertexArray&& draw();

 private:
  Handle<GLuint> id_;
  DrawPrimitive primitive_;
  IndexDataType indexType_;
  GLsizei count_ = 0;
  GLsizei offset_ = 0;
  bool indexed_ = false;
};

}} // namespace Slum::GL
