#include "Texture.h"

#include "API.h"
#include "Context.h"

#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>

#include <utility>

namespace Slum { namespace GL {

Texture::Texture(TextureTarget target, TextureFormat format) {
  setTarget(target);
  setFormat(format);
}

Texture::~Texture() {
  if (id_)
    glDeleteTextures(1, &id_);
}

Texture&& Texture::create() {
  if (!id_)
    glGenTextures(1, &id_);
  return std::move(*this);
}

Texture&& Texture::setTarget(TextureTarget target) {
  target_ = target;
  return std::move(*this);
}

Texture&& Texture::setFormat(TextureFormat format) {
  format_ = format;
  return std::move(*this);
}

Texture&& Texture::bind() {
  create();
  Context::bindTexture(target_, id_);
  return std::move(*this);
}

Texture&& Texture::bind(GLuint unit) {
  create();
  Context::bindTexture(unit, target_, id_);
  return std::move(*this);
}

Texture&& Texture::setBaseLevel(GLint level) {
  bind();
  glTexParameteri(toGLenum(target_), GL_TEXTURE_BASE_LEVEL, level);
  return std::move(*this);
}

Texture&& Texture::setMaxLevel(GLint level) {
  bind();
  glTexParameteri(toGLenum(target_), GL_TEXTURE_MAX_LEVEL, level);
  return std::move(*this);
}

Texture&& Texture::setMinLod(GLfloat lod) {
  bind();
  glTexParameterf(toGLenum(target_), GL_TEXTURE_MIN_LOD, lod);
  return std::move(*this);
}

Texture&& Texture::setMaxLod(GLfloat lod) {
  bind();
  glTexParameterf(toGLenum(target_), GL_TEXTURE_MAX_LOD, lod);
  return std::move(*this);
}

Texture&& Texture::setLodBias(GLfloat lod) {
  bind();
  glTexParameterf(toGLenum(target_), GL_TEXTURE_LOD_BIAS, lod);
  return std::move(*this);
}

Texture&& Texture::setMinFilter(TextureMinFilter filter) {
  bind();
  glTexParameteri(toGLenum(target_), GL_TEXTURE_MIN_FILTER, toGLint(filter));
  return std::move(*this);
}

Texture&& Texture::setMagFilter(TextureMagFilter filter) {
  bind();
  glTexParameteri(toGLenum(target_), GL_TEXTURE_MAG_FILTER, toGLint(filter));
  return std::move(*this);
}

Texture&& Texture::setSwizzleR(TextureSwizzle swizzleR) {
  bind();
  glTexParameteri(toGLenum(target_), GL_TEXTURE_SWIZZLE_R, toGLint(swizzleR));
  return std::move(*this);
}

Texture&& Texture::setSwizzleG(TextureSwizzle swizzleG) {
  bind();
  glTexParameteri(toGLenum(target_), GL_TEXTURE_SWIZZLE_G, toGLint(swizzleG));
  return std::move(*this);
}

Texture&& Texture::setSwizzleB(TextureSwizzle swizzleB) {
  bind();
  glTexParameteri(toGLenum(target_), GL_TEXTURE_SWIZZLE_B, toGLint(swizzleB));
  return std::move(*this);
}

Texture&& Texture::setSwizzleA(TextureSwizzle swizzleA) {
  bind();
  glTexParameteri(toGLenum(target_), GL_TEXTURE_SWIZZLE_A, toGLint(swizzleA));
  return std::move(*this);
}

Texture&& Texture::setSwizzle(TextureSwizzle swizzleRGBA) {
  bind();
  glTexParameteri(toGLenum(target_), GL_TEXTURE_SWIZZLE_RGBA, toGLint(swizzleRGBA));
  return std::move(*this);
}

Texture&& Texture::setSwizzle(TextureSwizzle swizzleR, TextureSwizzle swizzleG, TextureSwizzle swizzleB, TextureSwizzle swizzleA) {
  bind();
  glTexParameteri(toGLenum(target_), GL_TEXTURE_SWIZZLE_R, toGLint(swizzleR));
  glTexParameteri(toGLenum(target_), GL_TEXTURE_SWIZZLE_G, toGLint(swizzleG));
  glTexParameteri(toGLenum(target_), GL_TEXTURE_SWIZZLE_B, toGLint(swizzleB));
  glTexParameteri(toGLenum(target_), GL_TEXTURE_SWIZZLE_A, toGLint(swizzleA));
  return std::move(*this);
}

Texture&& Texture::setWrapS(TextureWrap wrapS) {
  bind();
  glTexParameteri(toGLenum(target_), GL_TEXTURE_WRAP_S, toGLint(wrapS));
  return std::move(*this);
}

Texture&& Texture::setWrapT(TextureWrap wrapT) {
  bind();
  glTexParameteri(toGLenum(target_), GL_TEXTURE_WRAP_T, toGLint(wrapT));
  return std::move(*this);
}

Texture&& Texture::setWrapR(TextureWrap wrapR) {
  bind();
  glTexParameteri(toGLenum(target_), GL_TEXTURE_WRAP_R, toGLint(wrapR));
  return std::move(*this);
}

Texture&& Texture::setBorderColor(glm::vec4 color) {
  bind();
  glTexParameterfv(toGLenum(target_), GL_TEXTURE_BORDER_COLOR, glm::value_ptr(color));
  return std::move(*this);
}

Texture&& Texture::setBorderColor(glm::ivec4 color) {
  bind();
  glTexParameteriv(toGLenum(target_), GL_TEXTURE_BORDER_COLOR, glm::value_ptr(color));
  return std::move(*this);
}

Texture&& Texture::setCompareMode(TextureCompareMode mode) {
  bind();
  glTexParameteri(toGLenum(target_), GL_TEXTURE_COMPARE_MODE, toGLint(mode));
  return std::move(*this);
}

Texture&& Texture::setCompareFunc(TextureCompareFunc func) {
  bind();
  glTexParameteri(toGLenum(target_), GL_TEXTURE_COMPARE_FUNC, toGLint(func));
  return std::move(*this);
}

Texture&& Texture::setImage(GLint level, const ImageView1D& image) {
  bind();
  glTexImage1D(toGLenum(target_), level, toGLint(format_), image.width(), 0, toGLenum(image.format()), toGLenum(image.type()), image.data());
  return std::move(*this);
}

Texture&& Texture::setImage(GLint level, const ImageView2D& image) {
  bind();
  glTexImage2D(toGLenum(target_), level, toGLint(format_), image.width(), image.height(), 0, toGLenum(image.format()), toGLenum(image.type()), image.data());
  return std::move(*this);
}

Texture&& Texture::setImage(GLint level, const ImageView3D& image) {
  bind();
  glTexImage3D(toGLenum(target_), level, toGLint(format_), image.width(), image.height(), image.depth(), 0, toGLenum(image.format()), toGLenum(image.type()), image.data());
  return std::move(*this);
}

Texture&& Texture::setSubImage(GLint level, GLint xoffset, const ImageView1D& image) {
  bind();
  glTexSubImage1D(toGLenum(target_), level, xoffset, image.width(), toGLenum(image.format()), toGLenum(image.type()), image.data());
  return std::move(*this);
}

Texture&& Texture::setSubImage(GLint level, GLint xoffset, GLint yoffset, const ImageView2D& image) {
  bind();
  glTexSubImage2D(toGLenum(target_), level, xoffset, yoffset, image.width(), image.height(), toGLenum(image.format()), toGLenum(image.type()), image.data());
  return std::move(*this);
}

Texture&& Texture::setSubImage(GLint level, GLint xoffset, GLint yoffset, GLint zoffset, const ImageView3D& image) {
  bind();
  glTexSubImage3D(toGLenum(target_), level, xoffset, yoffset, zoffset, image.width(), image.height(), image.depth(), toGLenum(image.format()), toGLenum(image.type()), image.data());
  return std::move(*this);
}

Texture&& Texture::generateMipmap() {
  bind();
  glGenerateMipmap(toGLenum(target_));
  return std::move(*this);
}

}} // namespace Slum::GL
