#pragma once

#include "Enums.h"
#include "Types.h"
#include "Util/Handle.h"
#include "Util/ImageView.h"

#include <glm/fwd.hpp>

// TODO: split texture classes eg. Texture1D, Texture2D
// NOTE: no support for GL_DEPTH_STENCIL_TEXTURE_MODE

namespace Slum { namespace GL {

class Texture {
 public:
  Texture(TextureTarget target, TextureFormat format);
  ~Texture();

  Texture() = default;
  Texture(Texture&&) noexcept = default;
  Texture& operator=(Texture&&) noexcept = default;

  GLuint id() const { return id_; }
  TextureTarget target() const { return target_; }
  TextureFormat format() const { return format_; }

  Texture&& create();
  Texture&& setTarget(TextureTarget target);
  Texture&& setFormat(TextureFormat format);

  Texture&& bind();
  Texture&& bind(GLuint unit);
  Texture&& setBaseLevel(GLint level);
  Texture&& setMaxLevel(GLint level);
  Texture&& setMinLod(GLfloat lod);
  Texture&& setMaxLod(GLfloat lod);
  Texture&& setLodBias(GLfloat lod);
  Texture&& setMinFilter(TextureMinFilter filter);
  Texture&& setMagFilter(TextureMagFilter filter);
  Texture&& setSwizzleR(TextureSwizzle swizzleR);
  Texture&& setSwizzleG(TextureSwizzle swizzleG);
  Texture&& setSwizzleB(TextureSwizzle swizzleB);
  Texture&& setSwizzleA(TextureSwizzle swizzleA);
  Texture&& setSwizzle(TextureSwizzle swizzleRGBA);
  Texture&& setSwizzle(TextureSwizzle swizzleR, TextureSwizzle swizzleG, TextureSwizzle swizzleB, TextureSwizzle swizzleA);
  Texture&& setWrapS(TextureWrap wrapS);
  Texture&& setWrapT(TextureWrap wrapT);
  Texture&& setWrapR(TextureWrap wrapR);
  Texture&& setBorderColor(glm::vec4 color);
  Texture&& setBorderColor(glm::ivec4 color);
  Texture&& setCompareMode(TextureCompareMode mode);
  Texture&& setCompareFunc(TextureCompareFunc func);
  Texture&& setImage(GLint level, const ImageView1D& image);
  Texture&& setImage(GLint level, const ImageView2D& image);
  Texture&& setImage(GLint level, const ImageView3D& image);
  Texture&& setSubImage(GLint level, GLint xoffset, const ImageView1D& image);
  Texture&& setSubImage(GLint level, GLint xoffset, GLint yoffset, const ImageView2D& image);
  Texture&& setSubImage(GLint level, GLint xoffset, GLint yoffset, GLint zoffset, const ImageView3D& image);
  Texture&& generateMipmap();

 private:
  Handle<GLuint> id_;
  TextureTarget target_;
  TextureFormat format_;
};

}} // namespace Slum::GL
