#include "VertexArray.h"

#include "Buffer.h"
#include "Context.h"

#include <utility>

namespace Slum { namespace GL {

VertexArray::VertexArray(DrawPrimitive primitive) {
  setPrimitive(primitive);
}

VertexArray::~VertexArray() {
  if (id_)
    glDeleteVertexArrays(1, &id_);
}

VertexArray&& VertexArray::create() {
  if (!id_)
    glGenVertexArrays(1, &id_);
  return std::move(*this);
}

VertexArray&& VertexArray::setPrimitive(DrawPrimitive primitive) {
  primitive_ = primitive;
  return std::move(*this);
}

VertexArray&& VertexArray::setOffset(const GLvoid* offset) {
  offset_ = reinterpret_cast<intptr_t>(offset);
  return std::move(*this);
}

VertexArray&& VertexArray::setOffset(GLsizei offset) {
  offset_ = offset;
  return std::move(*this);
}

VertexArray&& VertexArray::setCount(GLsizei count) {
  count_ = count;
  return std::move(*this);
}

VertexArray&& VertexArray::bind() {
  create();
  Context::bindVertexArray(id_);
  return std::move(*this);
}

VertexArray&& VertexArray::setAttributePointer(GLint location, Buffer& buffer, GLint size, VertexDataType type, bool normalized, GLsizei stride, const GLvoid* offset) {
  bind();
  buffer.bind();
  glEnableVertexAttribArray(location);
  glVertexAttribPointer(location, size, toGLenum(type), normalized ? GL_TRUE : GL_FALSE, stride, offset);
  return std::move(*this);
}

VertexArray&& VertexArray::setIndexBuffer(Buffer& buffer, IndexDataType type) {
  bind();
  Context::resetBuffer(buffer.target());
  buffer.bind();
  indexType_ = type;
  indexed_ = true;
  return std::move(*this);
}

VertexArray&& VertexArray::draw() {
  bind();
  if (!indexed_) {
    glDrawArrays(toGLenum(primitive_), offset_, count_);
  } else {
    glDrawElements(toGLenum(primitive_), count_, toGLenum(indexType_), reinterpret_cast<GLvoid*>(offset_));
  }
  return std::move(*this);
}

}} // namespace Slum::GL
