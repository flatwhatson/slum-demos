#pragma once

#include "Enums.h"
#include "Types.h"

#include <initializer_list>

// TODO: support capability GL_CLIP_DISTANCE

namespace Slum { namespace GL {

namespace detail { struct ContextBindings; }
namespace detail { struct ContextCapabilities; }

constexpr GLuint NUM_TEXTURE_UNITS = 32;

class Context {
 public:
  Context() = delete;

  static void reset();
  static void resetBuffer(BufferTarget target);

  static void useProgram(GLuint id);
  static void bindVertexArray(GLuint id);
  static void setTextureUnit(GLuint unit);
  static void bindBuffer(BufferTarget target, GLuint id);
  static void bindTexture(TextureTarget target, GLuint id);
  static void bindTexture(GLuint unit, TextureTarget target, GLuint id);

  static void enable(Capability capability);
  static void disable(Capability capability);
  static void setCapabilities(std::initializer_list<Capability> capabilities);

  static void setBlendEquation(BlendEquation mode);
  static void setBlendFunction(BlendFunction sfactor, BlendFunction dfactor);
  static void setDepthFunction(DepthFunction func);
  static void setScissor(GLint x, GLint y, GLsizei width, GLsizei height);

 private:
  static detail::ContextBindings bound_;
  static detail::ContextCapabilities capability_;
};

}} // namespace Slum::GL
