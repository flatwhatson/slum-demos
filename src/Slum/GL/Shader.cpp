#include "Shader.h"

#include "API.h"

#include <stdexcept>
#include <utility>

namespace Slum { namespace GL {

Shader::Shader(ShaderType type, std::string source) {
  setType(type);
  addSource(std::move(source));
  compile();
}

Shader::~Shader() {
  if (id_)
    glDeleteShader(id_);
}

Shader&& Shader::create() {
  if (!id_)
    id_ = glCreateShader(toGLenum(type_));
  return std::move(*this);
}

Shader&& Shader::setType(ShaderType type) {
  type_ = type;
  return std::move(*this);
}

Shader&& Shader::addSource(std::string source) {
  sources_.emplace_back(std::move(source));
  return std::move(*this);
}

Shader&& Shader::compile() {
  std::size_t count = sources_.size();
  std::vector<const GLchar*> strings;
  strings.reserve(count);
  std::vector<GLint> lengths;
  lengths.reserve(count);

  for (const auto& source : sources_) {
    strings.push_back(source.data());
    lengths.push_back(source.size());
  }

  create();
  glShaderSource(id_, count, strings.data(), lengths.data());
  glCompileShader(id_);

  GLint compileStatus;
  glGetShaderiv(id_, GL_COMPILE_STATUS, &compileStatus);
  if (compileStatus != GL_TRUE)
    throw std::runtime_error(getInfoLog());

  return std::move(*this);
}

std::string Shader::getInfoLog() {
  GLint infoLogLength;
  glGetShaderiv(id_, GL_INFO_LOG_LENGTH, &infoLogLength);

  std::string infoLog;
  if (infoLogLength > 0) {
    infoLog.resize(infoLogLength);
    glGetShaderInfoLog(id_, infoLog.size(), nullptr, &infoLog.front());
  }

  return infoLog;
}

}} // namespace Slum::GL
