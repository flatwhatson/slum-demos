#include "Context.h"

#include "API.h"

#include <array>
#include <bitset>

namespace Slum { namespace GL {

namespace detail {

  using BufferTargets = std::array<GLint, NUM_BUFFER_TARGETS>;
  using TextureTargets = std::array<std::array<GLint, NUM_TEXTURE_TARGETS>, NUM_TEXTURE_UNITS>;
  using Capabilities = std::bitset<NUM_CAPABILITIES>;

  struct ContextBindings {
    GLint program = -1;
    GLint vertexArray = -1;
    GLint textureUnit = -1;
    BufferTargets buffer = []{
      BufferTargets b;
      b.fill(-1);
      return b;
    }();
    TextureTargets texture = []{
      TextureTargets t;
      for (auto& u : t)
        u.fill(-1);
      return t;
    }();
  };

  struct ContextCapabilities {
    Capabilities unknown = {};
    Capabilities enabled = []{
      Capabilities e;
      e[toIndex(Capability::DITHER)] = true;
      e[toIndex(Capability::MULTISAMPLE)] = true;
      return e;
    }();
  };

}

detail::ContextBindings Context::bound_ = {};
detail::ContextCapabilities Context::capability_ = {};

void Context::reset() {
  bound_ = detail::ContextBindings();
  capability_.unknown.set();
}

void Context::resetBuffer(BufferTarget target) {
  bound_.buffer[toIndex(target)] = -1;
}

void Context::useProgram(GLuint id) {
  if (bound_.program != (GLint)id) {
    bound_.program = id;
    glUseProgram(id);
  }
}

void Context::bindVertexArray(GLuint id) {
  if (bound_.vertexArray != (GLint)id) {
    bound_.vertexArray = id;
    glBindVertexArray(id);
  }
}

void Context::setTextureUnit(GLuint unit) {
  if (bound_.textureUnit != (GLint)unit) {
    bound_.textureUnit = unit;
    glActiveTexture(GL_TEXTURE0 + unit);
  }
}

void Context::bindBuffer(BufferTarget target, GLuint id) {
  auto ix = toIndex(target);
  if (bound_.buffer[ix] != (GLint)id) {
    bound_.buffer[ix] = id;
    glBindBuffer(toGLenum(target), id);
  }
}

void Context::bindTexture(TextureTarget target, GLuint id) {
  auto ix = toIndex(target);
  if (bound_.textureUnit == -1) {
    setTextureUnit(0);
  }
  if (bound_.texture[bound_.textureUnit][ix] != (GLint)id) {
    bound_.texture[bound_.textureUnit][ix] = id;
    glBindTexture(toGLenum(target), id);
  }
}

void Context::bindTexture(GLuint unit, TextureTarget target, GLuint id) {
  auto ix = toIndex(target);
  setTextureUnit(unit);
  if (bound_.texture[unit][ix] != (GLint)id) {
    bound_.texture[unit][ix] = id;
    glBindTexture(toGLenum(target), id);
  }
}

void Context::enable(Capability capability) {
  auto ix = toIndex(capability);
  if (capability_.unknown[ix] || !capability_.enabled[ix]) {
    capability_.unknown[ix] = false;
    capability_.enabled[ix] = true;
    glEnable(toGLenum(capability));
  }
}

void Context::disable(Capability capability) {
  auto ix = toIndex(capability);
  if (capability_.unknown[ix] || capability_.enabled[ix]) {
    capability_.unknown[ix] = false;
    capability_.enabled[ix] = false;
    glDisable(toGLenum(capability));
  }
}

void Context::setCapabilities(std::initializer_list<Capability> capabilities) {
  detail::Capabilities requested;
  for (auto capability : capabilities)
    requested[toIndex(capability)] = true;

  auto toEnable = requested & (capability_.unknown | ~capability_.enabled);
  auto toDisable = ~requested & (capability_.unknown | capability_.enabled);

  for (GLuint ix = 0; ix < NUM_CAPABILITIES; ++ix) {
    if (toEnable[ix]) {
      capability_.unknown[ix] = false;
      capability_.enabled[ix] = true;
      glEnable(toGLenum(fromCapabilityIndex(ix)));
    } else if (toDisable[ix]) {
      capability_.unknown[ix] = false;
      capability_.enabled[ix] = false;
      glDisable(toGLenum(fromCapabilityIndex(ix)));
    }
  }
}

void Context::setBlendEquation(BlendEquation mode) {
  glBlendEquation(toGLenum(mode));
}

void Context::setBlendFunction(BlendFunction sfactor, BlendFunction dfactor) {
  glBlendFunc(toGLenum(sfactor), toGLenum(dfactor));
}

void Context::setDepthFunction(DepthFunction func) {
  glDepthFunc(toGLenum(func));
}

void Context::setScissor(GLint x, GLint y, GLsizei width, GLsizei height) {
  glScissor(x, y, width, height);
}

}} // namespace Slum::GL
