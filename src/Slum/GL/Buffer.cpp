#include "Buffer.h"

#include "API.h"
#include "Context.h"

#include <utility>

namespace Slum { namespace GL {

Buffer::Buffer(BufferTarget target, BufferUsage usage) {
  setTarget(target);
  setUsage(usage);
}

Buffer::Buffer(BufferTarget target, BufferUsage usage, const ArrayView<void>& data) {
  setTarget(target);
  setUsage(usage);
  setData(data);
}

Buffer::~Buffer() {
  if (id_)
    glDeleteBuffers(1, &id_);
}

Buffer&& Buffer::create() {
  if (!id_)
    glGenBuffers(1, &id_);
  return std::move(*this);
}

Buffer&& Buffer::setTarget(BufferTarget target) {
  target_ = target;
  return std::move(*this);
}

Buffer&& Buffer::setUsage(BufferUsage usage) {
  usage_ = usage;
  return std::move(*this);
}

Buffer&& Buffer::bind() {
  create();
  Context::bindBuffer(target_, id_);
  return std::move(*this);
}

Buffer&& Buffer::setData(const ArrayView<void>& data) {
  bind();
  glBufferData(toGLenum(target_), data.size(), data.data(), toGLenum(usage_));
  return std::move(*this);
}

Buffer&& Buffer::setSubData(GLintptr offset, const ArrayView<void>& data) {
  bind();
  glBufferSubData(toGLenum(target_), offset, data.size(), data.data());
  return std::move(*this);
}

}} // namespace Slum::GL
