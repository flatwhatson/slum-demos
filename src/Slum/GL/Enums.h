#pragma once

#include "API.h"

#include <stdexcept>

namespace Slum { namespace GL {

enum class BlendEquation {
  INVALID = -1,
  FUNC_ADD = GL_FUNC_ADD,
  FUNC_SUBTRACT = GL_FUNC_SUBTRACT,
  FUNC_REVERSE_SUBTRACT = GL_FUNC_REVERSE_SUBTRACT,
  MIN = GL_MIN,
  MAX = GL_MAX,
};

constexpr GLuint NUM_BLEND_EQUATIONS = 5;

constexpr GLenum toGLenum(BlendEquation value) {
  return static_cast<GLenum>(value);
}

constexpr GLint toGLint(BlendEquation value) {
  return static_cast<GLint>(value);
}

inline GLuint toIndex(BlendEquation value) {
  switch (value) {
    case BlendEquation::FUNC_ADD: return 0;
    case BlendEquation::FUNC_SUBTRACT: return 1;
    case BlendEquation::FUNC_REVERSE_SUBTRACT: return 2;
    case BlendEquation::MIN: return 3;
    case BlendEquation::MAX: return 4;
    case BlendEquation::INVALID:
    default:
      throw std::runtime_error("Invalid BlendEquation");
  }
}

inline BlendEquation fromBlendEquationIndex(GLint index) {
  switch (index) {
    case 0: return BlendEquation::FUNC_ADD;
    case 1: return BlendEquation::FUNC_SUBTRACT;
    case 2: return BlendEquation::FUNC_REVERSE_SUBTRACT;
    case 3: return BlendEquation::MIN;
    case 4: return BlendEquation::MAX;
    default: return BlendEquation::INVALID;
  }
}

enum class BlendFunction {
  INVALID = -1,
  ZERO = GL_ZERO,
  ONE = GL_ONE,
  SRC_COLOR = GL_SRC_COLOR,
  ONE_MINUS_SRC_COLOR = GL_ONE_MINUS_SRC_COLOR,
  DST_COLOR = GL_DST_COLOR,
  ONE_MINUS_DST_COLOR = GL_ONE_MINUS_DST_COLOR,
  SRC_ALPHA = GL_SRC_ALPHA,
  ONE_MINUS_SRC_ALPHA = GL_ONE_MINUS_SRC_ALPHA,
  DST_ALPHA = GL_DST_ALPHA,
  ONE_MINUS_DST_ALPHA = GL_ONE_MINUS_DST_ALPHA,
  CONSTANT_COLOR = GL_CONSTANT_COLOR,
  ONE_MINUS_CONSTANT_COLOR = GL_ONE_MINUS_CONSTANT_COLOR,
  CONSTANT_ALPHA = GL_CONSTANT_ALPHA,
  ONE_MINUS_CONSTANT_ALPHA = GL_ONE_MINUS_CONSTANT_ALPHA,
  SRC_ALPHA_SATURATE = GL_SRC_ALPHA_SATURATE,
  SRC1_COLOR = GL_SRC1_COLOR,
  ONE_MINUS_SRC1_COLOR = GL_ONE_MINUS_SRC1_COLOR,
  SRC1_ALPHA = GL_SRC1_ALPHA,
  ONE_MINUS_SRC1_ALPHA = GL_ONE_MINUS_SRC1_ALPHA,
};

constexpr GLuint NUM_BLEND_FUNCTIONS = 19;

constexpr GLenum toGLenum(BlendFunction value) {
  return static_cast<GLenum>(value);
}

constexpr GLint toGLint(BlendFunction value) {
  return static_cast<GLint>(value);
}

inline GLuint toIndex(BlendFunction value) {
  switch (value) {
    case BlendFunction::ZERO: return 0;
    case BlendFunction::ONE: return 1;
    case BlendFunction::SRC_COLOR: return 2;
    case BlendFunction::ONE_MINUS_SRC_COLOR: return 3;
    case BlendFunction::DST_COLOR: return 4;
    case BlendFunction::ONE_MINUS_DST_COLOR: return 5;
    case BlendFunction::SRC_ALPHA: return 6;
    case BlendFunction::ONE_MINUS_SRC_ALPHA: return 7;
    case BlendFunction::DST_ALPHA: return 8;
    case BlendFunction::ONE_MINUS_DST_ALPHA: return 9;
    case BlendFunction::CONSTANT_COLOR: return 10;
    case BlendFunction::ONE_MINUS_CONSTANT_COLOR: return 11;
    case BlendFunction::CONSTANT_ALPHA: return 12;
    case BlendFunction::ONE_MINUS_CONSTANT_ALPHA: return 13;
    case BlendFunction::SRC_ALPHA_SATURATE: return 14;
    case BlendFunction::SRC1_COLOR: return 15;
    case BlendFunction::ONE_MINUS_SRC1_COLOR: return 16;
    case BlendFunction::SRC1_ALPHA: return 17;
    case BlendFunction::ONE_MINUS_SRC1_ALPHA: return 18;
    case BlendFunction::INVALID:
    default:
      throw std::runtime_error("Invalid BlendFunction");
  }
}

inline BlendFunction fromBlendFunctionIndex(GLint index) {
  switch (index) {
    case 0: return BlendFunction::ZERO;
    case 1: return BlendFunction::ONE;
    case 2: return BlendFunction::SRC_COLOR;
    case 3: return BlendFunction::ONE_MINUS_SRC_COLOR;
    case 4: return BlendFunction::DST_COLOR;
    case 5: return BlendFunction::ONE_MINUS_DST_COLOR;
    case 6: return BlendFunction::SRC_ALPHA;
    case 7: return BlendFunction::ONE_MINUS_SRC_ALPHA;
    case 8: return BlendFunction::DST_ALPHA;
    case 9: return BlendFunction::ONE_MINUS_DST_ALPHA;
    case 10: return BlendFunction::CONSTANT_COLOR;
    case 11: return BlendFunction::ONE_MINUS_CONSTANT_COLOR;
    case 12: return BlendFunction::CONSTANT_ALPHA;
    case 13: return BlendFunction::ONE_MINUS_CONSTANT_ALPHA;
    case 14: return BlendFunction::SRC_ALPHA_SATURATE;
    case 15: return BlendFunction::SRC1_COLOR;
    case 16: return BlendFunction::ONE_MINUS_SRC1_COLOR;
    case 17: return BlendFunction::SRC1_ALPHA;
    case 18: return BlendFunction::ONE_MINUS_SRC1_ALPHA;
    default: return BlendFunction::INVALID;
  }
}

enum class BufferTarget {
  INVALID = -1,
  ARRAY_BUFFER = GL_ARRAY_BUFFER,
  COPY_READ_BUFFER = GL_COPY_READ_BUFFER,
  COPY_WRITE_BUFFER = GL_COPY_WRITE_BUFFER,
  DRAW_INDIRECT_BUFFER = GL_DRAW_INDIRECT_BUFFER,
  ELEMENT_ARRAY_BUFFER = GL_ELEMENT_ARRAY_BUFFER,
  PIXEL_PACK_BUFFER = GL_PIXEL_PACK_BUFFER,
  PIXEL_UNPACK_BUFFER = GL_PIXEL_UNPACK_BUFFER,
  TEXTURE_BUFFER = GL_TEXTURE_BUFFER,
  TRANSFORM_FEEDBACK_BUFFER = GL_TRANSFORM_FEEDBACK_BUFFER,
  UNIFORM_BUFFER = GL_UNIFORM_BUFFER,
};

constexpr GLuint NUM_BUFFER_TARGETS = 10;

constexpr GLenum toGLenum(BufferTarget value) {
  return static_cast<GLenum>(value);
}

constexpr GLint toGLint(BufferTarget value) {
  return static_cast<GLint>(value);
}

inline GLuint toIndex(BufferTarget value) {
  switch (value) {
    case BufferTarget::ARRAY_BUFFER: return 0;
    case BufferTarget::COPY_READ_BUFFER: return 1;
    case BufferTarget::COPY_WRITE_BUFFER: return 2;
    case BufferTarget::DRAW_INDIRECT_BUFFER: return 3;
    case BufferTarget::ELEMENT_ARRAY_BUFFER: return 4;
    case BufferTarget::PIXEL_PACK_BUFFER: return 5;
    case BufferTarget::PIXEL_UNPACK_BUFFER: return 6;
    case BufferTarget::TEXTURE_BUFFER: return 7;
    case BufferTarget::TRANSFORM_FEEDBACK_BUFFER: return 8;
    case BufferTarget::UNIFORM_BUFFER: return 9;
    case BufferTarget::INVALID:
    default:
      throw std::runtime_error("Invalid BufferTarget");
  }
}

inline BufferTarget fromBufferTargetIndex(GLint index) {
  switch (index) {
    case 0: return BufferTarget::ARRAY_BUFFER;
    case 1: return BufferTarget::COPY_READ_BUFFER;
    case 2: return BufferTarget::COPY_WRITE_BUFFER;
    case 3: return BufferTarget::DRAW_INDIRECT_BUFFER;
    case 4: return BufferTarget::ELEMENT_ARRAY_BUFFER;
    case 5: return BufferTarget::PIXEL_PACK_BUFFER;
    case 6: return BufferTarget::PIXEL_UNPACK_BUFFER;
    case 7: return BufferTarget::TEXTURE_BUFFER;
    case 8: return BufferTarget::TRANSFORM_FEEDBACK_BUFFER;
    case 9: return BufferTarget::UNIFORM_BUFFER;
    default: return BufferTarget::INVALID;
  }
}

enum class BufferUsage {
  INVALID = -1,
  STREAM_DRAW = GL_STREAM_DRAW,
  STREAM_READ = GL_STREAM_READ,
  STREAM_COPY = GL_STREAM_COPY,
  STATIC_DRAW = GL_STATIC_DRAW,
  STATIC_READ = GL_STATIC_READ,
  STATIC_COPY = GL_STATIC_COPY,
  DYNAMIC_DRAW = GL_DYNAMIC_DRAW,
  DYNAMIC_READ = GL_DYNAMIC_READ,
  DYNAMIC_COPY = GL_DYNAMIC_COPY,
};

constexpr GLuint NUM_BUFFER_USAGES = 9;

constexpr GLenum toGLenum(BufferUsage value) {
  return static_cast<GLenum>(value);
}

constexpr GLint toGLint(BufferUsage value) {
  return static_cast<GLint>(value);
}

inline GLuint toIndex(BufferUsage value) {
  switch (value) {
    case BufferUsage::STREAM_DRAW: return 0;
    case BufferUsage::STREAM_READ: return 1;
    case BufferUsage::STREAM_COPY: return 2;
    case BufferUsage::STATIC_DRAW: return 3;
    case BufferUsage::STATIC_READ: return 4;
    case BufferUsage::STATIC_COPY: return 5;
    case BufferUsage::DYNAMIC_DRAW: return 6;
    case BufferUsage::DYNAMIC_READ: return 7;
    case BufferUsage::DYNAMIC_COPY: return 8;
    case BufferUsage::INVALID:
    default:
      throw std::runtime_error("Invalid BufferUsage");
  }
}

inline BufferUsage fromBufferUsageIndex(GLint index) {
  switch (index) {
    case 0: return BufferUsage::STREAM_DRAW;
    case 1: return BufferUsage::STREAM_READ;
    case 2: return BufferUsage::STREAM_COPY;
    case 3: return BufferUsage::STATIC_DRAW;
    case 4: return BufferUsage::STATIC_READ;
    case 5: return BufferUsage::STATIC_COPY;
    case 6: return BufferUsage::DYNAMIC_DRAW;
    case 7: return BufferUsage::DYNAMIC_READ;
    case 8: return BufferUsage::DYNAMIC_COPY;
    default: return BufferUsage::INVALID;
  }
}

enum class Capability {
  INVALID = -1,
  BLEND = GL_BLEND,
  COLOR_LOGIC_OP = GL_COLOR_LOGIC_OP,
  CULL_FACE = GL_CULL_FACE,
  DEPTH_CLAMP = GL_DEPTH_CLAMP,
  DEPTH_TEST = GL_DEPTH_TEST,
  DITHER = GL_DITHER,
  FRAMEBUFFER_SRGB = GL_FRAMEBUFFER_SRGB,
  LINE_SMOOTH = GL_LINE_SMOOTH,
  MULTISAMPLE = GL_MULTISAMPLE,
  POLYGON_OFFSET_FILL = GL_POLYGON_OFFSET_FILL,
  POLYGON_OFFSET_LINE = GL_POLYGON_OFFSET_LINE,
  POLYGON_OFFSET_POINT = GL_POLYGON_OFFSET_POINT,
  POLYGON_SMOOTH = GL_POLYGON_SMOOTH,
  PRIMITIVE_RESTART = GL_PRIMITIVE_RESTART,
  RASTERIZER_DISCARD = GL_RASTERIZER_DISCARD,
  SAMPLE_ALPHA_TO_COVERAGE = GL_SAMPLE_ALPHA_TO_COVERAGE,
  SAMPLE_ALPHA_TO_ONE = GL_SAMPLE_ALPHA_TO_ONE,
  SAMPLE_COVERAGE = GL_SAMPLE_COVERAGE,
  SAMPLE_SHADING = GL_SAMPLE_SHADING,
  SAMPLE_MASK = GL_SAMPLE_MASK,
  SCISSOR_TEST = GL_SCISSOR_TEST,
  STENCIL_TEST = GL_STENCIL_TEST,
  TEXTURE_CUBE_MAP_SEAMLESS = GL_TEXTURE_CUBE_MAP_SEAMLESS,
  PROGRAM_POINT_SIZE = GL_PROGRAM_POINT_SIZE,
};

constexpr GLuint NUM_CAPABILITIES = 24;

constexpr GLenum toGLenum(Capability value) {
  return static_cast<GLenum>(value);
}

constexpr GLint toGLint(Capability value) {
  return static_cast<GLint>(value);
}

inline GLuint toIndex(Capability value) {
  switch (value) {
    case Capability::BLEND: return 0;
    case Capability::COLOR_LOGIC_OP: return 1;
    case Capability::CULL_FACE: return 2;
    case Capability::DEPTH_CLAMP: return 3;
    case Capability::DEPTH_TEST: return 4;
    case Capability::DITHER: return 5;
    case Capability::FRAMEBUFFER_SRGB: return 6;
    case Capability::LINE_SMOOTH: return 7;
    case Capability::MULTISAMPLE: return 8;
    case Capability::POLYGON_OFFSET_FILL: return 9;
    case Capability::POLYGON_OFFSET_LINE: return 10;
    case Capability::POLYGON_OFFSET_POINT: return 11;
    case Capability::POLYGON_SMOOTH: return 12;
    case Capability::PRIMITIVE_RESTART: return 13;
    case Capability::RASTERIZER_DISCARD: return 14;
    case Capability::SAMPLE_ALPHA_TO_COVERAGE: return 15;
    case Capability::SAMPLE_ALPHA_TO_ONE: return 16;
    case Capability::SAMPLE_COVERAGE: return 17;
    case Capability::SAMPLE_SHADING: return 18;
    case Capability::SAMPLE_MASK: return 19;
    case Capability::SCISSOR_TEST: return 20;
    case Capability::STENCIL_TEST: return 21;
    case Capability::TEXTURE_CUBE_MAP_SEAMLESS: return 22;
    case Capability::PROGRAM_POINT_SIZE: return 23;
    case Capability::INVALID:
    default:
      throw std::runtime_error("Invalid Capability");
  }
}

inline Capability fromCapabilityIndex(GLint index) {
  switch (index) {
    case 0: return Capability::BLEND;
    case 1: return Capability::COLOR_LOGIC_OP;
    case 2: return Capability::CULL_FACE;
    case 3: return Capability::DEPTH_CLAMP;
    case 4: return Capability::DEPTH_TEST;
    case 5: return Capability::DITHER;
    case 6: return Capability::FRAMEBUFFER_SRGB;
    case 7: return Capability::LINE_SMOOTH;
    case 8: return Capability::MULTISAMPLE;
    case 9: return Capability::POLYGON_OFFSET_FILL;
    case 10: return Capability::POLYGON_OFFSET_LINE;
    case 11: return Capability::POLYGON_OFFSET_POINT;
    case 12: return Capability::POLYGON_SMOOTH;
    case 13: return Capability::PRIMITIVE_RESTART;
    case 14: return Capability::RASTERIZER_DISCARD;
    case 15: return Capability::SAMPLE_ALPHA_TO_COVERAGE;
    case 16: return Capability::SAMPLE_ALPHA_TO_ONE;
    case 17: return Capability::SAMPLE_COVERAGE;
    case 18: return Capability::SAMPLE_SHADING;
    case 19: return Capability::SAMPLE_MASK;
    case 20: return Capability::SCISSOR_TEST;
    case 21: return Capability::STENCIL_TEST;
    case 22: return Capability::TEXTURE_CUBE_MAP_SEAMLESS;
    case 23: return Capability::PROGRAM_POINT_SIZE;
    default: return Capability::INVALID;
  }
}

enum class DepthFunction {
  INVALID = -1,
  NEVER = GL_NEVER,
  LESS = GL_LESS,
  EQUAL = GL_EQUAL,
  LEQUAL = GL_LEQUAL,
  GREATER = GL_GREATER,
  NOTEQUAL = GL_NOTEQUAL,
  GEQUAL = GL_GEQUAL,
  ALWAYS = GL_ALWAYS,
};

constexpr GLuint NUM_DEPTH_FUNCTIONS = 8;

constexpr GLenum toGLenum(DepthFunction value) {
  return static_cast<GLenum>(value);
}

constexpr GLint toGLint(DepthFunction value) {
  return static_cast<GLint>(value);
}

inline GLuint toIndex(DepthFunction value) {
  switch (value) {
    case DepthFunction::NEVER: return 0;
    case DepthFunction::LESS: return 1;
    case DepthFunction::EQUAL: return 2;
    case DepthFunction::LEQUAL: return 3;
    case DepthFunction::GREATER: return 4;
    case DepthFunction::NOTEQUAL: return 5;
    case DepthFunction::GEQUAL: return 6;
    case DepthFunction::ALWAYS: return 7;
    case DepthFunction::INVALID:
    default:
      throw std::runtime_error("Invalid DepthFunction");
  }
}

inline DepthFunction fromDepthFunctionIndex(GLint index) {
  switch (index) {
    case 0: return DepthFunction::NEVER;
    case 1: return DepthFunction::LESS;
    case 2: return DepthFunction::EQUAL;
    case 3: return DepthFunction::LEQUAL;
    case 4: return DepthFunction::GREATER;
    case 5: return DepthFunction::NOTEQUAL;
    case 6: return DepthFunction::GEQUAL;
    case 7: return DepthFunction::ALWAYS;
    default: return DepthFunction::INVALID;
  }
}

enum class DrawPrimitive {
  INVALID = -1,
  POINTS = GL_POINTS,
  LINE_STRIP = GL_LINE_STRIP,
  LINE_LOOP = GL_LINE_LOOP,
  LINES = GL_LINES,
  LINE_STRIP_ADJACENCY = GL_LINE_STRIP_ADJACENCY,
  LINES_ADJACENCY = GL_LINES_ADJACENCY,
  TRIANGLE_STRIP = GL_TRIANGLE_STRIP,
  TRIANGLE_FAN = GL_TRIANGLE_FAN,
  TRIANGLES = GL_TRIANGLES,
  TRIANGLE_STRIP_ADJACENCY = GL_TRIANGLE_STRIP_ADJACENCY,
  TRIANGLES_ADJACENCY = GL_TRIANGLES_ADJACENCY,
  PATCHES = GL_PATCHES,
};

constexpr GLuint NUM_DRAW_PRIMITIVES = 12;

constexpr GLenum toGLenum(DrawPrimitive value) {
  return static_cast<GLenum>(value);
}

constexpr GLint toGLint(DrawPrimitive value) {
  return static_cast<GLint>(value);
}

inline GLuint toIndex(DrawPrimitive value) {
  switch (value) {
    case DrawPrimitive::POINTS: return 0;
    case DrawPrimitive::LINE_STRIP: return 1;
    case DrawPrimitive::LINE_LOOP: return 2;
    case DrawPrimitive::LINES: return 3;
    case DrawPrimitive::LINE_STRIP_ADJACENCY: return 4;
    case DrawPrimitive::LINES_ADJACENCY: return 5;
    case DrawPrimitive::TRIANGLE_STRIP: return 6;
    case DrawPrimitive::TRIANGLE_FAN: return 7;
    case DrawPrimitive::TRIANGLES: return 8;
    case DrawPrimitive::TRIANGLE_STRIP_ADJACENCY: return 9;
    case DrawPrimitive::TRIANGLES_ADJACENCY: return 10;
    case DrawPrimitive::PATCHES: return 11;
    case DrawPrimitive::INVALID:
    default:
      throw std::runtime_error("Invalid DrawPrimitive");
  }
}

inline DrawPrimitive fromDrawPrimitiveIndex(GLint index) {
  switch (index) {
    case 0: return DrawPrimitive::POINTS;
    case 1: return DrawPrimitive::LINE_STRIP;
    case 2: return DrawPrimitive::LINE_LOOP;
    case 3: return DrawPrimitive::LINES;
    case 4: return DrawPrimitive::LINE_STRIP_ADJACENCY;
    case 5: return DrawPrimitive::LINES_ADJACENCY;
    case 6: return DrawPrimitive::TRIANGLE_STRIP;
    case 7: return DrawPrimitive::TRIANGLE_FAN;
    case 8: return DrawPrimitive::TRIANGLES;
    case 9: return DrawPrimitive::TRIANGLE_STRIP_ADJACENCY;
    case 10: return DrawPrimitive::TRIANGLES_ADJACENCY;
    case 11: return DrawPrimitive::PATCHES;
    default: return DrawPrimitive::INVALID;
  }
}

enum class ImageDataType {
  INVALID = -1,
  UNSIGNED_BYTE = GL_UNSIGNED_BYTE,
  BYTE = GL_BYTE,
  UNSIGNED_SHORT = GL_UNSIGNED_SHORT,
  SHORT = GL_SHORT,
  UNSIGNED_INT = GL_UNSIGNED_INT,
  INT = GL_INT,
  FLOAT = GL_FLOAT,
  UNSIGNED_BYTE_3_3_2 = GL_UNSIGNED_BYTE_3_3_2,
  UNSIGNED_BYTE_2_3_3_REV = GL_UNSIGNED_BYTE_2_3_3_REV,
  UNSIGNED_SHORT_5_6_5 = GL_UNSIGNED_SHORT_5_6_5,
  UNSIGNED_SHORT_5_6_5_REV = GL_UNSIGNED_SHORT_5_6_5_REV,
  UNSIGNED_SHORT_4_4_4_4 = GL_UNSIGNED_SHORT_4_4_4_4,
  UNSIGNED_SHORT_4_4_4_4_REV = GL_UNSIGNED_SHORT_4_4_4_4_REV,
  UNSIGNED_SHORT_5_5_5_1 = GL_UNSIGNED_SHORT_5_5_5_1,
  UNSIGNED_SHORT_1_5_5_5_REV = GL_UNSIGNED_SHORT_1_5_5_5_REV,
  UNSIGNED_INT_8_8_8_8 = GL_UNSIGNED_INT_8_8_8_8,
  UNSIGNED_INT_8_8_8_8_REV = GL_UNSIGNED_INT_8_8_8_8_REV,
  UNSIGNED_INT_10_10_10_2 = GL_UNSIGNED_INT_10_10_10_2,
  UNSIGNED_INT_2_10_10_10_REV = GL_UNSIGNED_INT_2_10_10_10_REV,
};

constexpr GLuint NUM_IMAGE_DATA_TYPES = 19;

constexpr GLenum toGLenum(ImageDataType value) {
  return static_cast<GLenum>(value);
}

constexpr GLint toGLint(ImageDataType value) {
  return static_cast<GLint>(value);
}

inline GLuint toIndex(ImageDataType value) {
  switch (value) {
    case ImageDataType::UNSIGNED_BYTE: return 0;
    case ImageDataType::BYTE: return 1;
    case ImageDataType::UNSIGNED_SHORT: return 2;
    case ImageDataType::SHORT: return 3;
    case ImageDataType::UNSIGNED_INT: return 4;
    case ImageDataType::INT: return 5;
    case ImageDataType::FLOAT: return 6;
    case ImageDataType::UNSIGNED_BYTE_3_3_2: return 7;
    case ImageDataType::UNSIGNED_BYTE_2_3_3_REV: return 8;
    case ImageDataType::UNSIGNED_SHORT_5_6_5: return 9;
    case ImageDataType::UNSIGNED_SHORT_5_6_5_REV: return 10;
    case ImageDataType::UNSIGNED_SHORT_4_4_4_4: return 11;
    case ImageDataType::UNSIGNED_SHORT_4_4_4_4_REV: return 12;
    case ImageDataType::UNSIGNED_SHORT_5_5_5_1: return 13;
    case ImageDataType::UNSIGNED_SHORT_1_5_5_5_REV: return 14;
    case ImageDataType::UNSIGNED_INT_8_8_8_8: return 15;
    case ImageDataType::UNSIGNED_INT_8_8_8_8_REV: return 16;
    case ImageDataType::UNSIGNED_INT_10_10_10_2: return 17;
    case ImageDataType::UNSIGNED_INT_2_10_10_10_REV: return 18;
    case ImageDataType::INVALID:
    default:
      throw std::runtime_error("Invalid ImageDataType");
  }
}

inline ImageDataType fromImageDataTypeIndex(GLint index) {
  switch (index) {
    case 0: return ImageDataType::UNSIGNED_BYTE;
    case 1: return ImageDataType::BYTE;
    case 2: return ImageDataType::UNSIGNED_SHORT;
    case 3: return ImageDataType::SHORT;
    case 4: return ImageDataType::UNSIGNED_INT;
    case 5: return ImageDataType::INT;
    case 6: return ImageDataType::FLOAT;
    case 7: return ImageDataType::UNSIGNED_BYTE_3_3_2;
    case 8: return ImageDataType::UNSIGNED_BYTE_2_3_3_REV;
    case 9: return ImageDataType::UNSIGNED_SHORT_5_6_5;
    case 10: return ImageDataType::UNSIGNED_SHORT_5_6_5_REV;
    case 11: return ImageDataType::UNSIGNED_SHORT_4_4_4_4;
    case 12: return ImageDataType::UNSIGNED_SHORT_4_4_4_4_REV;
    case 13: return ImageDataType::UNSIGNED_SHORT_5_5_5_1;
    case 14: return ImageDataType::UNSIGNED_SHORT_1_5_5_5_REV;
    case 15: return ImageDataType::UNSIGNED_INT_8_8_8_8;
    case 16: return ImageDataType::UNSIGNED_INT_8_8_8_8_REV;
    case 17: return ImageDataType::UNSIGNED_INT_10_10_10_2;
    case 18: return ImageDataType::UNSIGNED_INT_2_10_10_10_REV;
    default: return ImageDataType::INVALID;
  }
}

enum class ImageFormat {
  INVALID = -1,
  RED = GL_RED,
  RG = GL_RG,
  RGB = GL_RGB,
  BGR = GL_BGR,
  RGBA = GL_RGBA,
  BGRA = GL_BGRA,
  RED_INTEGER = GL_RED_INTEGER,
  RG_INTEGER = GL_RG_INTEGER,
  RGB_INTEGER = GL_RGB_INTEGER,
  BGR_INTEGER = GL_BGR_INTEGER,
  RGBA_INTEGER = GL_RGBA_INTEGER,
  BGRA_INTEGER = GL_BGRA_INTEGER,
  STENCIL_INDEX = GL_STENCIL_INDEX,
  DEPTH_COMPONENT = GL_DEPTH_COMPONENT,
  DEPTH_STENCIL = GL_DEPTH_STENCIL,
};

constexpr GLuint NUM_IMAGE_FORMATS = 15;

constexpr GLenum toGLenum(ImageFormat value) {
  return static_cast<GLenum>(value);
}

constexpr GLint toGLint(ImageFormat value) {
  return static_cast<GLint>(value);
}

inline GLuint toIndex(ImageFormat value) {
  switch (value) {
    case ImageFormat::RED: return 0;
    case ImageFormat::RG: return 1;
    case ImageFormat::RGB: return 2;
    case ImageFormat::BGR: return 3;
    case ImageFormat::RGBA: return 4;
    case ImageFormat::BGRA: return 5;
    case ImageFormat::RED_INTEGER: return 6;
    case ImageFormat::RG_INTEGER: return 7;
    case ImageFormat::RGB_INTEGER: return 8;
    case ImageFormat::BGR_INTEGER: return 9;
    case ImageFormat::RGBA_INTEGER: return 10;
    case ImageFormat::BGRA_INTEGER: return 11;
    case ImageFormat::STENCIL_INDEX: return 12;
    case ImageFormat::DEPTH_COMPONENT: return 13;
    case ImageFormat::DEPTH_STENCIL: return 14;
    case ImageFormat::INVALID:
    default:
      throw std::runtime_error("Invalid ImageFormat");
  }
}

inline ImageFormat fromImageFormatIndex(GLint index) {
  switch (index) {
    case 0: return ImageFormat::RED;
    case 1: return ImageFormat::RG;
    case 2: return ImageFormat::RGB;
    case 3: return ImageFormat::BGR;
    case 4: return ImageFormat::RGBA;
    case 5: return ImageFormat::BGRA;
    case 6: return ImageFormat::RED_INTEGER;
    case 7: return ImageFormat::RG_INTEGER;
    case 8: return ImageFormat::RGB_INTEGER;
    case 9: return ImageFormat::BGR_INTEGER;
    case 10: return ImageFormat::RGBA_INTEGER;
    case 11: return ImageFormat::BGRA_INTEGER;
    case 12: return ImageFormat::STENCIL_INDEX;
    case 13: return ImageFormat::DEPTH_COMPONENT;
    case 14: return ImageFormat::DEPTH_STENCIL;
    default: return ImageFormat::INVALID;
  }
}

enum class IndexDataType {
  INVALID = -1,
  UNSIGNED_BYTE = GL_UNSIGNED_BYTE,
  UNSIGNED_SHORT = GL_UNSIGNED_SHORT,
  UNSIGNED_INT = GL_UNSIGNED_INT,
};

constexpr GLuint NUM_INDEX_DATA_TYPES = 3;

constexpr GLenum toGLenum(IndexDataType value) {
  return static_cast<GLenum>(value);
}

constexpr GLint toGLint(IndexDataType value) {
  return static_cast<GLint>(value);
}

inline GLuint toIndex(IndexDataType value) {
  switch (value) {
    case IndexDataType::UNSIGNED_BYTE: return 0;
    case IndexDataType::UNSIGNED_SHORT: return 1;
    case IndexDataType::UNSIGNED_INT: return 2;
    case IndexDataType::INVALID:
    default:
      throw std::runtime_error("Invalid IndexDataType");
  }
}

inline IndexDataType fromIndexDataTypeIndex(GLint index) {
  switch (index) {
    case 0: return IndexDataType::UNSIGNED_BYTE;
    case 1: return IndexDataType::UNSIGNED_SHORT;
    case 2: return IndexDataType::UNSIGNED_INT;
    default: return IndexDataType::INVALID;
  }
}

enum class ShaderType {
  INVALID = -1,
  VERTEX_SHADER = GL_VERTEX_SHADER,
  TESS_CONTROL_SHADER = GL_TESS_CONTROL_SHADER,
  TESS_EVALUATION_SHADER = GL_TESS_EVALUATION_SHADER,
  GEOMETRY_SHADER = GL_GEOMETRY_SHADER,
  FRAGMENT_SHADER = GL_FRAGMENT_SHADER,
};

constexpr GLuint NUM_SHADER_TYPES = 5;

constexpr GLenum toGLenum(ShaderType value) {
  return static_cast<GLenum>(value);
}

constexpr GLint toGLint(ShaderType value) {
  return static_cast<GLint>(value);
}

inline GLuint toIndex(ShaderType value) {
  switch (value) {
    case ShaderType::VERTEX_SHADER: return 0;
    case ShaderType::TESS_CONTROL_SHADER: return 1;
    case ShaderType::TESS_EVALUATION_SHADER: return 2;
    case ShaderType::GEOMETRY_SHADER: return 3;
    case ShaderType::FRAGMENT_SHADER: return 4;
    case ShaderType::INVALID:
    default:
      throw std::runtime_error("Invalid ShaderType");
  }
}

inline ShaderType fromShaderTypeIndex(GLint index) {
  switch (index) {
    case 0: return ShaderType::VERTEX_SHADER;
    case 1: return ShaderType::TESS_CONTROL_SHADER;
    case 2: return ShaderType::TESS_EVALUATION_SHADER;
    case 3: return ShaderType::GEOMETRY_SHADER;
    case 4: return ShaderType::FRAGMENT_SHADER;
    default: return ShaderType::INVALID;
  }
}

enum class TextureCompareFunc {
  INVALID = -1,
  LEQUAL = GL_LEQUAL,
  GEQUAL = GL_GEQUAL,
  LESS = GL_LESS,
  GREATER = GL_GREATER,
  EQUAL = GL_EQUAL,
  NOTEQUAL = GL_NOTEQUAL,
  ALWAYS = GL_ALWAYS,
  NEVER = GL_NEVER,
};

constexpr GLuint NUM_TEXTURE_COMPARE_FUNCS = 8;

constexpr GLenum toGLenum(TextureCompareFunc value) {
  return static_cast<GLenum>(value);
}

constexpr GLint toGLint(TextureCompareFunc value) {
  return static_cast<GLint>(value);
}

inline GLuint toIndex(TextureCompareFunc value) {
  switch (value) {
    case TextureCompareFunc::LEQUAL: return 0;
    case TextureCompareFunc::GEQUAL: return 1;
    case TextureCompareFunc::LESS: return 2;
    case TextureCompareFunc::GREATER: return 3;
    case TextureCompareFunc::EQUAL: return 4;
    case TextureCompareFunc::NOTEQUAL: return 5;
    case TextureCompareFunc::ALWAYS: return 6;
    case TextureCompareFunc::NEVER: return 7;
    case TextureCompareFunc::INVALID:
    default:
      throw std::runtime_error("Invalid TextureCompareFunc");
  }
}

inline TextureCompareFunc fromTextureCompareFuncIndex(GLint index) {
  switch (index) {
    case 0: return TextureCompareFunc::LEQUAL;
    case 1: return TextureCompareFunc::GEQUAL;
    case 2: return TextureCompareFunc::LESS;
    case 3: return TextureCompareFunc::GREATER;
    case 4: return TextureCompareFunc::EQUAL;
    case 5: return TextureCompareFunc::NOTEQUAL;
    case 6: return TextureCompareFunc::ALWAYS;
    case 7: return TextureCompareFunc::NEVER;
    default: return TextureCompareFunc::INVALID;
  }
}

enum class TextureCompareMode {
  INVALID = -1,
  COMPARE_REF_TO_TEXTURE = GL_COMPARE_REF_TO_TEXTURE,
  NONE = GL_NONE,
};

constexpr GLuint NUM_TEXTURE_COMPARE_MODES = 2;

constexpr GLenum toGLenum(TextureCompareMode value) {
  return static_cast<GLenum>(value);
}

constexpr GLint toGLint(TextureCompareMode value) {
  return static_cast<GLint>(value);
}

inline GLuint toIndex(TextureCompareMode value) {
  switch (value) {
    case TextureCompareMode::COMPARE_REF_TO_TEXTURE: return 0;
    case TextureCompareMode::NONE: return 1;
    case TextureCompareMode::INVALID:
    default:
      throw std::runtime_error("Invalid TextureCompareMode");
  }
}

inline TextureCompareMode fromTextureCompareModeIndex(GLint index) {
  switch (index) {
    case 0: return TextureCompareMode::COMPARE_REF_TO_TEXTURE;
    case 1: return TextureCompareMode::NONE;
    default: return TextureCompareMode::INVALID;
  }
}

enum class TextureFormat {
  INVALID = -1,
  DEPTH_COMPONENT = GL_DEPTH_COMPONENT,
  DEPTH_STENCIL = GL_DEPTH_STENCIL,
  RED = GL_RED,
  RG = GL_RG,
  RGB = GL_RGB,
  RGBA = GL_RGBA,
  R8 = GL_R8,
  R8_SNORM = GL_R8_SNORM,
  R16 = GL_R16,
  R16_SNORM = GL_R16_SNORM,
  RG8 = GL_RG8,
  RG8_SNORM = GL_RG8_SNORM,
  RG16 = GL_RG16,
  RG16_SNORM = GL_RG16_SNORM,
  R3_G3_B2 = GL_R3_G3_B2,
  RGB4 = GL_RGB4,
  RGB5 = GL_RGB5,
  RGB8 = GL_RGB8,
  RGB8_SNORM = GL_RGB8_SNORM,
  RGB10 = GL_RGB10,
  RGB12 = GL_RGB12,
  RGB16_SNORM = GL_RGB16_SNORM,
  RGBA2 = GL_RGBA2,
  RGBA4 = GL_RGBA4,
  RGB5_A1 = GL_RGB5_A1,
  RGBA8 = GL_RGBA8,
  RGBA8_SNORM = GL_RGBA8_SNORM,
  RGB10_A2 = GL_RGB10_A2,
  RGB10_A2UI = GL_RGB10_A2UI,
  RGBA12 = GL_RGBA12,
  RGBA16 = GL_RGBA16,
  SRGB8 = GL_SRGB8,
  SRGB8_ALPHA8 = GL_SRGB8_ALPHA8,
  R16F = GL_R16F,
  RG16F = GL_RG16F,
  RGB16F = GL_RGB16F,
  RGBA16F = GL_RGBA16F,
  R32F = GL_R32F,
  RG32F = GL_RG32F,
  RGB32F = GL_RGB32F,
  RGBA32F = GL_RGBA32F,
  R11F_G11F_B10F = GL_R11F_G11F_B10F,
  RGB9_E5 = GL_RGB9_E5,
  R8I = GL_R8I,
  R8UI = GL_R8UI,
  R16I = GL_R16I,
  R16UI = GL_R16UI,
  R32I = GL_R32I,
  R32UI = GL_R32UI,
  RG8I = GL_RG8I,
  RG8UI = GL_RG8UI,
  RG16I = GL_RG16I,
  RG16UI = GL_RG16UI,
  RG32I = GL_RG32I,
  RG32UI = GL_RG32UI,
  RGB8I = GL_RGB8I,
  RGB8UI = GL_RGB8UI,
  RGB16I = GL_RGB16I,
  RGB16UI = GL_RGB16UI,
  RGB32I = GL_RGB32I,
  RGB32UI = GL_RGB32UI,
  RGBA8I = GL_RGBA8I,
  RGBA8UI = GL_RGBA8UI,
  RGBA16I = GL_RGBA16I,
  RGBA16UI = GL_RGBA16UI,
  RGBA32I = GL_RGBA32I,
  RGBA32UI = GL_RGBA32UI,
  COMPRESSED_RED = GL_COMPRESSED_RED,
  COMPRESSED_RG = GL_COMPRESSED_RG,
  COMPRESSED_RGB = GL_COMPRESSED_RGB,
  COMPRESSED_RGBA = GL_COMPRESSED_RGBA,
  COMPRESSED_SRGB = GL_COMPRESSED_SRGB,
  COMPRESSED_SRGB_ALPHA = GL_COMPRESSED_SRGB_ALPHA,
  COMPRESSED_RED_RGTC1 = GL_COMPRESSED_RED_RGTC1,
  COMPRESSED_SIGNED_RED_RGTC1 = GL_COMPRESSED_SIGNED_RED_RGTC1,
  COMPRESSED_RG_RGTC2 = GL_COMPRESSED_RG_RGTC2,
  COMPRESSED_SIGNED_RG_RGTC2 = GL_COMPRESSED_SIGNED_RG_RGTC2,
};

constexpr GLuint NUM_TEXTURE_FORMATS = 77;

constexpr GLenum toGLenum(TextureFormat value) {
  return static_cast<GLenum>(value);
}

constexpr GLint toGLint(TextureFormat value) {
  return static_cast<GLint>(value);
}

inline GLuint toIndex(TextureFormat value) {
  switch (value) {
    case TextureFormat::DEPTH_COMPONENT: return 0;
    case TextureFormat::DEPTH_STENCIL: return 1;
    case TextureFormat::RED: return 2;
    case TextureFormat::RG: return 3;
    case TextureFormat::RGB: return 4;
    case TextureFormat::RGBA: return 5;
    case TextureFormat::R8: return 6;
    case TextureFormat::R8_SNORM: return 7;
    case TextureFormat::R16: return 8;
    case TextureFormat::R16_SNORM: return 9;
    case TextureFormat::RG8: return 10;
    case TextureFormat::RG8_SNORM: return 11;
    case TextureFormat::RG16: return 12;
    case TextureFormat::RG16_SNORM: return 13;
    case TextureFormat::R3_G3_B2: return 14;
    case TextureFormat::RGB4: return 15;
    case TextureFormat::RGB5: return 16;
    case TextureFormat::RGB8: return 17;
    case TextureFormat::RGB8_SNORM: return 18;
    case TextureFormat::RGB10: return 19;
    case TextureFormat::RGB12: return 20;
    case TextureFormat::RGB16_SNORM: return 21;
    case TextureFormat::RGBA2: return 22;
    case TextureFormat::RGBA4: return 23;
    case TextureFormat::RGB5_A1: return 24;
    case TextureFormat::RGBA8: return 25;
    case TextureFormat::RGBA8_SNORM: return 26;
    case TextureFormat::RGB10_A2: return 27;
    case TextureFormat::RGB10_A2UI: return 28;
    case TextureFormat::RGBA12: return 29;
    case TextureFormat::RGBA16: return 30;
    case TextureFormat::SRGB8: return 31;
    case TextureFormat::SRGB8_ALPHA8: return 32;
    case TextureFormat::R16F: return 33;
    case TextureFormat::RG16F: return 34;
    case TextureFormat::RGB16F: return 35;
    case TextureFormat::RGBA16F: return 36;
    case TextureFormat::R32F: return 37;
    case TextureFormat::RG32F: return 38;
    case TextureFormat::RGB32F: return 39;
    case TextureFormat::RGBA32F: return 40;
    case TextureFormat::R11F_G11F_B10F: return 41;
    case TextureFormat::RGB9_E5: return 42;
    case TextureFormat::R8I: return 43;
    case TextureFormat::R8UI: return 44;
    case TextureFormat::R16I: return 45;
    case TextureFormat::R16UI: return 46;
    case TextureFormat::R32I: return 47;
    case TextureFormat::R32UI: return 48;
    case TextureFormat::RG8I: return 49;
    case TextureFormat::RG8UI: return 50;
    case TextureFormat::RG16I: return 51;
    case TextureFormat::RG16UI: return 52;
    case TextureFormat::RG32I: return 53;
    case TextureFormat::RG32UI: return 54;
    case TextureFormat::RGB8I: return 55;
    case TextureFormat::RGB8UI: return 56;
    case TextureFormat::RGB16I: return 57;
    case TextureFormat::RGB16UI: return 58;
    case TextureFormat::RGB32I: return 59;
    case TextureFormat::RGB32UI: return 60;
    case TextureFormat::RGBA8I: return 61;
    case TextureFormat::RGBA8UI: return 62;
    case TextureFormat::RGBA16I: return 63;
    case TextureFormat::RGBA16UI: return 64;
    case TextureFormat::RGBA32I: return 65;
    case TextureFormat::RGBA32UI: return 66;
    case TextureFormat::COMPRESSED_RED: return 67;
    case TextureFormat::COMPRESSED_RG: return 68;
    case TextureFormat::COMPRESSED_RGB: return 69;
    case TextureFormat::COMPRESSED_RGBA: return 70;
    case TextureFormat::COMPRESSED_SRGB: return 71;
    case TextureFormat::COMPRESSED_SRGB_ALPHA: return 72;
    case TextureFormat::COMPRESSED_RED_RGTC1: return 73;
    case TextureFormat::COMPRESSED_SIGNED_RED_RGTC1: return 74;
    case TextureFormat::COMPRESSED_RG_RGTC2: return 75;
    case TextureFormat::COMPRESSED_SIGNED_RG_RGTC2: return 76;
    case TextureFormat::INVALID:
    default:
      throw std::runtime_error("Invalid TextureFormat");
  }
}

inline TextureFormat fromTextureFormatIndex(GLint index) {
  switch (index) {
    case 0: return TextureFormat::DEPTH_COMPONENT;
    case 1: return TextureFormat::DEPTH_STENCIL;
    case 2: return TextureFormat::RED;
    case 3: return TextureFormat::RG;
    case 4: return TextureFormat::RGB;
    case 5: return TextureFormat::RGBA;
    case 6: return TextureFormat::R8;
    case 7: return TextureFormat::R8_SNORM;
    case 8: return TextureFormat::R16;
    case 9: return TextureFormat::R16_SNORM;
    case 10: return TextureFormat::RG8;
    case 11: return TextureFormat::RG8_SNORM;
    case 12: return TextureFormat::RG16;
    case 13: return TextureFormat::RG16_SNORM;
    case 14: return TextureFormat::R3_G3_B2;
    case 15: return TextureFormat::RGB4;
    case 16: return TextureFormat::RGB5;
    case 17: return TextureFormat::RGB8;
    case 18: return TextureFormat::RGB8_SNORM;
    case 19: return TextureFormat::RGB10;
    case 20: return TextureFormat::RGB12;
    case 21: return TextureFormat::RGB16_SNORM;
    case 22: return TextureFormat::RGBA2;
    case 23: return TextureFormat::RGBA4;
    case 24: return TextureFormat::RGB5_A1;
    case 25: return TextureFormat::RGBA8;
    case 26: return TextureFormat::RGBA8_SNORM;
    case 27: return TextureFormat::RGB10_A2;
    case 28: return TextureFormat::RGB10_A2UI;
    case 29: return TextureFormat::RGBA12;
    case 30: return TextureFormat::RGBA16;
    case 31: return TextureFormat::SRGB8;
    case 32: return TextureFormat::SRGB8_ALPHA8;
    case 33: return TextureFormat::R16F;
    case 34: return TextureFormat::RG16F;
    case 35: return TextureFormat::RGB16F;
    case 36: return TextureFormat::RGBA16F;
    case 37: return TextureFormat::R32F;
    case 38: return TextureFormat::RG32F;
    case 39: return TextureFormat::RGB32F;
    case 40: return TextureFormat::RGBA32F;
    case 41: return TextureFormat::R11F_G11F_B10F;
    case 42: return TextureFormat::RGB9_E5;
    case 43: return TextureFormat::R8I;
    case 44: return TextureFormat::R8UI;
    case 45: return TextureFormat::R16I;
    case 46: return TextureFormat::R16UI;
    case 47: return TextureFormat::R32I;
    case 48: return TextureFormat::R32UI;
    case 49: return TextureFormat::RG8I;
    case 50: return TextureFormat::RG8UI;
    case 51: return TextureFormat::RG16I;
    case 52: return TextureFormat::RG16UI;
    case 53: return TextureFormat::RG32I;
    case 54: return TextureFormat::RG32UI;
    case 55: return TextureFormat::RGB8I;
    case 56: return TextureFormat::RGB8UI;
    case 57: return TextureFormat::RGB16I;
    case 58: return TextureFormat::RGB16UI;
    case 59: return TextureFormat::RGB32I;
    case 60: return TextureFormat::RGB32UI;
    case 61: return TextureFormat::RGBA8I;
    case 62: return TextureFormat::RGBA8UI;
    case 63: return TextureFormat::RGBA16I;
    case 64: return TextureFormat::RGBA16UI;
    case 65: return TextureFormat::RGBA32I;
    case 66: return TextureFormat::RGBA32UI;
    case 67: return TextureFormat::COMPRESSED_RED;
    case 68: return TextureFormat::COMPRESSED_RG;
    case 69: return TextureFormat::COMPRESSED_RGB;
    case 70: return TextureFormat::COMPRESSED_RGBA;
    case 71: return TextureFormat::COMPRESSED_SRGB;
    case 72: return TextureFormat::COMPRESSED_SRGB_ALPHA;
    case 73: return TextureFormat::COMPRESSED_RED_RGTC1;
    case 74: return TextureFormat::COMPRESSED_SIGNED_RED_RGTC1;
    case 75: return TextureFormat::COMPRESSED_RG_RGTC2;
    case 76: return TextureFormat::COMPRESSED_SIGNED_RG_RGTC2;
    default: return TextureFormat::INVALID;
  }
}

enum class TextureMagFilter {
  INVALID = -1,
  NEAREST = GL_NEAREST,
  LINEAR = GL_LINEAR,
};

constexpr GLuint NUM_TEXTURE_MAG_FILTERS = 2;

constexpr GLenum toGLenum(TextureMagFilter value) {
  return static_cast<GLenum>(value);
}

constexpr GLint toGLint(TextureMagFilter value) {
  return static_cast<GLint>(value);
}

inline GLuint toIndex(TextureMagFilter value) {
  switch (value) {
    case TextureMagFilter::NEAREST: return 0;
    case TextureMagFilter::LINEAR: return 1;
    case TextureMagFilter::INVALID:
    default:
      throw std::runtime_error("Invalid TextureMagFilter");
  }
}

inline TextureMagFilter fromTextureMagFilterIndex(GLint index) {
  switch (index) {
    case 0: return TextureMagFilter::NEAREST;
    case 1: return TextureMagFilter::LINEAR;
    default: return TextureMagFilter::INVALID;
  }
}

enum class TextureMinFilter {
  INVALID = -1,
  NEAREST = GL_NEAREST,
  LINEAR = GL_LINEAR,
  NEAREST_MIPMAP_NEAREST = GL_NEAREST_MIPMAP_NEAREST,
  LINEAR_MIPMAP_NEAREST = GL_LINEAR_MIPMAP_NEAREST,
  NEAREST_MIPMAP_LINEAR = GL_NEAREST_MIPMAP_LINEAR,
  LINEAR_MIPMAP_LINEAR = GL_LINEAR_MIPMAP_LINEAR,
};

constexpr GLuint NUM_TEXTURE_MIN_FILTERS = 6;

constexpr GLenum toGLenum(TextureMinFilter value) {
  return static_cast<GLenum>(value);
}

constexpr GLint toGLint(TextureMinFilter value) {
  return static_cast<GLint>(value);
}

inline GLuint toIndex(TextureMinFilter value) {
  switch (value) {
    case TextureMinFilter::NEAREST: return 0;
    case TextureMinFilter::LINEAR: return 1;
    case TextureMinFilter::NEAREST_MIPMAP_NEAREST: return 2;
    case TextureMinFilter::LINEAR_MIPMAP_NEAREST: return 3;
    case TextureMinFilter::NEAREST_MIPMAP_LINEAR: return 4;
    case TextureMinFilter::LINEAR_MIPMAP_LINEAR: return 5;
    case TextureMinFilter::INVALID:
    default:
      throw std::runtime_error("Invalid TextureMinFilter");
  }
}

inline TextureMinFilter fromTextureMinFilterIndex(GLint index) {
  switch (index) {
    case 0: return TextureMinFilter::NEAREST;
    case 1: return TextureMinFilter::LINEAR;
    case 2: return TextureMinFilter::NEAREST_MIPMAP_NEAREST;
    case 3: return TextureMinFilter::LINEAR_MIPMAP_NEAREST;
    case 4: return TextureMinFilter::NEAREST_MIPMAP_LINEAR;
    case 5: return TextureMinFilter::LINEAR_MIPMAP_LINEAR;
    default: return TextureMinFilter::INVALID;
  }
}

enum class TextureSwizzle {
  INVALID = -1,
  RED = GL_RED,
  GREEN = GL_GREEN,
  BLUE = GL_BLUE,
  ALPHA = GL_ALPHA,
  ZERO = GL_ZERO,
  ONE = GL_ONE,
};

constexpr GLuint NUM_TEXTURE_SWIZZLES = 6;

constexpr GLenum toGLenum(TextureSwizzle value) {
  return static_cast<GLenum>(value);
}

constexpr GLint toGLint(TextureSwizzle value) {
  return static_cast<GLint>(value);
}

inline GLuint toIndex(TextureSwizzle value) {
  switch (value) {
    case TextureSwizzle::RED: return 0;
    case TextureSwizzle::GREEN: return 1;
    case TextureSwizzle::BLUE: return 2;
    case TextureSwizzle::ALPHA: return 3;
    case TextureSwizzle::ZERO: return 4;
    case TextureSwizzle::ONE: return 5;
    case TextureSwizzle::INVALID:
    default:
      throw std::runtime_error("Invalid TextureSwizzle");
  }
}

inline TextureSwizzle fromTextureSwizzleIndex(GLint index) {
  switch (index) {
    case 0: return TextureSwizzle::RED;
    case 1: return TextureSwizzle::GREEN;
    case 2: return TextureSwizzle::BLUE;
    case 3: return TextureSwizzle::ALPHA;
    case 4: return TextureSwizzle::ZERO;
    case 5: return TextureSwizzle::ONE;
    default: return TextureSwizzle::INVALID;
  }
}

enum class TextureTarget {
  INVALID = -1,
  TEXTURE_1D = GL_TEXTURE_1D,
  PROXY_TEXTURE_1D = GL_PROXY_TEXTURE_1D,
  TEXTURE_2D = GL_TEXTURE_2D,
  PROXY_TEXTURE_2D = GL_PROXY_TEXTURE_2D,
  TEXTURE_3D = GL_TEXTURE_3D,
  PROXY_TEXTURE_3D = GL_PROXY_TEXTURE_3D,
  TEXTURE_1D_ARRAY = GL_TEXTURE_1D_ARRAY,
  PROXY_TEXTURE_1D_ARRAY = GL_PROXY_TEXTURE_1D_ARRAY,
  TEXTURE_2D_ARRAY = GL_TEXTURE_2D_ARRAY,
  PROXY_TEXTURE_2D_ARRAY = GL_PROXY_TEXTURE_2D_ARRAY,
  TEXTURE_RECTANGLE = GL_TEXTURE_RECTANGLE,
  PROXY_TEXTURE_RECTANGLE = GL_PROXY_TEXTURE_RECTANGLE,
  TEXTURE_CUBE_MAP_POSITIVE_X = GL_TEXTURE_CUBE_MAP_POSITIVE_X,
  TEXTURE_CUBE_MAP_NEGATIVE_X = GL_TEXTURE_CUBE_MAP_NEGATIVE_X,
  TEXTURE_CUBE_MAP_POSITIVE_Y = GL_TEXTURE_CUBE_MAP_POSITIVE_Y,
  TEXTURE_CUBE_MAP_NEGATIVE_Y = GL_TEXTURE_CUBE_MAP_NEGATIVE_Y,
  TEXTURE_CUBE_MAP_POSITIVE_Z = GL_TEXTURE_CUBE_MAP_POSITIVE_Z,
  TEXTURE_CUBE_MAP_NEGATIVE_Z = GL_TEXTURE_CUBE_MAP_NEGATIVE_Z,
  PROXY_TEXTURE_CUBE_MAP = GL_PROXY_TEXTURE_CUBE_MAP,
  TEXTURE_2D_MULTISAMPLE = GL_TEXTURE_2D_MULTISAMPLE,
  PROXY_TEXTURE_2D_MULTISAMPLE = GL_PROXY_TEXTURE_2D_MULTISAMPLE,
  TEXTURE_CUBE_MAP = GL_TEXTURE_CUBE_MAP,
  TEXTURE_BUFFER = GL_TEXTURE_BUFFER,
};

constexpr GLuint NUM_TEXTURE_TARGETS = 23;

constexpr GLenum toGLenum(TextureTarget value) {
  return static_cast<GLenum>(value);
}

constexpr GLint toGLint(TextureTarget value) {
  return static_cast<GLint>(value);
}

inline GLuint toIndex(TextureTarget value) {
  switch (value) {
    case TextureTarget::TEXTURE_1D: return 0;
    case TextureTarget::PROXY_TEXTURE_1D: return 1;
    case TextureTarget::TEXTURE_2D: return 2;
    case TextureTarget::PROXY_TEXTURE_2D: return 3;
    case TextureTarget::TEXTURE_3D: return 4;
    case TextureTarget::PROXY_TEXTURE_3D: return 5;
    case TextureTarget::TEXTURE_1D_ARRAY: return 6;
    case TextureTarget::PROXY_TEXTURE_1D_ARRAY: return 7;
    case TextureTarget::TEXTURE_2D_ARRAY: return 8;
    case TextureTarget::PROXY_TEXTURE_2D_ARRAY: return 9;
    case TextureTarget::TEXTURE_RECTANGLE: return 10;
    case TextureTarget::PROXY_TEXTURE_RECTANGLE: return 11;
    case TextureTarget::TEXTURE_CUBE_MAP_POSITIVE_X: return 12;
    case TextureTarget::TEXTURE_CUBE_MAP_NEGATIVE_X: return 13;
    case TextureTarget::TEXTURE_CUBE_MAP_POSITIVE_Y: return 14;
    case TextureTarget::TEXTURE_CUBE_MAP_NEGATIVE_Y: return 15;
    case TextureTarget::TEXTURE_CUBE_MAP_POSITIVE_Z: return 16;
    case TextureTarget::TEXTURE_CUBE_MAP_NEGATIVE_Z: return 17;
    case TextureTarget::PROXY_TEXTURE_CUBE_MAP: return 18;
    case TextureTarget::TEXTURE_2D_MULTISAMPLE: return 19;
    case TextureTarget::PROXY_TEXTURE_2D_MULTISAMPLE: return 20;
    case TextureTarget::TEXTURE_CUBE_MAP: return 21;
    case TextureTarget::TEXTURE_BUFFER: return 22;
    case TextureTarget::INVALID:
    default:
      throw std::runtime_error("Invalid TextureTarget");
  }
}

inline TextureTarget fromTextureTargetIndex(GLint index) {
  switch (index) {
    case 0: return TextureTarget::TEXTURE_1D;
    case 1: return TextureTarget::PROXY_TEXTURE_1D;
    case 2: return TextureTarget::TEXTURE_2D;
    case 3: return TextureTarget::PROXY_TEXTURE_2D;
    case 4: return TextureTarget::TEXTURE_3D;
    case 5: return TextureTarget::PROXY_TEXTURE_3D;
    case 6: return TextureTarget::TEXTURE_1D_ARRAY;
    case 7: return TextureTarget::PROXY_TEXTURE_1D_ARRAY;
    case 8: return TextureTarget::TEXTURE_2D_ARRAY;
    case 9: return TextureTarget::PROXY_TEXTURE_2D_ARRAY;
    case 10: return TextureTarget::TEXTURE_RECTANGLE;
    case 11: return TextureTarget::PROXY_TEXTURE_RECTANGLE;
    case 12: return TextureTarget::TEXTURE_CUBE_MAP_POSITIVE_X;
    case 13: return TextureTarget::TEXTURE_CUBE_MAP_NEGATIVE_X;
    case 14: return TextureTarget::TEXTURE_CUBE_MAP_POSITIVE_Y;
    case 15: return TextureTarget::TEXTURE_CUBE_MAP_NEGATIVE_Y;
    case 16: return TextureTarget::TEXTURE_CUBE_MAP_POSITIVE_Z;
    case 17: return TextureTarget::TEXTURE_CUBE_MAP_NEGATIVE_Z;
    case 18: return TextureTarget::PROXY_TEXTURE_CUBE_MAP;
    case 19: return TextureTarget::TEXTURE_2D_MULTISAMPLE;
    case 20: return TextureTarget::PROXY_TEXTURE_2D_MULTISAMPLE;
    case 21: return TextureTarget::TEXTURE_CUBE_MAP;
    case 22: return TextureTarget::TEXTURE_BUFFER;
    default: return TextureTarget::INVALID;
  }
}

enum class TextureWrap {
  INVALID = -1,
  CLAMP_TO_EDGE = GL_CLAMP_TO_EDGE,
  CLAMP_TO_BORDER = GL_CLAMP_TO_BORDER,
  MIRRORED_REPEAT = GL_MIRRORED_REPEAT,
  REPEAT = GL_REPEAT,
};

constexpr GLuint NUM_TEXTURE_WRAPS = 4;

constexpr GLenum toGLenum(TextureWrap value) {
  return static_cast<GLenum>(value);
}

constexpr GLint toGLint(TextureWrap value) {
  return static_cast<GLint>(value);
}

inline GLuint toIndex(TextureWrap value) {
  switch (value) {
    case TextureWrap::CLAMP_TO_EDGE: return 0;
    case TextureWrap::CLAMP_TO_BORDER: return 1;
    case TextureWrap::MIRRORED_REPEAT: return 2;
    case TextureWrap::REPEAT: return 3;
    case TextureWrap::INVALID:
    default:
      throw std::runtime_error("Invalid TextureWrap");
  }
}

inline TextureWrap fromTextureWrapIndex(GLint index) {
  switch (index) {
    case 0: return TextureWrap::CLAMP_TO_EDGE;
    case 1: return TextureWrap::CLAMP_TO_BORDER;
    case 2: return TextureWrap::MIRRORED_REPEAT;
    case 3: return TextureWrap::REPEAT;
    default: return TextureWrap::INVALID;
  }
}

enum class VertexDataType {
  INVALID = -1,
  BYTE = GL_BYTE,
  UNSIGNED_BYTE = GL_UNSIGNED_BYTE,
  SHORT = GL_SHORT,
  UNSIGNED_SHORT = GL_UNSIGNED_SHORT,
  INT = GL_INT,
  UNSIGNED_INT = GL_UNSIGNED_INT,
  HALF_FLOAT = GL_HALF_FLOAT,
  FLOAT = GL_FLOAT,
  DOUBLE = GL_DOUBLE,
  FIXED = GL_FIXED,
  INT_2_10_10_10_REV = GL_INT_2_10_10_10_REV,
  UNSIGNED_INT_2_10_10_10_REV = GL_UNSIGNED_INT_2_10_10_10_REV,
  UNSIGNED_INT_10F_11F_11F_REV = GL_UNSIGNED_INT_10F_11F_11F_REV,
};

constexpr GLuint NUM_VERTEX_DATA_TYPES = 13;

constexpr GLenum toGLenum(VertexDataType value) {
  return static_cast<GLenum>(value);
}

constexpr GLint toGLint(VertexDataType value) {
  return static_cast<GLint>(value);
}

inline GLuint toIndex(VertexDataType value) {
  switch (value) {
    case VertexDataType::BYTE: return 0;
    case VertexDataType::UNSIGNED_BYTE: return 1;
    case VertexDataType::SHORT: return 2;
    case VertexDataType::UNSIGNED_SHORT: return 3;
    case VertexDataType::INT: return 4;
    case VertexDataType::UNSIGNED_INT: return 5;
    case VertexDataType::HALF_FLOAT: return 6;
    case VertexDataType::FLOAT: return 7;
    case VertexDataType::DOUBLE: return 8;
    case VertexDataType::FIXED: return 9;
    case VertexDataType::INT_2_10_10_10_REV: return 10;
    case VertexDataType::UNSIGNED_INT_2_10_10_10_REV: return 11;
    case VertexDataType::UNSIGNED_INT_10F_11F_11F_REV: return 12;
    case VertexDataType::INVALID:
    default:
      throw std::runtime_error("Invalid VertexDataType");
  }
}

inline VertexDataType fromVertexDataTypeIndex(GLint index) {
  switch (index) {
    case 0: return VertexDataType::BYTE;
    case 1: return VertexDataType::UNSIGNED_BYTE;
    case 2: return VertexDataType::SHORT;
    case 3: return VertexDataType::UNSIGNED_SHORT;
    case 4: return VertexDataType::INT;
    case 5: return VertexDataType::UNSIGNED_INT;
    case 6: return VertexDataType::HALF_FLOAT;
    case 7: return VertexDataType::FLOAT;
    case 8: return VertexDataType::DOUBLE;
    case 9: return VertexDataType::FIXED;
    case 10: return VertexDataType::INT_2_10_10_10_REV;
    case 11: return VertexDataType::UNSIGNED_INT_2_10_10_10_REV;
    case 12: return VertexDataType::UNSIGNED_INT_10F_11F_11F_REV;
    default: return VertexDataType::INVALID;
  }
}

struct SharedEnum_ALWAYS {
  constexpr operator DepthFunction() const { return DepthFunction::ALWAYS; }
  constexpr operator TextureCompareFunc() const { return TextureCompareFunc::ALWAYS; }
};

struct SharedEnum_BYTE {
  constexpr operator ImageDataType() const { return ImageDataType::BYTE; }
  constexpr operator VertexDataType() const { return VertexDataType::BYTE; }
};

struct SharedEnum_DEPTH_COMPONENT {
  constexpr operator ImageFormat() const { return ImageFormat::DEPTH_COMPONENT; }
  constexpr operator TextureFormat() const { return TextureFormat::DEPTH_COMPONENT; }
};

struct SharedEnum_DEPTH_STENCIL {
  constexpr operator ImageFormat() const { return ImageFormat::DEPTH_STENCIL; }
  constexpr operator TextureFormat() const { return TextureFormat::DEPTH_STENCIL; }
};

struct SharedEnum_EQUAL {
  constexpr operator DepthFunction() const { return DepthFunction::EQUAL; }
  constexpr operator TextureCompareFunc() const { return TextureCompareFunc::EQUAL; }
};

struct SharedEnum_FLOAT {
  constexpr operator ImageDataType() const { return ImageDataType::FLOAT; }
  constexpr operator VertexDataType() const { return VertexDataType::FLOAT; }
};

struct SharedEnum_GEQUAL {
  constexpr operator DepthFunction() const { return DepthFunction::GEQUAL; }
  constexpr operator TextureCompareFunc() const { return TextureCompareFunc::GEQUAL; }
};

struct SharedEnum_GREATER {
  constexpr operator DepthFunction() const { return DepthFunction::GREATER; }
  constexpr operator TextureCompareFunc() const { return TextureCompareFunc::GREATER; }
};

struct SharedEnum_INT {
  constexpr operator ImageDataType() const { return ImageDataType::INT; }
  constexpr operator VertexDataType() const { return VertexDataType::INT; }
};

struct SharedEnum_LEQUAL {
  constexpr operator DepthFunction() const { return DepthFunction::LEQUAL; }
  constexpr operator TextureCompareFunc() const { return TextureCompareFunc::LEQUAL; }
};

struct SharedEnum_LESS {
  constexpr operator DepthFunction() const { return DepthFunction::LESS; }
  constexpr operator TextureCompareFunc() const { return TextureCompareFunc::LESS; }
};

struct SharedEnum_LINEAR {
  constexpr operator TextureMagFilter() const { return TextureMagFilter::LINEAR; }
  constexpr operator TextureMinFilter() const { return TextureMinFilter::LINEAR; }
};

struct SharedEnum_NEAREST {
  constexpr operator TextureMagFilter() const { return TextureMagFilter::NEAREST; }
  constexpr operator TextureMinFilter() const { return TextureMinFilter::NEAREST; }
};

struct SharedEnum_NEVER {
  constexpr operator DepthFunction() const { return DepthFunction::NEVER; }
  constexpr operator TextureCompareFunc() const { return TextureCompareFunc::NEVER; }
};

struct SharedEnum_NOTEQUAL {
  constexpr operator DepthFunction() const { return DepthFunction::NOTEQUAL; }
  constexpr operator TextureCompareFunc() const { return TextureCompareFunc::NOTEQUAL; }
};

struct SharedEnum_ONE {
  constexpr operator BlendFunction() const { return BlendFunction::ONE; }
  constexpr operator TextureSwizzle() const { return TextureSwizzle::ONE; }
};

struct SharedEnum_RED {
  constexpr operator ImageFormat() const { return ImageFormat::RED; }
  constexpr operator TextureFormat() const { return TextureFormat::RED; }
  constexpr operator TextureSwizzle() const { return TextureSwizzle::RED; }
};

struct SharedEnum_RG {
  constexpr operator ImageFormat() const { return ImageFormat::RG; }
  constexpr operator TextureFormat() const { return TextureFormat::RG; }
};

struct SharedEnum_RGB {
  constexpr operator ImageFormat() const { return ImageFormat::RGB; }
  constexpr operator TextureFormat() const { return TextureFormat::RGB; }
};

struct SharedEnum_RGBA {
  constexpr operator ImageFormat() const { return ImageFormat::RGBA; }
  constexpr operator TextureFormat() const { return TextureFormat::RGBA; }
};

struct SharedEnum_SHORT {
  constexpr operator ImageDataType() const { return ImageDataType::SHORT; }
  constexpr operator VertexDataType() const { return VertexDataType::SHORT; }
};

struct SharedEnum_TEXTURE_BUFFER {
  constexpr operator BufferTarget() const { return BufferTarget::TEXTURE_BUFFER; }
  constexpr operator TextureTarget() const { return TextureTarget::TEXTURE_BUFFER; }
};

struct SharedEnum_UNSIGNED_BYTE {
  constexpr operator ImageDataType() const { return ImageDataType::UNSIGNED_BYTE; }
  constexpr operator IndexDataType() const { return IndexDataType::UNSIGNED_BYTE; }
  constexpr operator VertexDataType() const { return VertexDataType::UNSIGNED_BYTE; }
};

struct SharedEnum_UNSIGNED_INT {
  constexpr operator ImageDataType() const { return ImageDataType::UNSIGNED_INT; }
  constexpr operator IndexDataType() const { return IndexDataType::UNSIGNED_INT; }
  constexpr operator VertexDataType() const { return VertexDataType::UNSIGNED_INT; }
};

struct SharedEnum_UNSIGNED_INT_2_10_10_10_REV {
  constexpr operator ImageDataType() const { return ImageDataType::UNSIGNED_INT_2_10_10_10_REV; }
  constexpr operator VertexDataType() const { return VertexDataType::UNSIGNED_INT_2_10_10_10_REV; }
};

struct SharedEnum_UNSIGNED_SHORT {
  constexpr operator ImageDataType() const { return ImageDataType::UNSIGNED_SHORT; }
  constexpr operator IndexDataType() const { return IndexDataType::UNSIGNED_SHORT; }
  constexpr operator VertexDataType() const { return VertexDataType::UNSIGNED_SHORT; }
};

struct SharedEnum_ZERO {
  constexpr operator BlendFunction() const { return BlendFunction::ZERO; }
  constexpr operator TextureSwizzle() const { return TextureSwizzle::ZERO; }
};

constexpr BlendEquation FUNC_ADD = BlendEquation::FUNC_ADD;
constexpr BlendEquation FUNC_SUBTRACT = BlendEquation::FUNC_SUBTRACT;
constexpr BlendEquation FUNC_REVERSE_SUBTRACT = BlendEquation::FUNC_REVERSE_SUBTRACT;
constexpr BlendEquation MIN = BlendEquation::MIN;
constexpr BlendEquation MAX = BlendEquation::MAX;

//constexpr BlendFunction ZERO = BlendFunction::ZERO;
//constexpr BlendFunction ONE = BlendFunction::ONE;
constexpr BlendFunction SRC_COLOR = BlendFunction::SRC_COLOR;
constexpr BlendFunction ONE_MINUS_SRC_COLOR = BlendFunction::ONE_MINUS_SRC_COLOR;
constexpr BlendFunction DST_COLOR = BlendFunction::DST_COLOR;
constexpr BlendFunction ONE_MINUS_DST_COLOR = BlendFunction::ONE_MINUS_DST_COLOR;
constexpr BlendFunction SRC_ALPHA = BlendFunction::SRC_ALPHA;
constexpr BlendFunction ONE_MINUS_SRC_ALPHA = BlendFunction::ONE_MINUS_SRC_ALPHA;
constexpr BlendFunction DST_ALPHA = BlendFunction::DST_ALPHA;
constexpr BlendFunction ONE_MINUS_DST_ALPHA = BlendFunction::ONE_MINUS_DST_ALPHA;
constexpr BlendFunction CONSTANT_COLOR = BlendFunction::CONSTANT_COLOR;
constexpr BlendFunction ONE_MINUS_CONSTANT_COLOR = BlendFunction::ONE_MINUS_CONSTANT_COLOR;
constexpr BlendFunction CONSTANT_ALPHA = BlendFunction::CONSTANT_ALPHA;
constexpr BlendFunction ONE_MINUS_CONSTANT_ALPHA = BlendFunction::ONE_MINUS_CONSTANT_ALPHA;
constexpr BlendFunction SRC_ALPHA_SATURATE = BlendFunction::SRC_ALPHA_SATURATE;
constexpr BlendFunction SRC1_COLOR = BlendFunction::SRC1_COLOR;
constexpr BlendFunction ONE_MINUS_SRC1_COLOR = BlendFunction::ONE_MINUS_SRC1_COLOR;
constexpr BlendFunction SRC1_ALPHA = BlendFunction::SRC1_ALPHA;
constexpr BlendFunction ONE_MINUS_SRC1_ALPHA = BlendFunction::ONE_MINUS_SRC1_ALPHA;

constexpr BufferTarget ARRAY_BUFFER = BufferTarget::ARRAY_BUFFER;
constexpr BufferTarget COPY_READ_BUFFER = BufferTarget::COPY_READ_BUFFER;
constexpr BufferTarget COPY_WRITE_BUFFER = BufferTarget::COPY_WRITE_BUFFER;
constexpr BufferTarget DRAW_INDIRECT_BUFFER = BufferTarget::DRAW_INDIRECT_BUFFER;
constexpr BufferTarget ELEMENT_ARRAY_BUFFER = BufferTarget::ELEMENT_ARRAY_BUFFER;
constexpr BufferTarget PIXEL_PACK_BUFFER = BufferTarget::PIXEL_PACK_BUFFER;
constexpr BufferTarget PIXEL_UNPACK_BUFFER = BufferTarget::PIXEL_UNPACK_BUFFER;
//constexpr BufferTarget TEXTURE_BUFFER = BufferTarget::TEXTURE_BUFFER;
constexpr BufferTarget TRANSFORM_FEEDBACK_BUFFER = BufferTarget::TRANSFORM_FEEDBACK_BUFFER;
constexpr BufferTarget UNIFORM_BUFFER = BufferTarget::UNIFORM_BUFFER;

constexpr BufferUsage STREAM_DRAW = BufferUsage::STREAM_DRAW;
constexpr BufferUsage STREAM_READ = BufferUsage::STREAM_READ;
constexpr BufferUsage STREAM_COPY = BufferUsage::STREAM_COPY;
constexpr BufferUsage STATIC_DRAW = BufferUsage::STATIC_DRAW;
constexpr BufferUsage STATIC_READ = BufferUsage::STATIC_READ;
constexpr BufferUsage STATIC_COPY = BufferUsage::STATIC_COPY;
constexpr BufferUsage DYNAMIC_DRAW = BufferUsage::DYNAMIC_DRAW;
constexpr BufferUsage DYNAMIC_READ = BufferUsage::DYNAMIC_READ;
constexpr BufferUsage DYNAMIC_COPY = BufferUsage::DYNAMIC_COPY;

constexpr Capability BLEND = Capability::BLEND;
constexpr Capability COLOR_LOGIC_OP = Capability::COLOR_LOGIC_OP;
constexpr Capability CULL_FACE = Capability::CULL_FACE;
constexpr Capability DEPTH_CLAMP = Capability::DEPTH_CLAMP;
constexpr Capability DEPTH_TEST = Capability::DEPTH_TEST;
constexpr Capability DITHER = Capability::DITHER;
constexpr Capability FRAMEBUFFER_SRGB = Capability::FRAMEBUFFER_SRGB;
constexpr Capability LINE_SMOOTH = Capability::LINE_SMOOTH;
constexpr Capability MULTISAMPLE = Capability::MULTISAMPLE;
constexpr Capability POLYGON_OFFSET_FILL = Capability::POLYGON_OFFSET_FILL;
constexpr Capability POLYGON_OFFSET_LINE = Capability::POLYGON_OFFSET_LINE;
constexpr Capability POLYGON_OFFSET_POINT = Capability::POLYGON_OFFSET_POINT;
constexpr Capability POLYGON_SMOOTH = Capability::POLYGON_SMOOTH;
constexpr Capability PRIMITIVE_RESTART = Capability::PRIMITIVE_RESTART;
constexpr Capability RASTERIZER_DISCARD = Capability::RASTERIZER_DISCARD;
constexpr Capability SAMPLE_ALPHA_TO_COVERAGE = Capability::SAMPLE_ALPHA_TO_COVERAGE;
constexpr Capability SAMPLE_ALPHA_TO_ONE = Capability::SAMPLE_ALPHA_TO_ONE;
constexpr Capability SAMPLE_COVERAGE = Capability::SAMPLE_COVERAGE;
constexpr Capability SAMPLE_SHADING = Capability::SAMPLE_SHADING;
constexpr Capability SAMPLE_MASK = Capability::SAMPLE_MASK;
constexpr Capability SCISSOR_TEST = Capability::SCISSOR_TEST;
constexpr Capability STENCIL_TEST = Capability::STENCIL_TEST;
constexpr Capability TEXTURE_CUBE_MAP_SEAMLESS = Capability::TEXTURE_CUBE_MAP_SEAMLESS;
constexpr Capability PROGRAM_POINT_SIZE = Capability::PROGRAM_POINT_SIZE;

//constexpr DepthFunction NEVER = DepthFunction::NEVER;
//constexpr DepthFunction LESS = DepthFunction::LESS;
//constexpr DepthFunction EQUAL = DepthFunction::EQUAL;
//constexpr DepthFunction LEQUAL = DepthFunction::LEQUAL;
//constexpr DepthFunction GREATER = DepthFunction::GREATER;
//constexpr DepthFunction NOTEQUAL = DepthFunction::NOTEQUAL;
//constexpr DepthFunction GEQUAL = DepthFunction::GEQUAL;
//constexpr DepthFunction ALWAYS = DepthFunction::ALWAYS;

constexpr DrawPrimitive POINTS = DrawPrimitive::POINTS;
constexpr DrawPrimitive LINE_STRIP = DrawPrimitive::LINE_STRIP;
constexpr DrawPrimitive LINE_LOOP = DrawPrimitive::LINE_LOOP;
constexpr DrawPrimitive LINES = DrawPrimitive::LINES;
constexpr DrawPrimitive LINE_STRIP_ADJACENCY = DrawPrimitive::LINE_STRIP_ADJACENCY;
constexpr DrawPrimitive LINES_ADJACENCY = DrawPrimitive::LINES_ADJACENCY;
constexpr DrawPrimitive TRIANGLE_STRIP = DrawPrimitive::TRIANGLE_STRIP;
constexpr DrawPrimitive TRIANGLE_FAN = DrawPrimitive::TRIANGLE_FAN;
constexpr DrawPrimitive TRIANGLES = DrawPrimitive::TRIANGLES;
constexpr DrawPrimitive TRIANGLE_STRIP_ADJACENCY = DrawPrimitive::TRIANGLE_STRIP_ADJACENCY;
constexpr DrawPrimitive TRIANGLES_ADJACENCY = DrawPrimitive::TRIANGLES_ADJACENCY;
constexpr DrawPrimitive PATCHES = DrawPrimitive::PATCHES;

//constexpr ImageDataType UNSIGNED_BYTE = ImageDataType::UNSIGNED_BYTE;
//constexpr ImageDataType BYTE = ImageDataType::BYTE;
//constexpr ImageDataType UNSIGNED_SHORT = ImageDataType::UNSIGNED_SHORT;
//constexpr ImageDataType SHORT = ImageDataType::SHORT;
//constexpr ImageDataType UNSIGNED_INT = ImageDataType::UNSIGNED_INT;
//constexpr ImageDataType INT = ImageDataType::INT;
//constexpr ImageDataType FLOAT = ImageDataType::FLOAT;
constexpr ImageDataType UNSIGNED_BYTE_3_3_2 = ImageDataType::UNSIGNED_BYTE_3_3_2;
constexpr ImageDataType UNSIGNED_BYTE_2_3_3_REV = ImageDataType::UNSIGNED_BYTE_2_3_3_REV;
constexpr ImageDataType UNSIGNED_SHORT_5_6_5 = ImageDataType::UNSIGNED_SHORT_5_6_5;
constexpr ImageDataType UNSIGNED_SHORT_5_6_5_REV = ImageDataType::UNSIGNED_SHORT_5_6_5_REV;
constexpr ImageDataType UNSIGNED_SHORT_4_4_4_4 = ImageDataType::UNSIGNED_SHORT_4_4_4_4;
constexpr ImageDataType UNSIGNED_SHORT_4_4_4_4_REV = ImageDataType::UNSIGNED_SHORT_4_4_4_4_REV;
constexpr ImageDataType UNSIGNED_SHORT_5_5_5_1 = ImageDataType::UNSIGNED_SHORT_5_5_5_1;
constexpr ImageDataType UNSIGNED_SHORT_1_5_5_5_REV = ImageDataType::UNSIGNED_SHORT_1_5_5_5_REV;
constexpr ImageDataType UNSIGNED_INT_8_8_8_8 = ImageDataType::UNSIGNED_INT_8_8_8_8;
constexpr ImageDataType UNSIGNED_INT_8_8_8_8_REV = ImageDataType::UNSIGNED_INT_8_8_8_8_REV;
constexpr ImageDataType UNSIGNED_INT_10_10_10_2 = ImageDataType::UNSIGNED_INT_10_10_10_2;
//constexpr ImageDataType UNSIGNED_INT_2_10_10_10_REV = ImageDataType::UNSIGNED_INT_2_10_10_10_REV;

//constexpr ImageFormat RED = ImageFormat::RED;
//constexpr ImageFormat RG = ImageFormat::RG;
//constexpr ImageFormat RGB = ImageFormat::RGB;
constexpr ImageFormat BGR = ImageFormat::BGR;
//constexpr ImageFormat RGBA = ImageFormat::RGBA;
constexpr ImageFormat BGRA = ImageFormat::BGRA;
constexpr ImageFormat RED_INTEGER = ImageFormat::RED_INTEGER;
constexpr ImageFormat RG_INTEGER = ImageFormat::RG_INTEGER;
constexpr ImageFormat RGB_INTEGER = ImageFormat::RGB_INTEGER;
constexpr ImageFormat BGR_INTEGER = ImageFormat::BGR_INTEGER;
constexpr ImageFormat RGBA_INTEGER = ImageFormat::RGBA_INTEGER;
constexpr ImageFormat BGRA_INTEGER = ImageFormat::BGRA_INTEGER;
constexpr ImageFormat STENCIL_INDEX = ImageFormat::STENCIL_INDEX;
//constexpr ImageFormat DEPTH_COMPONENT = ImageFormat::DEPTH_COMPONENT;
//constexpr ImageFormat DEPTH_STENCIL = ImageFormat::DEPTH_STENCIL;

//constexpr IndexDataType UNSIGNED_BYTE = IndexDataType::UNSIGNED_BYTE;
//constexpr IndexDataType UNSIGNED_SHORT = IndexDataType::UNSIGNED_SHORT;
//constexpr IndexDataType UNSIGNED_INT = IndexDataType::UNSIGNED_INT;

constexpr ShaderType VERTEX_SHADER = ShaderType::VERTEX_SHADER;
constexpr ShaderType TESS_CONTROL_SHADER = ShaderType::TESS_CONTROL_SHADER;
constexpr ShaderType TESS_EVALUATION_SHADER = ShaderType::TESS_EVALUATION_SHADER;
constexpr ShaderType GEOMETRY_SHADER = ShaderType::GEOMETRY_SHADER;
constexpr ShaderType FRAGMENT_SHADER = ShaderType::FRAGMENT_SHADER;

//constexpr TextureCompareFunc LEQUAL = TextureCompareFunc::LEQUAL;
//constexpr TextureCompareFunc GEQUAL = TextureCompareFunc::GEQUAL;
//constexpr TextureCompareFunc LESS = TextureCompareFunc::LESS;
//constexpr TextureCompareFunc GREATER = TextureCompareFunc::GREATER;
//constexpr TextureCompareFunc EQUAL = TextureCompareFunc::EQUAL;
//constexpr TextureCompareFunc NOTEQUAL = TextureCompareFunc::NOTEQUAL;
//constexpr TextureCompareFunc ALWAYS = TextureCompareFunc::ALWAYS;
//constexpr TextureCompareFunc NEVER = TextureCompareFunc::NEVER;

constexpr TextureCompareMode COMPARE_REF_TO_TEXTURE = TextureCompareMode::COMPARE_REF_TO_TEXTURE;
constexpr TextureCompareMode NONE = TextureCompareMode::NONE;

//constexpr TextureFormat DEPTH_COMPONENT = TextureFormat::DEPTH_COMPONENT;
//constexpr TextureFormat DEPTH_STENCIL = TextureFormat::DEPTH_STENCIL;
//constexpr TextureFormat RED = TextureFormat::RED;
//constexpr TextureFormat RG = TextureFormat::RG;
//constexpr TextureFormat RGB = TextureFormat::RGB;
//constexpr TextureFormat RGBA = TextureFormat::RGBA;
constexpr TextureFormat R8 = TextureFormat::R8;
constexpr TextureFormat R8_SNORM = TextureFormat::R8_SNORM;
constexpr TextureFormat R16 = TextureFormat::R16;
constexpr TextureFormat R16_SNORM = TextureFormat::R16_SNORM;
constexpr TextureFormat RG8 = TextureFormat::RG8;
constexpr TextureFormat RG8_SNORM = TextureFormat::RG8_SNORM;
constexpr TextureFormat RG16 = TextureFormat::RG16;
constexpr TextureFormat RG16_SNORM = TextureFormat::RG16_SNORM;
constexpr TextureFormat R3_G3_B2 = TextureFormat::R3_G3_B2;
constexpr TextureFormat RGB4 = TextureFormat::RGB4;
constexpr TextureFormat RGB5 = TextureFormat::RGB5;
constexpr TextureFormat RGB8 = TextureFormat::RGB8;
constexpr TextureFormat RGB8_SNORM = TextureFormat::RGB8_SNORM;
constexpr TextureFormat RGB10 = TextureFormat::RGB10;
constexpr TextureFormat RGB12 = TextureFormat::RGB12;
constexpr TextureFormat RGB16_SNORM = TextureFormat::RGB16_SNORM;
constexpr TextureFormat RGBA2 = TextureFormat::RGBA2;
constexpr TextureFormat RGBA4 = TextureFormat::RGBA4;
constexpr TextureFormat RGB5_A1 = TextureFormat::RGB5_A1;
constexpr TextureFormat RGBA8 = TextureFormat::RGBA8;
constexpr TextureFormat RGBA8_SNORM = TextureFormat::RGBA8_SNORM;
constexpr TextureFormat RGB10_A2 = TextureFormat::RGB10_A2;
constexpr TextureFormat RGB10_A2UI = TextureFormat::RGB10_A2UI;
constexpr TextureFormat RGBA12 = TextureFormat::RGBA12;
constexpr TextureFormat RGBA16 = TextureFormat::RGBA16;
constexpr TextureFormat SRGB8 = TextureFormat::SRGB8;
constexpr TextureFormat SRGB8_ALPHA8 = TextureFormat::SRGB8_ALPHA8;
constexpr TextureFormat R16F = TextureFormat::R16F;
constexpr TextureFormat RG16F = TextureFormat::RG16F;
constexpr TextureFormat RGB16F = TextureFormat::RGB16F;
constexpr TextureFormat RGBA16F = TextureFormat::RGBA16F;
constexpr TextureFormat R32F = TextureFormat::R32F;
constexpr TextureFormat RG32F = TextureFormat::RG32F;
constexpr TextureFormat RGB32F = TextureFormat::RGB32F;
constexpr TextureFormat RGBA32F = TextureFormat::RGBA32F;
constexpr TextureFormat R11F_G11F_B10F = TextureFormat::R11F_G11F_B10F;
constexpr TextureFormat RGB9_E5 = TextureFormat::RGB9_E5;
constexpr TextureFormat R8I = TextureFormat::R8I;
constexpr TextureFormat R8UI = TextureFormat::R8UI;
constexpr TextureFormat R16I = TextureFormat::R16I;
constexpr TextureFormat R16UI = TextureFormat::R16UI;
constexpr TextureFormat R32I = TextureFormat::R32I;
constexpr TextureFormat R32UI = TextureFormat::R32UI;
constexpr TextureFormat RG8I = TextureFormat::RG8I;
constexpr TextureFormat RG8UI = TextureFormat::RG8UI;
constexpr TextureFormat RG16I = TextureFormat::RG16I;
constexpr TextureFormat RG16UI = TextureFormat::RG16UI;
constexpr TextureFormat RG32I = TextureFormat::RG32I;
constexpr TextureFormat RG32UI = TextureFormat::RG32UI;
constexpr TextureFormat RGB8I = TextureFormat::RGB8I;
constexpr TextureFormat RGB8UI = TextureFormat::RGB8UI;
constexpr TextureFormat RGB16I = TextureFormat::RGB16I;
constexpr TextureFormat RGB16UI = TextureFormat::RGB16UI;
constexpr TextureFormat RGB32I = TextureFormat::RGB32I;
constexpr TextureFormat RGB32UI = TextureFormat::RGB32UI;
constexpr TextureFormat RGBA8I = TextureFormat::RGBA8I;
constexpr TextureFormat RGBA8UI = TextureFormat::RGBA8UI;
constexpr TextureFormat RGBA16I = TextureFormat::RGBA16I;
constexpr TextureFormat RGBA16UI = TextureFormat::RGBA16UI;
constexpr TextureFormat RGBA32I = TextureFormat::RGBA32I;
constexpr TextureFormat RGBA32UI = TextureFormat::RGBA32UI;
constexpr TextureFormat COMPRESSED_RED = TextureFormat::COMPRESSED_RED;
constexpr TextureFormat COMPRESSED_RG = TextureFormat::COMPRESSED_RG;
constexpr TextureFormat COMPRESSED_RGB = TextureFormat::COMPRESSED_RGB;
constexpr TextureFormat COMPRESSED_RGBA = TextureFormat::COMPRESSED_RGBA;
constexpr TextureFormat COMPRESSED_SRGB = TextureFormat::COMPRESSED_SRGB;
constexpr TextureFormat COMPRESSED_SRGB_ALPHA = TextureFormat::COMPRESSED_SRGB_ALPHA;
constexpr TextureFormat COMPRESSED_RED_RGTC1 = TextureFormat::COMPRESSED_RED_RGTC1;
constexpr TextureFormat COMPRESSED_SIGNED_RED_RGTC1 = TextureFormat::COMPRESSED_SIGNED_RED_RGTC1;
constexpr TextureFormat COMPRESSED_RG_RGTC2 = TextureFormat::COMPRESSED_RG_RGTC2;
constexpr TextureFormat COMPRESSED_SIGNED_RG_RGTC2 = TextureFormat::COMPRESSED_SIGNED_RG_RGTC2;

//constexpr TextureMagFilter NEAREST = TextureMagFilter::NEAREST;
//constexpr TextureMagFilter LINEAR = TextureMagFilter::LINEAR;

//constexpr TextureMinFilter NEAREST = TextureMinFilter::NEAREST;
//constexpr TextureMinFilter LINEAR = TextureMinFilter::LINEAR;
constexpr TextureMinFilter NEAREST_MIPMAP_NEAREST = TextureMinFilter::NEAREST_MIPMAP_NEAREST;
constexpr TextureMinFilter LINEAR_MIPMAP_NEAREST = TextureMinFilter::LINEAR_MIPMAP_NEAREST;
constexpr TextureMinFilter NEAREST_MIPMAP_LINEAR = TextureMinFilter::NEAREST_MIPMAP_LINEAR;
constexpr TextureMinFilter LINEAR_MIPMAP_LINEAR = TextureMinFilter::LINEAR_MIPMAP_LINEAR;

//constexpr TextureSwizzle RED = TextureSwizzle::RED;
constexpr TextureSwizzle GREEN = TextureSwizzle::GREEN;
constexpr TextureSwizzle BLUE = TextureSwizzle::BLUE;
constexpr TextureSwizzle ALPHA = TextureSwizzle::ALPHA;
//constexpr TextureSwizzle ZERO = TextureSwizzle::ZERO;
//constexpr TextureSwizzle ONE = TextureSwizzle::ONE;

constexpr TextureTarget TEXTURE_1D = TextureTarget::TEXTURE_1D;
constexpr TextureTarget PROXY_TEXTURE_1D = TextureTarget::PROXY_TEXTURE_1D;
constexpr TextureTarget TEXTURE_2D = TextureTarget::TEXTURE_2D;
constexpr TextureTarget PROXY_TEXTURE_2D = TextureTarget::PROXY_TEXTURE_2D;
constexpr TextureTarget TEXTURE_3D = TextureTarget::TEXTURE_3D;
constexpr TextureTarget PROXY_TEXTURE_3D = TextureTarget::PROXY_TEXTURE_3D;
constexpr TextureTarget TEXTURE_1D_ARRAY = TextureTarget::TEXTURE_1D_ARRAY;
constexpr TextureTarget PROXY_TEXTURE_1D_ARRAY = TextureTarget::PROXY_TEXTURE_1D_ARRAY;
constexpr TextureTarget TEXTURE_2D_ARRAY = TextureTarget::TEXTURE_2D_ARRAY;
constexpr TextureTarget PROXY_TEXTURE_2D_ARRAY = TextureTarget::PROXY_TEXTURE_2D_ARRAY;
constexpr TextureTarget TEXTURE_RECTANGLE = TextureTarget::TEXTURE_RECTANGLE;
constexpr TextureTarget PROXY_TEXTURE_RECTANGLE = TextureTarget::PROXY_TEXTURE_RECTANGLE;
constexpr TextureTarget TEXTURE_CUBE_MAP_POSITIVE_X = TextureTarget::TEXTURE_CUBE_MAP_POSITIVE_X;
constexpr TextureTarget TEXTURE_CUBE_MAP_NEGATIVE_X = TextureTarget::TEXTURE_CUBE_MAP_NEGATIVE_X;
constexpr TextureTarget TEXTURE_CUBE_MAP_POSITIVE_Y = TextureTarget::TEXTURE_CUBE_MAP_POSITIVE_Y;
constexpr TextureTarget TEXTURE_CUBE_MAP_NEGATIVE_Y = TextureTarget::TEXTURE_CUBE_MAP_NEGATIVE_Y;
constexpr TextureTarget TEXTURE_CUBE_MAP_POSITIVE_Z = TextureTarget::TEXTURE_CUBE_MAP_POSITIVE_Z;
constexpr TextureTarget TEXTURE_CUBE_MAP_NEGATIVE_Z = TextureTarget::TEXTURE_CUBE_MAP_NEGATIVE_Z;
constexpr TextureTarget PROXY_TEXTURE_CUBE_MAP = TextureTarget::PROXY_TEXTURE_CUBE_MAP;
constexpr TextureTarget TEXTURE_2D_MULTISAMPLE = TextureTarget::TEXTURE_2D_MULTISAMPLE;
constexpr TextureTarget PROXY_TEXTURE_2D_MULTISAMPLE = TextureTarget::PROXY_TEXTURE_2D_MULTISAMPLE;
constexpr TextureTarget TEXTURE_CUBE_MAP = TextureTarget::TEXTURE_CUBE_MAP;
//constexpr TextureTarget TEXTURE_BUFFER = TextureTarget::TEXTURE_BUFFER;

constexpr TextureWrap CLAMP_TO_EDGE = TextureWrap::CLAMP_TO_EDGE;
constexpr TextureWrap CLAMP_TO_BORDER = TextureWrap::CLAMP_TO_BORDER;
constexpr TextureWrap MIRRORED_REPEAT = TextureWrap::MIRRORED_REPEAT;
constexpr TextureWrap REPEAT = TextureWrap::REPEAT;

//constexpr VertexDataType BYTE = VertexDataType::BYTE;
//constexpr VertexDataType UNSIGNED_BYTE = VertexDataType::UNSIGNED_BYTE;
//constexpr VertexDataType SHORT = VertexDataType::SHORT;
//constexpr VertexDataType UNSIGNED_SHORT = VertexDataType::UNSIGNED_SHORT;
//constexpr VertexDataType INT = VertexDataType::INT;
//constexpr VertexDataType UNSIGNED_INT = VertexDataType::UNSIGNED_INT;
constexpr VertexDataType HALF_FLOAT = VertexDataType::HALF_FLOAT;
//constexpr VertexDataType FLOAT = VertexDataType::FLOAT;
constexpr VertexDataType DOUBLE = VertexDataType::DOUBLE;
constexpr VertexDataType FIXED = VertexDataType::FIXED;
constexpr VertexDataType INT_2_10_10_10_REV = VertexDataType::INT_2_10_10_10_REV;
//constexpr VertexDataType UNSIGNED_INT_2_10_10_10_REV = VertexDataType::UNSIGNED_INT_2_10_10_10_REV;
constexpr VertexDataType UNSIGNED_INT_10F_11F_11F_REV = VertexDataType::UNSIGNED_INT_10F_11F_11F_REV;

constexpr SharedEnum_ALWAYS ALWAYS = {};
constexpr SharedEnum_BYTE BYTE = {};
constexpr SharedEnum_DEPTH_COMPONENT DEPTH_COMPONENT = {};
constexpr SharedEnum_DEPTH_STENCIL DEPTH_STENCIL = {};
constexpr SharedEnum_EQUAL EQUAL = {};
constexpr SharedEnum_FLOAT FLOAT = {};
constexpr SharedEnum_GEQUAL GEQUAL = {};
constexpr SharedEnum_GREATER GREATER = {};
constexpr SharedEnum_INT INT = {};
constexpr SharedEnum_LEQUAL LEQUAL = {};
constexpr SharedEnum_LESS LESS = {};
constexpr SharedEnum_LINEAR LINEAR = {};
constexpr SharedEnum_NEAREST NEAREST = {};
constexpr SharedEnum_NEVER NEVER = {};
constexpr SharedEnum_NOTEQUAL NOTEQUAL = {};
constexpr SharedEnum_ONE ONE = {};
constexpr SharedEnum_RED RED = {};
constexpr SharedEnum_RG RG = {};
constexpr SharedEnum_RGB RGB = {};
constexpr SharedEnum_RGBA RGBA = {};
constexpr SharedEnum_SHORT SHORT = {};
constexpr SharedEnum_TEXTURE_BUFFER TEXTURE_BUFFER = {};
constexpr SharedEnum_UNSIGNED_BYTE UNSIGNED_BYTE = {};
constexpr SharedEnum_UNSIGNED_INT UNSIGNED_INT = {};
constexpr SharedEnum_UNSIGNED_INT_2_10_10_10_REV UNSIGNED_INT_2_10_10_10_REV = {};
constexpr SharedEnum_UNSIGNED_SHORT UNSIGNED_SHORT = {};
constexpr SharedEnum_ZERO ZERO = {};

}} // namespace Slum::GL
