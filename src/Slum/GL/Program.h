#pragma once

#include "Types.h"
#include "Util/ArrayView.h"
#include "Util/Handle.h"

#include <glm/fwd.hpp>

#include <string>

namespace Slum { namespace GL {

class Shader;

class Program {
 public:
  Program(const Shader& shader1, const Shader& shader2);
  ~Program();

  Program() = default;
  Program(Program&&) noexcept = default;
  Program& operator=(Program&&) noexcept = default;

  GLuint id() const { return id_; }

  Program&& create();
  Program&& attachShader(const Shader& shader);
  Program&& bindAttribute(GLint location, const std::string& name);
  Program&& link();
  Program&& validate();

  std::string getInfoLog();
  GLint getAttributeLocation(const std::string& name);
  GLint getUniformLocation(const std::string& name);

  Program&& use();
  Program&& setUniform(GLint location, GLfloat value);
  Program&& setUniform(GLint location, const ArrayView<GLfloat>& value);
  Program&& setUniform(GLint location, const ArrayView<glm::vec2>& value);
  Program&& setUniform(GLint location, const ArrayView<glm::vec3>& value);
  Program&& setUniform(GLint location, const ArrayView<glm::vec4>& value);
  Program&& setUniform(GLint location, GLint value);
  Program&& setUniform(GLint location, const ArrayView<GLint>& value);
  Program&& setUniform(GLint location, const ArrayView<glm::ivec2>& value);
  Program&& setUniform(GLint location, const ArrayView<glm::ivec3>& value);
  Program&& setUniform(GLint location, const ArrayView<glm::ivec4>& value);
  Program&& setUniform(GLint location, GLuint value);
  Program&& setUniform(GLint location, const ArrayView<GLuint>& value);
  Program&& setUniform(GLint location, const ArrayView<glm::uvec2>& value);
  Program&& setUniform(GLint location, const ArrayView<glm::uvec3>& value);
  Program&& setUniform(GLint location, const ArrayView<glm::uvec4>& value);
  Program&& setUniform(GLint location, const ArrayView<glm::mat2>& value);
  Program&& setUniform(GLint location, const ArrayView<glm::mat3>& value);
  Program&& setUniform(GLint location, const ArrayView<glm::mat4>& value);
  Program&& setUniform(GLint location, const ArrayView<glm::mat2x3>& value);
  Program&& setUniform(GLint location, const ArrayView<glm::mat2x4>& value);
  Program&& setUniform(GLint location, const ArrayView<glm::mat3x2>& value);
  Program&& setUniform(GLint location, const ArrayView<glm::mat3x4>& value);
  Program&& setUniform(GLint location, const ArrayView<glm::mat4x2>& value);
  Program&& setUniform(GLint location, const ArrayView<glm::mat4x3>& value);

 private:
  Handle<GLuint> id_;
};

}} // namespace Slum::GL
