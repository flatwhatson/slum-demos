#include "Program.h"

#include "API.h"
#include "Context.h"
#include "Shader.h"

#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>

#include <stdexcept>
#include <utility>

namespace Slum { namespace GL {

Program::Program(const Shader& shader1, const Shader& shader2) {
  attachShader(shader1);
  attachShader(shader2);
  link();
  validate();
}

Program::~Program() {
  if (id_)
    glDeleteProgram(id_);
}

Program&& Program::create() {
  if (!id_)
    id_ = glCreateProgram();
  return std::move(*this);
}

Program&& Program::attachShader(const Shader& shader) {
  create();
  glAttachShader(id_, shader.id());
  return std::move(*this);
}

Program&& Program::bindAttribute(GLint location, const std::string& name) {
  glBindAttribLocation(id_, location, name.c_str());
  return std::move(*this);
}

Program&& Program::link() {
  glLinkProgram(id_);

  GLint linkStatus;
  glGetProgramiv(id_, GL_LINK_STATUS, &linkStatus);
  if (linkStatus != GL_TRUE)
    throw std::runtime_error(getInfoLog());

  return std::move(*this);
}

Program&& Program::validate() {
  glValidateProgram(id_);

  GLint validateStatus;
  glGetProgramiv(id_, GL_VALIDATE_STATUS, &validateStatus);
  if (validateStatus != GL_TRUE)
    throw std::runtime_error(getInfoLog());

  return std::move(*this);
}

std::string Program::getInfoLog() {
  GLint infoLogLength;
  glGetProgramiv(id_, GL_INFO_LOG_LENGTH, &infoLogLength);

  std::string infoLog;
  if (infoLogLength > 0) {
    infoLog.resize(infoLogLength);
    glGetProgramInfoLog(id_, infoLog.size(), nullptr, &infoLog.front());
  }

  return infoLog;
}

GLint Program::getAttributeLocation(const std::string& name) {
  return glGetAttribLocation(id_, name.c_str());
}

GLint Program::getUniformLocation(const std::string& name) {
  return glGetUniformLocation(id_, name.c_str());
}

Program&& Program::use() {
  Context::useProgram(id_);
  return std::move(*this);
}

Program&& Program::setUniform(GLint location, GLfloat value) {
  use();
  glUniform1fv(location, 1, &value);
  return std::move(*this);
}

Program&& Program::setUniform(GLint location, const ArrayView<GLfloat>& value) {
  use();
  glUniform1fv(location, value.size(), value.data());
  return std::move(*this);
}

Program&& Program::setUniform(GLint location, const ArrayView<glm::vec2>& value) {
  use();
  glUniform2fv(location, value.size(), glm::value_ptr(*value.data()));
  return std::move(*this);
}

Program&& Program::setUniform(GLint location, const ArrayView<glm::vec3>& value) {
  use();
  glUniform3fv(location, value.size(), glm::value_ptr(*value.data()));
  return std::move(*this);
}

Program&& Program::setUniform(GLint location, const ArrayView<glm::vec4>& value) {
  use();
  glUniform4fv(location, value.size(), glm::value_ptr(*value.data()));
  return std::move(*this);
}

Program&& Program::setUniform(GLint location, GLint value) {
  use();
  glUniform1iv(location, 1, &value);
  return std::move(*this);
}

Program&& Program::setUniform(GLint location, const ArrayView<GLint>& value) {
  use();
  glUniform1iv(location, value.size(), value.data());
  return std::move(*this);
}

Program&& Program::setUniform(GLint location, const ArrayView<glm::ivec2>& value) {
  use();
  glUniform2iv(location, value.size(), glm::value_ptr(*value.data()));
  return std::move(*this);
}

Program&& Program::setUniform(GLint location, const ArrayView<glm::ivec3>& value) {
  use();
  glUniform3iv(location, value.size(), glm::value_ptr(*value.data()));
  return std::move(*this);
}

Program&& Program::setUniform(GLint location, const ArrayView<glm::ivec4>& value) {
  use();
  glUniform4iv(location, value.size(), glm::value_ptr(*value.data()));
  return std::move(*this);
}

Program&& Program::setUniform(GLint location, GLuint value) {
  use();
  glUniform1uiv(location, 1, &value);
  return std::move(*this);
}

Program&& Program::setUniform(GLint location, const ArrayView<GLuint>& value) {
  use();
  glUniform1uiv(location, value.size(), value.data());
  return std::move(*this);
}

Program&& Program::setUniform(GLint location, const ArrayView<glm::uvec2>& value) {
  use();
  glUniform2uiv(location, value.size(), glm::value_ptr(*value.data()));
  return std::move(*this);
}

Program&& Program::setUniform(GLint location, const ArrayView<glm::uvec3>& value) {
  use();
  glUniform3uiv(location, value.size(), glm::value_ptr(*value.data()));
  return std::move(*this);
}

Program&& Program::setUniform(GLint location, const ArrayView<glm::uvec4>& value) {
  use();
  glUniform4uiv(location, value.size(), glm::value_ptr(*value.data()));
  return std::move(*this);
}

Program&& Program::setUniform(GLint location, const ArrayView<glm::mat2>& value) {
  use();
  glUniformMatrix2fv(location, value.size(), GL_FALSE, glm::value_ptr(*value.data()));
  return std::move(*this);
}

Program&& Program::setUniform(GLint location, const ArrayView<glm::mat3>& value) {
  use();
  glUniformMatrix3fv(location, value.size(), GL_FALSE, glm::value_ptr(*value.data()));
  return std::move(*this);
}

Program&& Program::setUniform(GLint location, const ArrayView<glm::mat4>& value) {
  use();
  glUniformMatrix4fv(location, value.size(), GL_FALSE, glm::value_ptr(*value.data()));
  return std::move(*this);
}

Program&& Program::setUniform(GLint location, const ArrayView<glm::mat2x3>& value) {
  use();
  glUniformMatrix2x3fv(location, value.size(), GL_FALSE, glm::value_ptr(*value.data()));
  return std::move(*this);
}

Program&& Program::setUniform(GLint location, const ArrayView<glm::mat2x4>& value) {
  use();
  glUniformMatrix2x4fv(location, value.size(), GL_FALSE, glm::value_ptr(*value.data()));
  return std::move(*this);
}

Program&& Program::setUniform(GLint location, const ArrayView<glm::mat3x2>& value) {
  use();
  glUniformMatrix3x2fv(location, value.size(), GL_FALSE, glm::value_ptr(*value.data()));
  return std::move(*this);
}

Program&& Program::setUniform(GLint location, const ArrayView<glm::mat3x4>& value) {
  use();
  glUniformMatrix3x4fv(location, value.size(), GL_FALSE, glm::value_ptr(*value.data()));
  return std::move(*this);
}

Program&& Program::setUniform(GLint location, const ArrayView<glm::mat4x2>& value) {
  use();
  glUniformMatrix4x2fv(location, value.size(), GL_FALSE, glm::value_ptr(*value.data()));
  return std::move(*this);
}

Program&& Program::setUniform(GLint location, const ArrayView<glm::mat4x3>& value) {
  use();
  glUniformMatrix4x3fv(location, value.size(), GL_FALSE, glm::value_ptr(*value.data()));
  return std::move(*this);
}

}} // namespace Slum::GL
