#pragma once

#include "Enums.h"
#include "Types.h"
#include "Util/Handle.h"

#include <string>
#include <vector>

namespace Slum { namespace GL {

class Shader {
 public:
  Shader(ShaderType type, std::string source);
  ~Shader();

  Shader() = default;
  Shader(Shader&&) noexcept = default;
  Shader& operator=(Shader&&) noexcept = default;

  GLuint id() const { return id_; }
  ShaderType type() const { return type_; }
  const std::vector<std::string>& sources() const { return sources_; }

  Shader&& create();
  Shader&& setType(ShaderType type);
  Shader&& addSource(std::string source);
  Shader&& compile();

  std::string getInfoLog();

 private:
  Handle<GLuint> id_;
  ShaderType type_;
  std::vector<std::string> sources_;
};

}} // namespace Slum::GL
