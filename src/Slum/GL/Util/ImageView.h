#pragma once

#include "../Enums.h"
#include "../Types.h"

namespace Slum { namespace GL {

namespace detail {
  class ImageView {
   protected:
    ImageView(ImageFormat format, ImageDataType type, const GLvoid* data) :
      format_(format), type_(type), data_(data) {}

   public:
    ImageFormat format() const { return format_; }
    ImageDataType type() const { return type_; }
    const GLvoid* data() const { return data_; }

   private:
    ImageFormat format_;
    ImageDataType type_;
    const GLvoid* data_;
  };
}

class ImageView1D : public detail::ImageView {
 public:
  ImageView1D(ImageFormat format, ImageDataType type, GLsizei width, const GLvoid* data) :
    detail::ImageView(format, type, data),
    width_(width) {}

  GLsizei width() const { return width_; }

 private:
  GLsizei width_;
};

class ImageView2D : public detail::ImageView {
 public:
  ImageView2D(ImageFormat format, ImageDataType type, GLsizei width, GLsizei height, const GLvoid* data) :
    detail::ImageView(format, type, data),
    width_(width), height_(height) {}

  GLsizei width() const { return width_; }
  GLsizei height() const { return height_; }

 private:
  GLsizei width_;
  GLsizei height_;
};

class ImageView3D : public detail::ImageView {
 public:
  ImageView3D(ImageFormat format, ImageDataType type, GLsizei width, GLsizei height, GLsizei depth, const GLvoid* data) :
    detail::ImageView(format, type, data),
    width_(width), height_(height), depth_(depth) {}

  GLsizei width() const { return width_; }
  GLsizei height() const { return height_; }
  GLsizei depth() const { return depth_; }

 private:
  GLsizei width_;
  GLsizei height_;
  GLsizei depth_;
};

}} // namespace Slum::GL
