#pragma once

#include <cstddef>

namespace Slum { namespace GL {

namespace detail {
  template<class T>
  class ArrayView {
   public:

    ArrayView(const T* data, std::size_t size) :
        data_(data), size_(size) {}

    const T* data() const { return data_; }
    std::size_t size() const { return size_; }

   private:
    const T* data_;
    std::size_t size_;
  };
}

template<class T>
class ArrayView : public detail::ArrayView<T> {
 public:
  using detail::ArrayView<T>::ArrayView;

  ArrayView(const std::nullptr_t& null) :
      detail::ArrayView<T>(null, 0) {}

  ArrayView(const T& value) :
      detail::ArrayView<T>(&value, 1) {}

  template<std::size_t Size>
  ArrayView(const T (&array)[Size]) :
      detail::ArrayView<T>(array, Size) {}

  template<template<class, std::size_t, class...> class Container, std::size_t Size, class... Args>
  ArrayView(const Container<T, Size, Args...>& container) :
      detail::ArrayView<T>(container.data(), Size) {}

  template<template<class, class...> class Container, class... Args>
  ArrayView(const Container<T, Args...>& container) :
      detail::ArrayView<T>(container.data(), container.size()) {}
};

template<>
class ArrayView<void> : public detail::ArrayView<void> {
 public:
  using detail::ArrayView<void>::ArrayView;

  ArrayView(const std::nullptr_t& null) :
      detail::ArrayView<void>(null, 0) {}

  template<class U>
  ArrayView(const U& value) :
      detail::ArrayView<void>(&value, sizeof(U)) {}

  template<class U, std::size_t Size>
  ArrayView(const U (&array)[Size]) :
      detail::ArrayView<void>(array, Size * sizeof(U)) {}

  template<class U, template<class, std::size_t, class...> class Container, std::size_t Size, class... Args>
  ArrayView(const Container<U, Size, Args...>& container) :
      detail::ArrayView<void>(container.data(), Size * sizeof(U)) {}

  template<class U, template<class, class...> class Container, class... Args>
  ArrayView(const Container<U, Args...>& container) :
      detail::ArrayView<void>(container.data(), container.size() * sizeof(U)) {}

  template<class U>
  ArrayView(const ArrayView<U>& other) :
      detail::ArrayView<void>(other.data(), other.size() * sizeof(U)) {}
};

}} // namespace Slum::GL
