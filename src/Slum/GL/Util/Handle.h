#pragma once

#include <utility>

namespace Slum { namespace GL {

template<class T>
class Handle {
 public:

  Handle(const T& id) :
      id_(id) {}

  Handle(Handle&& other) noexcept :
      id_(other.id_) {
    other.id_ = 0;
  }

  Handle& operator=(Handle&& other) noexcept {
    using std::swap;
    swap(id_, other.id_);
    return *this;
  }

  Handle() = default;
  Handle(const Handle&) = delete;
  Handle& operator=(const Handle&) = delete;

  T* operator&() {
    return &id_;
  }

  const T* operator&() const {
    return &id_;
  }

  operator T&() {
    return id_;
  }

  operator const T&() const {
    return id_;
  }

 private:
  T id_ = 0;
};

}} // namespace Slum::GL
