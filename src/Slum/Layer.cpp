#include "Layer.h"
#include "Application.h"

namespace Slum {

Layer::Layer(Application& parent) : app_(parent) {
  app_.addLayer(this);
}

Layer::~Layer() {
  app_.removeLayer(this);
}

} // namespace Slum
