#pragma once

#include "Common/Types.h"

namespace Slum { namespace Noise {

class Engine {
 public:
  virtual ~Engine() = default;

  virtual float at(float x, float y) const = 0;
  virtual float at(float x, float y, float z) const = 0;

  virtual void fill(float* data, size_t size, IntVec2 offset, IntVec2 dimensions) const = 0;
  virtual void fill(float* data, size_t size, IntVec3 offset, IntVec3 dimensions) const = 0;

  template<class Container>
  void fill(Container& container, IntVec2 offset, IntVec2 dimensions) const;
  template<class Container>
  void fill(Container& container, IntVec3 offset, IntVec3 dimensions) const;
};

template<class Container>
void Engine::fill(Container& container, IntVec2 offset, IntVec2 dimensions) const {
  fill(container.data(), container.size(), offset, dimensions);
}

template<class Container>
void Engine::fill(Container& container, IntVec3 offset, IntVec3 dimensions) const {
  fill(container.data(), container.size(), offset, dimensions);
}

}} // namespace Slum::Noise
