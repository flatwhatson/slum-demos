#pragma once

#include "NoiseEngine.h"
#include "Common/Constants.h"
#include "Common/Interp.h"
#include "Common/ValCoord.h"

#include <boost/simd/config.hpp>
#include <boost/simd/function/floor.hpp>
#include <boost/simd/function/toint.hpp>

namespace Slum { namespace Noise {

class ValueNoise : public NoiseImplementation<ValueNoise> {
 public:

  template<class Int, class Float>
  BOOST_FORCEINLINE
  static Float single(Int seed, Float x, Float y) {
    using boost::simd::floor;
    using boost::simd::toint;

    Float xs = floor(x);
    Float ys = floor(y);

    Int x0 = toint(xs) * XPrime<Int>;
    Int y0 = toint(ys) * YPrime<Int>;

    Int x1 = x0 + XPrime<Int>;
    Int y1 = y0 + YPrime<Int>;

    xs = InterpQuintic(x - xs);
    ys = InterpQuintic(y - ys);

    return
      Lerp(
        Lerp(ValCoord(seed, x0, y0), ValCoord(seed, x1, y0), xs),
        Lerp(ValCoord(seed, x0, y1), ValCoord(seed, x1, y1), xs), ys);
  }

  template<class Int, class Float>
  BOOST_FORCEINLINE
  static Float single(Int seed, Float x, Float y, Float z) {
    using boost::simd::floor;
    using boost::simd::toint;

    Float xs = floor(x);
    Float ys = floor(y);
    Float zs = floor(z);

    Int x0 = toint(xs) * XPrime<Int>;
    Int y0 = toint(ys) * YPrime<Int>;
    Int z0 = toint(zs) * ZPrime<Int>;

    Int x1 = x0 + XPrime<Int>;
    Int y1 = y0 + YPrime<Int>;
    Int z1 = z0 + ZPrime<Int>;

    xs = InterpQuintic(x - xs);
    ys = InterpQuintic(y - ys);
    zs = InterpQuintic(z - zs);

    return
      Lerp(
        Lerp(
          Lerp(ValCoord(seed, x0, y0, z0), ValCoord(seed, x1, y0, z0), xs),
          Lerp(ValCoord(seed, x0, y1, z0), ValCoord(seed, x1, y1, z0), xs), ys),
        Lerp(
          Lerp(ValCoord(seed, x0, y0, z1), ValCoord(seed, x1, y0, z1), xs),
          Lerp(ValCoord(seed, x0, y1, z1), ValCoord(seed, x1, y1, z1), xs), ys), zs);
  }

};

}} // namespace Slum::Noise
