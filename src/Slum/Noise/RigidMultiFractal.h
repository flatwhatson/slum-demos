#pragma once

#include "FractalEngine.h"
#include "Common/Constants.h"

#include <boost/simd/config.hpp>
#include <boost/simd/function/abs.hpp>
#include <boost/simd/function/fnms.hpp>

namespace Slum { namespace Noise {

template<class Noise>
class RigidMultiFractal : public FractalImplementation<RigidMultiFractal<Noise>> {
 public:

  template<class Int, class Float>
  BOOST_FORCEINLINE
  static Float single(int octaves, Float lacunarity, Float gain, Float bounding, Int seed, Float x, Float y) {
    using boost::simd::abs;
    using boost::simd::fnms;

    Float amplitude = One<Float>;
    Float result = One<Float> - abs(Noise::single(seed, x, y));

    for (int i = 1; i < octaves; ++i) {
      seed += One<Int>;
      x *= lacunarity;
      y *= lacunarity;

      amplitude *= gain;
      result = fnms(One<Float> - abs(Noise::single(seed, x, y)), amplitude, result);
    }

    return result;
  }

  template<class Int, class Float>
  BOOST_FORCEINLINE
  static Float single(int octaves, Float lacunarity, Float gain, Float bounding, Int seed, Float x, Float y, Float z) {
    using boost::simd::abs;
    using boost::simd::fnms;

    Float amplitude = One<Float>;
    Float result = One<Float> - abs(Noise::single(seed, x, y, z));

    for (int i = 1; i < octaves; ++i) {
      seed += One<Int>;
      x *= lacunarity;
      y *= lacunarity;
      z *= lacunarity;

      amplitude *= gain;
      result = fnms(One<Float> - abs(Noise::single(seed, x, y, z)), amplitude, result);
    }

    return result;
  }

};

}} // namespace Slum::Noise
