#pragma once

#include "FractalEngine.h"
#include "Common/Constants.h"

#include <boost/simd/config.hpp>
#include <boost/simd/function/fma.hpp>

namespace Slum { namespace Noise {

template<class Noise>
class FbmFractal : public FractalImplementation<FbmFractal<Noise>> {
 public:

  template<class Int, class Float>
  BOOST_FORCEINLINE
  static Float single(int octaves, Float lacunarity, Float gain, Float bounding, Int seed, Float x, Float y) {
    using boost::simd::fma;

    Float amplitude = One<Float>;
    Float result = Noise::single(seed, x, y);

    for (int i = 1; i < octaves; ++i) {
      seed += One<Int>;
      x *= lacunarity;
      y *= lacunarity;

      amplitude *= gain;
      result = fma(Noise::single(seed, x, y), amplitude, result);
    }

    return result * bounding;
  }

  template<class Int, class Float>
  BOOST_FORCEINLINE
  static Float single(int octaves, Float lacunarity, Float gain, Float bounding, Int seed, Float x, Float y, Float z) {
    using boost::simd::fma;

    Float amplitude = One<Float>;
    Float result = Noise::single(seed, x, y, z);

    for (int i = 1; i < octaves; ++i) {
      seed += One<Int>;
      x *= lacunarity;
      y *= lacunarity;
      z *= lacunarity;

      amplitude *= gain;
      result = fma(Noise::single(seed, x, y, z), amplitude, result);
    }

    return result * bounding;
  }

};

}} // namespace Slum::Noise
