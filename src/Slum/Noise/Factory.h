#pragma once

#include "NoiseEngine.h"
#include "FractalEngine.h"

#include "ValueNoise.h"
#include "CubicNoise.h"
#include "PerlinNoise.h"
#include "SimplexNoise.h"
#include "WhiteNoise.h"

#include "NoFractal.h"
#include "FbmFractal.h"
#include "BillowFractal.h"
#include "RigidMultiFractal.h"

#include <memory>

namespace Slum { namespace Noise {

enum class NoiseType {
  ValueNoise,
  CubicNoise,
  PerlinNoise,
  SimplexNoise,
  WhiteNoise,
};

enum class FractalType {
  NoFractal,
  FbmFractal,
  BillowFractal,
  RigidMultiFractal,
};

std::unique_ptr<NoiseEngine> MakeNoiseEngine(NoiseType noiseType) {
  switch (noiseType) {
    case NoiseType::ValueNoise:   return std::make_unique<ValueNoise>();
    case NoiseType::CubicNoise:   return std::make_unique<CubicNoise>();
    case NoiseType::PerlinNoise:  return std::make_unique<PerlinNoise>();
    case NoiseType::SimplexNoise: return std::make_unique<SimplexNoise>();
    case NoiseType::WhiteNoise:   return std::make_unique<WhiteNoise>();
  }
  return nullptr;
}

std::unique_ptr<FractalEngine> MakeFractalEngine(NoiseType noiseType, FractalType fractalType) {
  switch (noiseType) {
    case NoiseType::ValueNoise:
      switch (fractalType) {
        case FractalType::NoFractal:         return std::make_unique<NoFractal<ValueNoise>>();
        case FractalType::FbmFractal:        return std::make_unique<FbmFractal<ValueNoise>>();
        case FractalType::BillowFractal:     return std::make_unique<BillowFractal<ValueNoise>>();
        case FractalType::RigidMultiFractal: return std::make_unique<RigidMultiFractal<ValueNoise>>();
      }
    case NoiseType::CubicNoise:
      switch (fractalType) {
        case FractalType::NoFractal:         return std::make_unique<NoFractal<CubicNoise>>();
        case FractalType::FbmFractal:        return std::make_unique<FbmFractal<CubicNoise>>();
        case FractalType::BillowFractal:     return std::make_unique<BillowFractal<CubicNoise>>();
        case FractalType::RigidMultiFractal: return std::make_unique<RigidMultiFractal<CubicNoise>>();
      }
    case NoiseType::PerlinNoise:
      switch (fractalType) {
        case FractalType::NoFractal:         return std::make_unique<NoFractal<PerlinNoise>>();
        case FractalType::FbmFractal:        return std::make_unique<FbmFractal<PerlinNoise>>();
        case FractalType::BillowFractal:     return std::make_unique<BillowFractal<PerlinNoise>>();
        case FractalType::RigidMultiFractal: return std::make_unique<RigidMultiFractal<PerlinNoise>>();
      }
    case NoiseType::SimplexNoise:
      switch (fractalType) {
        case FractalType::NoFractal:         return std::make_unique<NoFractal<SimplexNoise>>();
        case FractalType::FbmFractal:        return std::make_unique<FbmFractal<SimplexNoise>>();
        case FractalType::BillowFractal:     return std::make_unique<BillowFractal<SimplexNoise>>();
        case FractalType::RigidMultiFractal: return std::make_unique<RigidMultiFractal<SimplexNoise>>();
      }
    case NoiseType::WhiteNoise:              return std::make_unique<NoFractal<WhiteNoise>>();
  }
  return nullptr;
}

}} // namespace Slum::Noise
