#pragma once

#include "NoiseEngine.h"
#include "Common/Constants.h"
#include "Common/ValCoord.h"

#include <boost/config.hpp>
#include <boost/simd/function/bitwise_cast.hpp>

namespace Slum { namespace Noise {

class WhiteNoise : public NoiseImplementation<WhiteNoise> {
 public:
  using NoiseImplementation<WhiteNoise>::fill;

  template<class Int, class Float>
  BOOST_FORCEINLINE
  static Float single(Int seed, Float x, Float y) {
    using boost::simd::bitwise_cast;

    Int ix = bitwise_cast<Int>(x);
    Int iy = bitwise_cast<Int>(y);

    return ValCoord(seed,
        (ix ^ ix >> 16) * XPrime<Int>,
        (iy ^ iy >> 16) * YPrime<Int>);
  }

  template<class Int, class Float>
  BOOST_FORCEINLINE
  static Float single(Int seed, Float x, Float y, Float z) {
    using boost::simd::bitwise_cast;

    Int ix = bitwise_cast<Int>(x);
    Int iy = bitwise_cast<Int>(y);
    Int iz = bitwise_cast<Int>(z);

    return ValCoord(seed,
        (ix ^ ix >> 16) * XPrime<Int>,
        (iy ^ iy >> 16) * YPrime<Int>,
        (iz ^ iz >> 16) * ZPrime<Int>);
  }

  void fill(float* data, size_t size, IntVec2 offset, IntVec2 dimensions) const override {
    const IntPack seed(seed_);
    const IntVec2 frequency(XPrime<int>, YPrime<int>);

    Fill(data, size, offset, dimensions, frequency, [&](IntPack x, IntPack y) {
      return ValCoord(seed, x, y);
    });
  }

  void fill(float* data, size_t size, IntVec3 offset, IntVec3 dimensions) const override {
    const IntPack seed(seed_);
    const IntVec3 frequency(XPrime<int>, YPrime<int>, ZPrime<int>);

    Fill(data, size, offset, dimensions, frequency, [&](IntPack x, IntPack y, IntPack z) {
      return ValCoord(seed, x, y, z);
    });
  }

};

}} // namespace Slum::Noise
