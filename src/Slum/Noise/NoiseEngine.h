#pragma once

#include "Engine.h"
#include "Common/Types.h"
#include "Common/Fill2D.h"
#include "Common/Fill3D.h"

#include <cstddef>
#include <utility>

namespace Slum { namespace Noise {

class NoiseEngine : public Engine {
 public:
  int   getSeed()      const { return seed_; }
  float getFrequency() const { return frequency_; }

  void setSeed      (int seed)        { seed_      = seed; }
  void setFrequency (float frequency) { frequency_ = frequency; }

 protected:
  int seed_ = 1337;
  float frequency_ = 0.01f;
};

template<class Noise>
class NoiseImplementation : public NoiseEngine {
 public:
  using NoiseEngine::at;
  float at(float x, float y) const override;
  float at(float x, float y, float z) const override;

  using NoiseEngine::fill;
  void fill(float* data, size_t size, IntVec2 offset, IntVec2 dimensions) const override;
  void fill(float* data, size_t size, IntVec3 offset, IntVec3 dimensions) const override;
};

template<class Noise>
float NoiseImplementation<Noise>::at(float x, float y) const {
  return Noise::single(seed_, x, y);
}

template<class Noise>
float NoiseImplementation<Noise>::at(float x, float y, float z) const {
  return Noise::single(seed_, x, y, z);
}

template<class Noise>
void NoiseImplementation<Noise>::fill(float* data, size_t size, IntVec2 offset, IntVec2 dimensions) const {
  const IntPack seed(seed_);
  const FloatVec2 frequency(frequency_);

  Fill(data, size, offset, dimensions, frequency, [&](FloatPack x, FloatPack y) {
    return Noise::single(seed, x, y);
  });
}

template<class Noise>
void NoiseImplementation<Noise>::fill(float* data, size_t size, IntVec3 offset, IntVec3 dimensions) const {
  const IntPack seed(seed_);
  const FloatVec3 frequency(frequency_);

  Fill(data, size, offset, dimensions, frequency, [&](FloatPack x, FloatPack y, FloatPack z) {
    return Noise::single(seed, x, y, z);
  });
}

}} // namespace Slum::Noise
