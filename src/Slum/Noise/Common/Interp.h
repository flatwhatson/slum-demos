#pragma once

#include "Constants.h"

#include <boost/simd/config.hpp>
#include <boost/simd/function/fma.hpp>
#include <boost/simd/function/fms.hpp>
#include <boost/simd/function/sqr.hpp>

namespace Slum { namespace Noise {

template<class Float>
BOOST_FORCEINLINE
Float InterpQuintic(Float t) {
  using boost::simd::fma;
  using boost::simd::fms;
  using boost::simd::sqr;

  Float r;
  r = fms(t, Six<Float>, Fifteen<Float>);
  r = fma(r, t, Ten<Float>);
  return r * t * sqr(t);
}

template<class Float>
BOOST_FORCEINLINE
Float Lerp(Float a, Float b, Float t) {
  using boost::simd::fma;
  return fma(b - a, t, a);
}

template<class Float>
BOOST_FORCEINLINE
Float CubicLerp(Float a, Float b, Float c, Float d, Float t) {
  using boost::simd::fma;
  using boost::simd::sqr;

  Float t2 = sqr(t);
  Float q = a - b;
  Float p = d - c - q;

  Float r;
  r = fma(t, c - a, b);
  r = fma(t2, q - p, r);
  r = fma(t2, t * p, r);
  return r;
}

}} // namespace Slum::Noise
