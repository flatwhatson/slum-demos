#pragma once

#include "Types.h"

#include <boost/simd/function/sqrt.hpp>

#include <numeric>

#define SLUM_NOISE_CONSTANT(Name, Value)            \
  template<class T> static const T Name = T(Value);

namespace Slum { namespace Noise {

SLUM_NOISE_CONSTANT(PackSizeV, PackSize);
SLUM_NOISE_CONSTANT(Increment, []{ IntArray v; std::iota(begin(v), end(v), 0); return v; }());

SLUM_NOISE_CONSTANT(XPrime, 1619);
SLUM_NOISE_CONSTANT(YPrime, 31337);
SLUM_NOISE_CONSTANT(ZPrime, 6971);

SLUM_NOISE_CONSTANT(HashPrime, 60493);
SLUM_NOISE_CONSTANT(Hash2Float, 1.0f/2147483648.0f);
SLUM_NOISE_CONSTANT(CubicBounding, 1.0f/(1.5f*1.5f*1.5f));

SLUM_NOISE_CONSTANT(Zero, 0);
SLUM_NOISE_CONSTANT(ZeroPointFive, 0.5f);
SLUM_NOISE_CONSTANT(ZeroPointSix, 0.6f);
SLUM_NOISE_CONSTANT(One, 1);
SLUM_NOISE_CONSTANT(Two, 2);
SLUM_NOISE_CONSTANT(Six, 6);
SLUM_NOISE_CONSTANT(Eight, 8);
SLUM_NOISE_CONSTANT(Ten, 10);
SLUM_NOISE_CONSTANT(Twelve, 12);
SLUM_NOISE_CONSTANT(Thirteen, 13);
SLUM_NOISE_CONSTANT(Fifteen, 15);
SLUM_NOISE_CONSTANT(ThirtyTwo, 32);
SLUM_NOISE_CONSTANT(Seventy, 70);

SLUM_NOISE_CONSTANT(SimplexF2, 0.5f * (boost::simd::sqrt(3.0f) - 1.0f));
SLUM_NOISE_CONSTANT(SimplexG2, (3.0f - boost::simd::sqrt(3.0f)) / 6.0f);
SLUM_NOISE_CONSTANT(SimplexG22, 2.0f * SimplexG2<T> - 1.0f);
SLUM_NOISE_CONSTANT(SimplexF3, 1.0f/3.0f);
SLUM_NOISE_CONSTANT(SimplexG3, 1.0f/6.0f);
SLUM_NOISE_CONSTANT(SimplexG33, 3.0f * SimplexG3<T> - 1.0f);

}} // namespace Slum::Noise
