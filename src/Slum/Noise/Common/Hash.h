#pragma once

#include "Constants.h"

#include <boost/simd/config.hpp>
#include <boost/simd/function/sqr.hpp>

namespace Slum { namespace Noise {

template<class Int>
BOOST_FORCEINLINE
Int HashHB(Int seed, Int x, Int y) {
  using boost::simd::sqr;
  Int hash = seed ^ x ^ y;
  return hash * sqr(hash) * HashPrime<Int>;
}

template<class Int>
BOOST_FORCEINLINE
Int HashHB(Int seed, Int x, Int y, Int z) {
  using boost::simd::sqr;
  Int hash = seed ^ x ^ y ^ z;
  return hash * sqr(hash) * HashPrime<Int>;
}

template<class Int>
BOOST_FORCEINLINE
Int Hash(Int seed, Int x, Int y) {
  Int hash = HashHB(seed, x, y);
  return hash ^ hash >> 13;
}

template<class Int>
BOOST_FORCEINLINE
Int Hash(Int seed, Int x, Int y, Int z) {
  Int hash = HashHB(seed, x, y, z);
  return hash ^ hash >> 13;
}

}} // namespace Slum::Noise
