#pragma once

#include "Types.h"
#include "Constants.h"

#include <boost/simd/config.hpp>
#include <boost/simd/function/aligned_store.hpp>
#include <boost/simd/function/fma.hpp>
#include <boost/simd/function/tofloat.hpp>
#include <boost/simd/memory/is_aligned.hpp>

#include <cassert>
#include <cstddef>

namespace Slum { namespace Noise {

template<class Func>
BOOST_FORCEINLINE
void FastFill(float* data, size_t size, IntVec3 offs, IntVec3 dims, FloatVec3 freq, Func func) {
  using boost::simd::aligned_store;
  using boost::simd::is_aligned;
  using boost::simd::tofloat;

  assert(dims.x > 0);
  assert(dims.y > 0);
  assert(dims.z > 0);
  assert((dims.z & (PackSize - 1)) == 0);
  assert(data != nullptr);
  assert(size >= size_t(dims.x * dims.y * dims.z));
  assert(is_aligned(data));

  const IntPack ixBase(offs.x);
  const IntPack iyBase(offs.y);
  const IntPack izBase(Increment<IntPack> + offs.z);

  const IntPack ixStep(One<IntPack>);
  const IntPack iyStep(One<IntPack>);
  const IntPack izStep(PackSizeV<IntPack>);

  const FloatPack xFreqV(freq.x);
  const FloatPack yFreqV(freq.y);
  const FloatPack zFreqV(freq.z);

  IntPack ix = ixBase;
  for (int x = 0; x < dims.x; ++x) {
    FloatPack xV = tofloat(ix) * xFreqV;

    IntPack iy = iyBase;
    for (int y = 0; y < dims.y; ++y) {
      FloatPack yV = tofloat(iy) * yFreqV;

      IntPack iz = izBase;
      for (int z = 0; z < dims.z; z += PackSize) {
        FloatPack zV = tofloat(iz) * zFreqV;

        FloatPack result = func(xV, yV, zV);
        aligned_store(result, data);
        data += PackSize;

        iz += izStep;
      }
      iy += iyStep;
    }
    ix += ixStep;
  }
}

template<class Func>
BOOST_FORCEINLINE
void FastFill(float* data, size_t size, IntVec3 offs, IntVec3 dims, IntVec3 freq, Func func) {
  using boost::simd::aligned_store;
  using boost::simd::is_aligned;

  assert(dims.x > 0);
  assert(dims.y > 0);
  assert(dims.z > 0);
  assert((dims.z & (PackSize - 1)) == 0);
  assert(data != nullptr);
  assert(size >= size_t(dims.x * dims.y * dims.z));
  assert(is_aligned(data));

  const IntPack xBase(offs.x * freq.x);
  const IntPack yBase(offs.y * freq.y);
  const IntPack zBase((Increment<IntPack> + offs.z) * freq.z);

  const IntPack xStep(freq.x);
  const IntPack yStep(freq.y);
  const IntPack zStep(PackSize * freq.z);

  IntPack xV = xBase;
  for (int x = 0; x < dims.x; ++x) {
    IntPack yV = yBase;
    for (int y = 0; y < dims.y; ++y) {
      IntPack zV = zBase;
      for (int z = 0; z < dims.z; z += PackSize) {

        FloatPack result = func(xV, yV, zV);
        aligned_store(result, data);
        data += PackSize;

        zV += zStep;
      }
      yV += yStep;
    }
    xV += xStep;
  }
}

template<class Freq, class Func>
BOOST_FORCEINLINE
void Fill(float* data, size_t size, IntVec3 offs, IntVec3 dims, Vec3<Freq> freq, Func func) {
  using boost::simd::aligned_store;
  using boost::simd::is_aligned;
  using boost::simd::fma;

  if ((dims.z & (PackSize - 1)) == 0) {
    FastFill(data, size, offs, dims, freq, func);
    return;
  }

  assert(dims.x > 0);
  assert(dims.y > 0);
  assert(dims.z > 0);
  assert(data != nullptr);
  assert(size >= size_t(dims.x * dims.y * dims.z));
  assert(is_aligned(data));

  using ValuePack = PackFor<Freq>;

  const ValuePack xBaseV(offs.x * freq.x);
  const ValuePack yBaseV(offs.y * freq.y);
  const ValuePack zBaseV(offs.z * freq.z);

  const ValuePack xFreqV(freq.x);
  const ValuePack yFreqV(freq.y);
  const ValuePack zFreqV(freq.z);

  const size_t n = dims.x * dims.y * dims.z;

  int x = 0, y = 0, z = 0;
  for (size_t i = 0; i < n; i += PackSize) {
    IntArray xs, ys, zs;

    for (int j = 0; j < PackSize; ++j) {
      xs[j] = x;
      ys[j] = y;
      zs[j] = z;
      if (++z >= dims.z) {
        z = 0;
        if (++y >= dims.y) {
          y = 0;
          if (++x >= dims.x) {
            x = 0;
          }
        }
      }
    }

    ValuePack xV(xs), yV(ys), zV(zs);
    xV = fma(xV, xFreqV, xBaseV);
    yV = fma(yV, yFreqV, yBaseV);
    zV = fma(zV, zFreqV, zBaseV);

    FloatPack result = func(xV, yV, zV);
    aligned_store(result, data);
    data += PackSize;
  }
}

}} // namespace Slum::Noise
