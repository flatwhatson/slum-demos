#pragma once

#include "Types.h"
#include "Constants.h"
#include "Hash.h"

#include <boost/simd/config.hpp>
#include <boost/simd/function/tofloat.hpp>

namespace Slum { namespace Noise {

template<class Int, class Float = FloatFor<Int>>
BOOST_FORCEINLINE
Float ValCoord(Int seed, Int x, Int y) {
  using boost::simd::tofloat;
  Int hash = HashHB(seed, x, y);
  return tofloat(hash) * Hash2Float<Float>;
}

template<class Int, class Float = FloatFor<Int>>
BOOST_FORCEINLINE
Float ValCoord(Int seed, Int x, Int y, Int z) {
  using boost::simd::tofloat;
  Int hash = HashHB(seed, x, y, z);
  return tofloat(hash) * Hash2Float<Float>;
}

}} // namespace Slum::Noise
