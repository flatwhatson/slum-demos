#pragma once

#include "Constants.h"
#include "Hash.h"

#include <boost/simd/config.hpp>
#include <boost/simd/function/bitwise_cast.hpp>
#include <boost/simd/function/bitwise_xor.hpp>
#include <boost/simd/function/if_else.hpp>

namespace Slum { namespace Noise {

/********************************
* Coordinate gradient algorithm *
*********************************

 Regular implementation:

  float GRAD_X[] = { 1, -1,  1, -1,  1, -1,  1, -1,  0,  0,  0,  0 };
  float GRAD_Y[] = { 1,  1, -1, -1,  0,  0,  0,  0,  1, -1,  1, -1 };
  float GRAD_Z[] = { 0,  0,  0,  0,  1,  1, -1, -1,  1,  1, -1, -1 };

  float GradCoord(float x, float y, float z) {
    int p = Hash(x,y,z) % 12;
    return x*GRAD_X[p] + y*GRAD_Y[p] + z*GRAD_Z[p];
  }

 Jordan's SIMD implementation:

  hash=  h13=    u=  v=  h1= h2= grad=
   0000    0      x   y   0   0   x + y
   0001    1      x   y   1   0  -x + y
   0010    0      x   y   0   1   x - y
   0011    1 <2   x   y   1   1  -x - y
   0100    4      x   z   0   0   x + z
   0101    5      x   z   1   0  -x + z
   0110    4      x   z   0   1   x - z
   0111    5 <8   x   z   1   1  -x - z
   1000    8      y   z   0   0   y + z
   1001    9      y   z   1   0  -y + z
   1010    8      y   z   0   1   y - z
   1011    9      y   z   1   1  -y - z
   1100   12 =12  y   x   0   0   y + x  "extra"
   1101   13      y   z   1   0  -y + z  "extra"
   1110   12 =12  y   x   0   1   y - x  "extra"
   1111   13      y   z   1   1  -y - z  "extra"

****************************************/

namespace detail {

template<class Int, class Float>
BOOST_FORCEINLINE
Float GradCoord(Int hash, Float x, Float y, Float z) {
  using boost::simd::bitwise_cast;
  using boost::simd::bitwise_xor;
  using boost::simd::if_else;

  Int hasha13 = hash & Thirteen<Int>;
  auto lt8 = hasha13 < Eight<Int>;
  auto lt4 = hasha13 < Two<Int>;
  auto h12o14 = hasha13 == Twelve<Int>;

  Float u = if_else(lt8, x, y);
  Float v = if_else(lt4, y, if_else(h12o14, x, z));

  Float h1 = bitwise_cast<Float>(hash << 31);
  Float h2 = bitwise_cast<Float>((hash & Two<Int>) << 30);

  return bitwise_xor(u, h1) + bitwise_xor(v, h2);
}

} // namespace detail

template<class Int, class Float>
BOOST_FORCEINLINE
Float GradCoord(Int seed, Int xi, Int yi, Float x, Float y) {
  Int hash = Hash(seed, xi, yi);
  return detail::GradCoord(hash, x, y, Zero<Float>);
}

template<class Int, class Float>
BOOST_FORCEINLINE
Float GradCoord(Int seed, Int xi, Int yi, Int zi, Float x, Float y, Float z) {
  Int hash = Hash(seed, xi, yi, zi);
  return detail::GradCoord(hash, x, y, z);
}

}} // namespace Slum::Noise
