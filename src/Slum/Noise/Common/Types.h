#pragma once

#include <boost/simd/pack.hpp>
#include <boost/simd/function/tofloat.hpp>
#include <boost/simd/function/toint.hpp>

#include <array>
#include <utility>

namespace Slum { namespace Noise {

static constexpr int PackSize = boost::simd::pack<float>::static_size;

using IntPack = boost::simd::pack<int, PackSize>;
using FloatPack = boost::simd::pack<float, PackSize>;

using IntArray = std::array<int, PackSize>;
using FloatArray = std::array<float, PackSize>;

template<class T>
using PackFor = decltype(boost::simd::pack<T, PackSize>());

template<class Int>
using FloatFor = decltype(boost::simd::tofloat(std::declval<Int>()));

template<class Float>
using IntFor = decltype(boost::simd::toint(std::declval<Float>()));

template<class T>
struct Vec2 {
  Vec2(T x, T y) : x(x), y(y) {}
  Vec2(T v) : Vec2(v, v) {}
  Vec2() : Vec2(0) {}
  T x, y;
};

template<class T>
struct Vec3 {
  Vec3(T x, T y, T z) : x(x), y(y), z(z) {}
  Vec3(T v) : Vec3(v, v, v) {}
  Vec3() : Vec3(0) {}
  T x, y, z;
};

using IntVec2 = Vec2<int>;
using FloatVec2 = Vec2<float>;

using IntVec3 = Vec3<int>;
using FloatVec3 = Vec3<float>;

}} // namespace Slum::Noise
