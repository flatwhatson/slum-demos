#pragma once

#include "NoiseEngine.h"
#include "Common/Constants.h"
#include "Common/GradCoord.h"

#include <boost/simd/config.hpp>
#include <boost/simd/function/floor.hpp>
#include <boost/simd/function/fnms.hpp>
#include <boost/simd/function/if_else.hpp>
#include <boost/simd/function/sqr.hpp>
#include <boost/simd/function/toint.hpp>

namespace Slum { namespace Noise {

class SimplexNoise : public NoiseImplementation<SimplexNoise> {
 public:

  template<class Int, class Float>
  BOOST_FORCEINLINE
  static Float single(Int seed, Float x, Float y) {
    using boost::simd::floor;
    using boost::simd::fnms;
    using boost::simd::if_else;
    using boost::simd::sqr;
    using boost::simd::toint;

    Float f = (x + y) * SimplexF2<Float>;
    Float x0 = floor(x + f);
    Float y0 = floor(y + f);

    Int i = toint(x0) * XPrime<Int>;
    Int j = toint(y0) * YPrime<Int>;

    Float g = (x0 + y0) * SimplexG2<Float>;
    x0 = x - (x0 - g);
    y0 = y - (y0 - g);

    auto x0_gt_y0 = x0 > y0;
    auto i1 = x0_gt_y0;
    auto j1 = !x0_gt_y0;

    Float x1 = x0 - if_else(i1, One<Float>, Zero<Float>) + SimplexG2<Float>;
    Float y1 = y0 - if_else(j1, One<Float>, Zero<Float>) + SimplexG2<Float>;
    Float x2 = x0 + SimplexG22<Float>;
    Float y2 = y0 + SimplexG22<Float>;

    Float t0 = fnms(y0, y0, fnms(x0, x0, ZeroPointFive<Float>));
    Float t1 = fnms(y1, y1, fnms(x1, x1, ZeroPointFive<Float>));
    Float t2 = fnms(y2, y2, fnms(x2, x2, ZeroPointFive<Float>));

    auto n0 = t0 >= Zero<Float>;
    auto n1 = t1 >= Zero<Float>;
    auto n2 = t2 >= Zero<Float>;

    Int gi1 = i + if_else(i1, XPrime<Int>, Zero<Int>);
    Int gj1 = j + if_else(j1, YPrime<Int>, Zero<Int>);
    Int gi2 = i + XPrime<Int>;
    Int gj2 = j + YPrime<Int>;

    Float v0 = sqr(sqr(t0)) * GradCoord(seed, i, j, x0, y0);
    Float v1 = sqr(sqr(t1)) * GradCoord(seed, gi1, gj1, x1, y1);
    Float v2 = sqr(sqr(t2)) * GradCoord(seed, gi2, gj2, x2, y2);

    return Seventy<Float> * (
      if_else(n0, v0, Zero<Float>) +
      if_else(n1, v1, Zero<Float>) +
      if_else(n2, v2, Zero<Float>));
  }

  template<class Int, class Float>
  BOOST_FORCEINLINE
  static Float single(Int seed, Float x, Float y, Float z) {
    using boost::simd::floor;
    using boost::simd::fnms;
    using boost::simd::if_else;
    using boost::simd::sqr;
    using boost::simd::toint;

    Float f = (x + y + z) * SimplexF3<Float>;
    Float x0 = floor(x + f);
    Float y0 = floor(y + f);
    Float z0 = floor(z + f);

    Int i = toint(x0) * XPrime<Int>;
    Int j = toint(y0) * YPrime<Int>;
    Int k = toint(z0) * ZPrime<Int>;

    Float g = (x0 + y0 + z0) * SimplexG3<Float>;
    x0 = x - (x0 - g);
    y0 = y - (y0 - g);
    z0 = z - (z0 - g);

    auto x0_ge_y0 = x0 >= y0;
    auto x0_ge_z0 = x0 >= z0;
    auto y0_ge_z0 = y0 >= z0;

    auto i1 = x0_ge_y0 && x0_ge_z0;
    auto j1 = !x0_ge_y0 && y0_ge_z0;
    auto k1 = !x0_ge_z0 && !y0_ge_z0;

    auto i2 = x0_ge_y0 || x0_ge_z0;
    auto j2 = !x0_ge_y0 || y0_ge_z0;
    auto k2 = !x0_ge_z0 || !y0_ge_z0;

    Float x1 = x0 - if_else(i1, One<Float>, Zero<Float>) + SimplexG3<Float>;
    Float y1 = y0 - if_else(j1, One<Float>, Zero<Float>) + SimplexG3<Float>;
    Float z1 = z0 - if_else(k1, One<Float>, Zero<Float>) + SimplexG3<Float>;
    Float x2 = x0 - if_else(i2, One<Float>, Zero<Float>) + SimplexF3<Float>;
    Float y2 = y0 - if_else(j2, One<Float>, Zero<Float>) + SimplexF3<Float>;
    Float z2 = z0 - if_else(k2, One<Float>, Zero<Float>) + SimplexF3<Float>;
    Float x3 = x0 + SimplexG33<Float>;
    Float y3 = y0 + SimplexG33<Float>;
    Float z3 = z0 + SimplexG33<Float>;

    Float t0 = fnms(z0, z0, fnms(y0, y0, fnms(x0, x0, ZeroPointSix<Float>)));
    Float t1 = fnms(z1, z1, fnms(y1, y1, fnms(x1, x1, ZeroPointSix<Float>)));
    Float t2 = fnms(z2, z2, fnms(y2, y2, fnms(x2, x2, ZeroPointSix<Float>)));
    Float t3 = fnms(z3, z3, fnms(y3, y3, fnms(x3, x3, ZeroPointSix<Float>)));

    auto n0 = t0 >= Zero<Float>;
    auto n1 = t1 >= Zero<Float>;
    auto n2 = t2 >= Zero<Float>;
    auto n3 = t3 >= Zero<Float>;

    Int gi1 = i + if_else(i1, XPrime<Int>, Zero<Int>);
    Int gj1 = j + if_else(j1, YPrime<Int>, Zero<Int>);
    Int gk1 = k + if_else(k1, ZPrime<Int>, Zero<Int>);
    Int gi2 = i + if_else(i2, XPrime<Int>, Zero<Int>);
    Int gj2 = j + if_else(j2, YPrime<Int>, Zero<Int>);
    Int gk2 = k + if_else(k2, ZPrime<Int>, Zero<Int>);
    Int gi3 = i + XPrime<Int>;
    Int gj3 = j + YPrime<Int>;
    Int gk3 = k + ZPrime<Int>;

    Float v0 = sqr(sqr(t0)) * GradCoord(seed, i, j, k, x0, y0, z0);
    Float v1 = sqr(sqr(t1)) * GradCoord(seed, gi1, gj1, gk1, x1, y1, z1);
    Float v2 = sqr(sqr(t2)) * GradCoord(seed, gi2, gj2, gk2, x2, y2, z2);
    Float v3 = sqr(sqr(t3)) * GradCoord(seed, gi3, gj3, gk3, x3, y3, z3);

    return ThirtyTwo<Float> * (
      if_else(n0, v0, Zero<Float>) +
      if_else(n1, v1, Zero<Float>) +
      if_else(n2, v2, Zero<Float>) +
      if_else(n3, v3, Zero<Float>));
  }

};

}} // namespace Slum::Noise
