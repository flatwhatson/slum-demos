#pragma once

#include "FractalEngine.h"
#include "Common/Constants.h"

#include <boost/simd/config.hpp>
#include <boost/simd/function/abs.hpp>
#include <boost/simd/function/fma.hpp>
#include <boost/simd/function/fms.hpp>

namespace Slum { namespace Noise {

template<class Noise>
class BillowFractal : public FractalImplementation<BillowFractal<Noise>> {
 public:

  template<class Int, class Float>
  BOOST_FORCEINLINE
  static Float single(int octaves, Float lacunarity, Float gain, Float bounding, Int seed, Float x, Float y) {
    using boost::simd::abs;
    using boost::simd::fma;
    using boost::simd::fms;

    Float amplitude = One<Float>;
    Float result = fms(abs(Noise::single(seed, x, y)), Two<Float>, One<Float>);

    for (int i = 1; i < octaves; ++i) {
      seed += One<Int>;
      x *= lacunarity;
      y *= lacunarity;

      amplitude *= gain;
      result = fma(fms(abs(Noise::single(seed, x, y)), Two<Float>, One<Float>), amplitude, result);
    }

    return result * bounding;
  }

  template<class Int, class Float>
  BOOST_FORCEINLINE
  static Float single(int octaves, Float lacunarity, Float gain, Float bounding, Int seed, Float x, Float y, Float z) {
    using boost::simd::abs;
    using boost::simd::fma;
    using boost::simd::fms;

    Float amplitude = One<Float>;
    Float result = fms(abs(Noise::single(seed, x, y, z)), Two<Float>, One<Float>);

    for (int i = 1; i < octaves; ++i) {
      seed += One<Int>;
      x *= lacunarity;
      y *= lacunarity;
      z *= lacunarity;

      amplitude *= gain;
      result = fma(fms(abs(Noise::single(seed, x, y, z)), Two<Float>, One<Float>), amplitude, result);
    }

    return result * bounding;
  }

};

}} // namespace Slum::Noise
