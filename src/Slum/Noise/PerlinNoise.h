#pragma once

#include "NoiseEngine.h"
#include "Common/Constants.h"
#include "Common/GradCoord.h"
#include "Common/Interp.h"

#include <boost/simd/config.hpp>
#include <boost/simd/function/floor.hpp>
#include <boost/simd/function/toint.hpp>

namespace Slum { namespace Noise {

class PerlinNoise : public NoiseImplementation<PerlinNoise> {
 public:

  template<class Int, class Float>
  BOOST_FORCEINLINE
  static Float single(Int seed, Float x, Float y) {
    using boost::simd::floor;
    using boost::simd::toint;

    Float xs = floor(x);
    Float ys = floor(y);

    Int x0 = toint(xs) * XPrime<Int>;
    Int y0 = toint(ys) * YPrime<Int>;

    Int x1 = x0 + XPrime<Int>;
    Int y1 = y0 + YPrime<Int>;

    Float xf0 = x - xs;
    Float yf0 = y - ys;

    Float xf1 = xf0 - One<Float>;
    Float yf1 = yf0 - One<Float>;

    xs = InterpQuintic(xf0);
    ys = InterpQuintic(yf0);

    return
      Lerp(
        Lerp(GradCoord(seed, x0, y0, xf0, yf0), GradCoord(seed, x1, y0, xf1, yf0), xs),
        Lerp(GradCoord(seed, x0, y1, xf0, yf1), GradCoord(seed, x1, y1, xf1, yf1), xs), ys);
  }

  template<class Int, class Float>
  BOOST_FORCEINLINE
  static Float single(Int seed, Float x, Float y, Float z) {
    using boost::simd::floor;
    using boost::simd::toint;

    Float xs = floor(x);
    Float ys = floor(y);
    Float zs = floor(z);

    Int x0 = toint(xs) * XPrime<Int>;
    Int y0 = toint(ys) * YPrime<Int>;
    Int z0 = toint(zs) * ZPrime<Int>;

    Int x1 = x0 + XPrime<Int>;
    Int y1 = y0 + YPrime<Int>;
    Int z1 = z0 + ZPrime<Int>;

    Float xf0 = x - xs;
    Float yf0 = y - ys;
    Float zf0 = z - zs;

    Float xf1 = xf0 - One<Float>;
    Float yf1 = yf0 - One<Float>;
    Float zf1 = zf0 - One<Float>;

    xs = InterpQuintic(xf0);
    ys = InterpQuintic(yf0);
    zs = InterpQuintic(zf0);

    return
      Lerp(
        Lerp(
          Lerp(GradCoord(seed, x0, y0, z0, xf0, yf0, zf0), GradCoord(seed, x1, y0, z0, xf1, yf0, zf0), xs),
          Lerp(GradCoord(seed, x0, y1, z0, xf0, yf1, zf0), GradCoord(seed, x1, y1, z0, xf1, yf1, zf0), xs), ys),
        Lerp(
          Lerp(GradCoord(seed, x0, y0, z1, xf0, yf0, zf1), GradCoord(seed, x1, y0, z1, xf1, yf0, zf1), xs),
          Lerp(GradCoord(seed, x0, y1, z1, xf0, yf1, zf1), GradCoord(seed, x1, y1, z1, xf1, yf1, zf1), xs), ys), zs);
  }

};

}} // namespace Slum::Noise
