#pragma once

#include "NoiseEngine.h"
#include "Common/Constants.h"
#include "Common/Interp.h"
#include "Common/ValCoord.h"

#include <boost/simd/config.hpp>
#include <boost/simd/function/floor.hpp>
#include <boost/simd/function/toint.hpp>

namespace Slum { namespace Noise {

class CubicNoise : public NoiseImplementation<CubicNoise> {
 public:

  template<class Int, class Float>
  BOOST_FORCEINLINE
  static Float single(Int seed, Float x, Float y) {
    using boost::simd::floor;
    using boost::simd::toint;

    Float xf1 = floor(x);
    Float yf1 = floor(y);

    Int x1 = toint(xf1) * XPrime<Int>;
    Int y1 = toint(yf1) * YPrime<Int>;

    Int x0 = x1 - XPrime<Int>;
    Int y0 = y1 - YPrime<Int>;

    Int x2 = x1 + XPrime<Int>;
    Int y2 = y1 + YPrime<Int>;

    Int x3 = x2 + XPrime<Int>;
    Int y3 = y2 + YPrime<Int>;

    Float xs = x - xf1;
    Float ys = y - yf1;

    return CubicBounding<Float> * (
      CubicLerp(
        CubicLerp(
          ValCoord(seed, x0, y0), ValCoord(seed, x1, y0),
          ValCoord(seed, x2, y0), ValCoord(seed, x3, y0), xs),
        CubicLerp(
          ValCoord(seed, x0, y1), ValCoord(seed, x1, y1),
          ValCoord(seed, x2, y1), ValCoord(seed, x3, y1), xs),
        CubicLerp(
          ValCoord(seed, x0, y2), ValCoord(seed, x1, y2),
          ValCoord(seed, x2, y2), ValCoord(seed, x3, y2), xs),
        CubicLerp(
          ValCoord(seed, x0, y3), ValCoord(seed, x1, y3),
          ValCoord(seed, x2, y3), ValCoord(seed, x3, y3), xs), ys));
  }

  template<class Int, class Float>
  BOOST_FORCEINLINE
  static Float single(Int seed, Float x, Float y, Float z) {
    using boost::simd::floor;
    using boost::simd::toint;

    Float xf1 = floor(x);
    Float yf1 = floor(y);
    Float zf1 = floor(z);

    Int x1 = toint(xf1) * XPrime<Int>;
    Int y1 = toint(yf1) * YPrime<Int>;
    Int z1 = toint(zf1) * ZPrime<Int>;

    Int x0 = x1 - XPrime<Int>;
    Int y0 = y1 - YPrime<Int>;
    Int z0 = z1 - ZPrime<Int>;

    Int x2 = x1 + XPrime<Int>;
    Int y2 = y1 + YPrime<Int>;
    Int z2 = z1 + ZPrime<Int>;

    Int x3 = x2 + XPrime<Int>;
    Int y3 = y2 + YPrime<Int>;
    Int z3 = z2 + ZPrime<Int>;

    Float xs = x - xf1;
    Float ys = y - yf1;
    Float zs = z - zf1;

    return CubicBounding<Float> * (
      CubicLerp(
        CubicLerp(
          CubicLerp(
            ValCoord(seed, x0, y0, z0), ValCoord(seed, x1, y0, z0),
            ValCoord(seed, x2, y0, z0), ValCoord(seed, x3, y0, z0), xs),
          CubicLerp(
            ValCoord(seed, x0, y1, z0), ValCoord(seed, x1, y1, z0),
            ValCoord(seed, x2, y1, z0), ValCoord(seed, x3, y1, z0), xs),
          CubicLerp(
            ValCoord(seed, x0, y2, z0), ValCoord(seed, x1, y2, z0),
            ValCoord(seed, x2, y2, z0), ValCoord(seed, x3, y2, z0), xs),
          CubicLerp(
            ValCoord(seed, x0, y3, z0), ValCoord(seed, x1, y3, z0),
            ValCoord(seed, x2, y3, z0), ValCoord(seed, x3, y3, z0), xs), ys),
        CubicLerp(
          CubicLerp(
            ValCoord(seed, x0, y0, z1), ValCoord(seed, x1, y0, z1),
            ValCoord(seed, x2, y0, z1), ValCoord(seed, x3, y0, z1), xs),
          CubicLerp(
            ValCoord(seed, x0, y1, z1), ValCoord(seed, x1, y1, z1),
            ValCoord(seed, x2, y1, z1), ValCoord(seed, x3, y1, z1), xs),
          CubicLerp(
            ValCoord(seed, x0, y2, z1), ValCoord(seed, x1, y2, z1),
            ValCoord(seed, x2, y2, z1), ValCoord(seed, x3, y2, z1), xs),
          CubicLerp(
            ValCoord(seed, x0, y3, z1), ValCoord(seed, x1, y3, z1),
            ValCoord(seed, x2, y3, z1), ValCoord(seed, x3, y3, z1), xs), ys),
        CubicLerp(
          CubicLerp(
            ValCoord(seed, x0, y0, z2), ValCoord(seed, x1, y0, z2),
            ValCoord(seed, x2, y0, z2), ValCoord(seed, x3, y0, z2), xs),
          CubicLerp(
            ValCoord(seed, x0, y1, z2), ValCoord(seed, x1, y1, z2),
            ValCoord(seed, x2, y1, z2), ValCoord(seed, x3, y1, z2), xs),
          CubicLerp(
            ValCoord(seed, x0, y2, z2), ValCoord(seed, x1, y2, z2),
            ValCoord(seed, x2, y2, z2), ValCoord(seed, x3, y2, z2), xs),
          CubicLerp(
            ValCoord(seed, x0, y3, z2), ValCoord(seed, x1, y3, z2),
            ValCoord(seed, x2, y3, z2), ValCoord(seed, x3, y3, z2), xs), ys),
        CubicLerp(
          CubicLerp(
            ValCoord(seed, x0, y0, z3), ValCoord(seed, x1, y0, z3),
            ValCoord(seed, x2, y0, z3), ValCoord(seed, x3, y0, z3), xs),
          CubicLerp(
            ValCoord(seed, x0, y1, z3), ValCoord(seed, x1, y1, z3),
            ValCoord(seed, x2, y1, z3), ValCoord(seed, x3, y1, z3), xs),
          CubicLerp(
            ValCoord(seed, x0, y2, z3), ValCoord(seed, x1, y2, z3),
            ValCoord(seed, x2, y2, z3), ValCoord(seed, x3, y2, z3), xs),
          CubicLerp(
            ValCoord(seed, x0, y3, z3), ValCoord(seed, x1, y3, z3),
            ValCoord(seed, x2, y3, z3), ValCoord(seed, x3, y3, z3), xs), ys), zs));
  }

};

}} // namespace Slum::Noise
