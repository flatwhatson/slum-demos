#pragma once

#include "FractalEngine.h"

namespace Slum { namespace Noise {

template<class Noise>
class NoFractal : public FractalImplementation<NoFractal<Noise>> {
 public:

  template<class Int, class Float>
  BOOST_FORCEINLINE
  static Float single(int octaves, Float lacunarity, Float gain, Float bounding, Int seed, Float x, Float y) {
    return Noise::single(seed, x, y);
  }

  template<class Int, class Float>
  BOOST_FORCEINLINE
  static Float single(int octaves, Float lacunarity, Float gain, Float bounding, Int seed, Float x, Float y, Float z) {
    return Noise::single(seed, x, y, z);
  }

};

}} // namespace Slum::Noise
