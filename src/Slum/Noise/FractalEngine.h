#pragma once

#include "NoiseEngine.h"
#include "Common/Types.h"
#include "Common/Fill2D.h"
#include "Common/Fill3D.h"

#include <cstddef>
#include <utility>

namespace Slum { namespace Noise {

class FractalEngine : public NoiseEngine {
 public:
  FractalEngine() { recalculate(); }

  int   getOctaves()    const { return octaves_; }
  float getLacunarity() const { return lacunarity_; }
  float getGain()       const { return gain_; }

  void setOctaves    (int octaves)      { octaves_    = octaves; }
  void setLacunarity (float lacunarity) { lacunarity_ = lacunarity; }
  void setGain       (float gain)       { gain_       = gain; }

 private:
  void recalculate();

 protected:
  int octaves_ = 3;
  float lacunarity_ = 2.0f;
  float gain_ = 0.5f;
  float bounding_;
};

template<class Fractal>
class FractalImplementation : public FractalEngine {
 public:
  using FractalEngine::at;
  float at(float x, float y) const override;
  float at(float x, float y, float z) const override;

  using FractalEngine::fill;
  void fill(float* data, size_t size, IntVec2 offset, IntVec2 dimensions) const override;
  void fill(float* data, size_t size, IntVec3 offset, IntVec3 dimensions) const override;
};

template<class Fractal>
float FractalImplementation<Fractal>::at(float x, float y) const {
  return Fractal::single(octaves_, lacunarity_, gain_, bounding_, seed_, x, y);
}

template<class Fractal>
float FractalImplementation<Fractal>::at(float x, float y, float z) const {
  return Fractal::single(octaves_, lacunarity_, gain_, bounding_, seed_, x, y, z);
}

template<class Fractal>
void FractalImplementation<Fractal>::fill(float* data, size_t size, IntVec2 offset, IntVec2 dimensions) const {
  const IntPack seed(seed_);
  const FloatVec2 frequency(frequency_);
  const int octaves(octaves_);
  const FloatPack lacunarity(lacunarity_);
  const FloatPack gain(gain_);
  const FloatPack bounding(bounding_);

  Fill(data, size, offset, dimensions, frequency, [&](FloatPack x, FloatPack y) {
    return Fractal::single(octaves, lacunarity, gain, bounding, seed, x, y);
  });
}

template<class Fractal>
void FractalImplementation<Fractal>::fill(float* data, size_t size, IntVec3 offset, IntVec3 dimensions) const {
  const IntPack seed(seed_);
  const FloatVec3 frequency(frequency_);
  const int octaves(octaves_);
  const FloatPack lacunarity(lacunarity_);
  const FloatPack gain(gain_);
  const FloatPack bounding(bounding_);

  Fill(data, size, offset, dimensions, frequency, [&](FloatPack x, FloatPack y, FloatPack z) {
    return Fractal::single(octaves, lacunarity, gain, bounding, seed, x, y, z);
  });
}

void FractalEngine::recalculate() {
  float amplitude = gain_;
  float ampFractal = 1.0f;
  for (int i = 1; i < octaves_; i++) {
    ampFractal += amplitude;
    amplitude *= gain_;
  }
  bounding_ = 1.0f / ampFractal;
}

}} // namespace Slum::Noise
