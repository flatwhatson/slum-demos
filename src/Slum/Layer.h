#pragma once

namespace Slum {

class Application;

class Layer {
 public:
  Layer(Application& parent);
  virtual ~Layer();

  Layer(Layer&&) = delete;
  Layer(const Layer&) = delete;
  Layer& operator=(Layer&&) = delete;
  Layer& operator=(const Layer&) = delete;

 protected:
  const Application& app() const { return app_; }
  Application& app() { return app_; }

 private:
  friend class Application;

  virtual void onResize(int width, int height) {}
  virtual void onTextInput(const char* text, bool& captured) {}
  virtual void onKeyPress(int key, int mod, bool& captured) {}
  virtual void onKeyRelease(int key, int mod, bool& captured) {}

  virtual void onMousePress(int button, bool& captured) {}
  virtual void onMouseRelease(int button, bool& captured) {}
  virtual void onMouseScroll(int xscroll, int yscroll, bool& captured) {}
  virtual void onMouseMove(int xpos, int ypos, bool& captured) {}

  virtual void prepare() {}
  virtual void render() {}

 private:
  Application& app_;
};

} // namespace Slum
