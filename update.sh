#!/usr/bin/env bash

PROJECT_DIR=$(dirname $(readlink -f $0))

find $PROJECT_DIR/external -mindepth 2 -maxdepth 2 -name .git \
 | sort | while read git; do
  dir=$(dirname $git)
  name=$(basename $dir)

  if [[ $name != boost.simd ]]; then
    echo "=== $name"

    cd $dir
    git fetch --prune --tags
    git rebase
  fi
done
