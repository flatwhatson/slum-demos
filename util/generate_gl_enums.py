#!/usr/bin/env python3

from argparse import ArgumentParser
from collections import OrderedDict
from pathlib import Path
import sys

parser = ArgumentParser()
parser.add_argument('input_files', nargs='+', type=Path)
parser.add_argument('-o', '--output_file', type=Path)
args = parser.parse_args()

enum_prefix = 'GL_'
enum_names = {}
enum_classes = {}

for input_file in args.input_files:
    enum_class = input_file.stem
    with input_file.open('r') as input_fh:
        for input_line in input_fh:
            if input_line.isspace(): continue
            if input_line.startswith('#'): continue
            enum_value = input_line.strip()
            if not enum_value.startswith(enum_prefix) or \
               not len(enum_value) > len(enum_prefix):
                raise Exception("Invalid enum value: %r" % (enum_value))
            enum_name = enum_value[len(enum_prefix):]
            enum_names.setdefault(enum_name, set()).add(enum_class)
            enum_classes.setdefault(enum_class, OrderedDict()).setdefault(enum_name, enum_value)

output = sys.stdout
if args.output_file:
    output = open(str(args.output_file), 'w')

def write_output(line):
    print(line, file=output)

def split_words(name):
    words = []
    chars = []
    for i in range(0, len(name)):
        if i > 0 and name[i].isupper() and name[i-1].islower():
            words.append(''.join(chars))
            chars = [name[i]]
        else:
            chars.append(name[i])
    if chars:
        words.append(''.join(chars))
    return words

def pluralize_word(word):
    if word[-1] == 'y':
        return word[:-1] + 'ies'
    else:
        return word + 's'

def num_constant(name):
    words = split_words(name)
    words[-1] = pluralize_word(words[-1])
    words[:0] = ('num',)
    return '_'.join(map(lambda s: s.upper(), words))

write_output('#pragma once')
write_output('')
write_output('#include "API.h"')
write_output('')
write_output('#include <stdexcept>')
write_output('')
write_output('namespace Slum { namespace GL {')
write_output('')

for enum_class in sorted(enum_classes.keys()):
    write_output('enum class %s {' % (enum_class))
    write_output('  INVALID = -1,')
    for enum_name, enum_value in enum_classes[enum_class].items():
        write_output('  %s = %s,' % (enum_name, enum_value))
    write_output('};')
    write_output('')
    write_output('constexpr GLuint %s = %d;' % (num_constant(enum_class), len(enum_classes[enum_class])))
    write_output('')
    write_output('constexpr GLenum toGLenum(%s value) {' % (enum_class))
    write_output('  return static_cast<GLenum>(value);')
    write_output('}')
    write_output('')
    write_output('constexpr GLint toGLint(%s value) {' % (enum_class))
    write_output('  return static_cast<GLint>(value);')
    write_output('}')
    write_output('')
    write_output('inline GLuint toIndex(%s value) {' % (enum_class))
    write_output('  switch (value) {')
    for ix, enum_name in enumerate(enum_classes[enum_class].keys()):
        write_output('    case %s::%s: return %d;' % (enum_class, enum_name, ix))
    write_output('    case %s::INVALID:' % (enum_class))
    write_output('    default:')
    write_output('      throw std::runtime_error("Invalid %s");' % (enum_class))
    write_output('  }')
    write_output('}')
    write_output('')
    write_output('inline %s from%sIndex(GLint index) {' % (enum_class, enum_class))
    write_output('  switch (index) {')
    for ix, enum_name in enumerate(enum_classes[enum_class].keys()):
        write_output('    case %d: return %s::%s;' % (ix, enum_class, enum_name))
    write_output('    default: return %s::INVALID;' % (enum_class))
    write_output('  }')
    write_output('}')
    write_output('')

for enum_name in sorted(enum_names.keys()):
    enum_in_classes = enum_names[enum_name]
    if len(enum_in_classes) == 1:
        continue
    write_output('struct SharedEnum_%s {' % (enum_name))
    for enum_class in sorted(enum_in_classes):
        write_output('  constexpr operator %s() const { return %s::%s; }' % (enum_class, enum_class, enum_name))
    write_output('};')
    write_output('')

for enum_class in sorted(enum_classes.keys()):
    for enum_name in enum_classes[enum_class].keys():
        if len(enum_names[enum_name]) == 1:
            write_output('constexpr %s %s = %s::%s;' % (enum_class, enum_name, enum_class, enum_name))
        else:
            write_output('//constexpr %s %s = %s::%s;' % (enum_class, enum_name, enum_class, enum_name))
    write_output('')

for enum_name in sorted(enum_names.keys()):
    if len(enum_names[enum_name]) > 1:
        write_output('constexpr SharedEnum_%s %s = {};' % (enum_name, enum_name))

write_output('')
write_output('}} // namespace Slum::GL')

if args.output_file:
    output.close()
