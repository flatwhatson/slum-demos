
#include <Slum/Noise/PerlinNoise.h>
#include <Slum/Noise/ValueNoise.h>
#include <Slum/Noise/WhiteNoise.h>

#include <FastNoise.h>
#include <FastNoiseSIMD.h>
#include <SDL.h>

#include <boost/simd/memory/allocator.hpp>

#include <cstdio>
#include <vector>

enum class Axis { X, Y, Z };

enum class SlumNoiseType { Perlin, Value, WhiteNoise };

using Slum::Noise::IntVec3;

using SlumNoiseData = std::vector<float, boost::simd::allocator<float>>;

class FastNoiseData {
 public:
  FastNoiseData(float* data) : data_(data) {}
  ~FastNoiseData() { FastNoiseSIMD::FreeNoiseSet(data_); }
  const float* data() const { return data_; }
 private:
  float* data_;
};

/** Return a vector describing a 2D noise slice across the specified axis */
IntVec3 GetDimensions(int width, int height, Axis axis) {
  switch (axis) {
    case Axis::Z: return { width, height, 1 };
    case Axis::Y: return { width, 1, height };
    case Axis::X: return { 1, width, height };
  }
  return {};
}

/** Calculate a noise set of specified type and dimensions (FastNoiseSIMD) */
FastNoiseData GetNoise(FastNoiseSIMD::NoiseType type, IntVec3 dims) {
  FastNoiseSIMD* noise = FastNoiseSIMD::NewFastNoiseSIMD();
  noise->SetNoiseType(type);
  noise->SetSeed(1337);
  noise->SetFrequency(0.05f);
  noise->SetCellularDistanceFunction(FastNoiseSIMD::Euclidean);
  noise->SetCellularReturnType(FastNoiseSIMD::CellValue);

  float* noiseSet = noise->GetNoiseSet(0, 0, 0, dims.x, dims.y, dims.z);

  delete noise;
  return noiseSet;
}

/** Calculate a noise set of specified type and dimensions (FastNoise) */
FastNoiseData GetNoise(FastNoise::NoiseType type, IntVec3 dims) {
  FastNoise noise;
  noise.SetNoiseType(type);
  noise.SetSeed(1337);
  noise.SetFrequency(0.05f);
  noise.SetCellularDistanceFunction(FastNoise::Euclidean);
  noise.SetCellularReturnType(FastNoise::CellValue);
  noise.SetInterp(FastNoise::Quintic);

  float* noiseSet = FastNoiseSIMD::GetEmptySet(dims.x, dims.y, dims.z);

  int i = 0;
  for (int x = 0; x < dims.x; ++x)
    for (int y = 0; y < dims.y; ++y)
      for (int z = 0; z < dims.z; ++z)
        noiseSet[i++] = noise.GetNoise(x, y, z);

  return noiseSet;
}

/** Calculate a noise set of specified type and dimensions (Slum::Noise) */
SlumNoiseData GetNoise(SlumNoiseType type, IntVec3 dims) {
  SlumNoiseData noiseSet;
  noiseSet.resize(dims.x * dims.y * dims.z);

  switch (type) {
    case SlumNoiseType::Value: {
      Slum::Noise::ValueNoise noise;
      noise.setSeed(1337);
      noise.setFrequency(0.05f);
      noise.fill(noiseSet, {}, dims);
      break;
    }
    case SlumNoiseType::Perlin: {
      Slum::Noise::PerlinNoise noise;
      noise.setSeed(1337);
      noise.setFrequency(0.05f);
      noise.fill(noiseSet, {}, dims);
      break;
    }
    case SlumNoiseType::WhiteNoise: {
      Slum::Noise::WhiteNoise noise;
      noise.setSeed(1337);
      noise.setFrequency(0.05f);
      noise.fill(noiseSet, {}, dims);
      break;
    }
  }

  return noiseSet;
}

/** Draw a noise set as greyscale points at the specified coordinates */
template<class Container>
void DrawNoise(SDL_Renderer* renderer, int left, int top, int width, int height, const Container& noiseSet) {
  const auto data = noiseSet.data();
  for (int i = 0, x = left; x < left + width; ++x) {
    for (int y = top; y < top + height; ++y) {
      float value = data[i++];
      int color = ((value + 1.0f) * 0.5f) * 255;

      SDL_SetRenderDrawColor(renderer, color, color, color, 255);
      SDL_RenderDrawPoint(renderer, x, y);
    }
  }
}

int ErrorExit(const char* msg1, const char* msg2) {
  fprintf(stderr, "%s: %s\n", msg1, msg2);
  return 1;
}

int main(int, char**) {
  ////////////////////////////////////////
  // Prepare window & renderer

  if (SDL_Init(SDL_INIT_EVERYTHING) != 0)
    return ErrorExit("SDL_Init", SDL_GetError());

  const int windowWidth = 1024;
  const int windowHeight = 768;

  SDL_Window* window = SDL_CreateWindow(
    "noise", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, windowWidth, windowHeight, 0);
  if (!window)
    return ErrorExit("SDL_CreateWindow", SDL_GetError());

  SDL_Renderer* renderer = SDL_CreateRenderer(window, -1, 0);
  if (!renderer)
    return ErrorExit("SDL_CreateRenderer", SDL_GetError());

  SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255);
  SDL_RenderClear(renderer);
  SDL_RenderPresent(renderer);

  ////////////////////////////////////////
  // Calculate grid coordinates

  const int numCols = 2; const int numRows = 3;
  const int minGap = 2; const int align = 8;

  const int boxW = ((windowWidth - minGap*(numCols+1)) / float(numCols*align)) * align;
  const int boxH = ((windowHeight - minGap*(numRows+1)) / float(numRows*align)) * align;
  const int gapW = (windowWidth - boxW*numCols) / (numCols+1);
  const int gapH = (windowHeight - boxH*numRows) / (numRows+1);

  int boxX[numCols];
  for (int i = 0; i < numCols; ++i)
    boxX[i] = gapW*(i+1) + boxW*i;

  int boxY[numRows];
  for (int i = 0; i < numRows; ++i)
    boxY[i] = gapH*(i+1) + boxH*i;

  ////////////////////////////////////////
  // Draw noise textures

  IntVec3 acrossZ = GetDimensions(boxW, boxH, Axis::Z);
  //IntVec3 acrossY = GetDimensions(boxW, boxH, Axis::Y);
  //IntVec3 acrossX = GetDimensions(boxW, boxH, Axis::X);

  int row = 0;

  DrawNoise(renderer, boxX[0], boxY[row], boxW, boxH, GetNoise(FastNoiseSIMD::Value, acrossZ));
  DrawNoise(renderer, boxX[1], boxY[row], boxW, boxH, GetNoise(SlumNoiseType::Value, acrossZ));
  ++row;

  DrawNoise(renderer, boxX[0], boxY[row], boxW, boxH, GetNoise(FastNoiseSIMD::Perlin, acrossZ));
  DrawNoise(renderer, boxX[1], boxY[row], boxW, boxH, GetNoise(SlumNoiseType::Perlin, acrossZ));
  ++row;

  DrawNoise(renderer, boxX[0], boxY[row], boxW, boxH, GetNoise(FastNoiseSIMD::WhiteNoise, acrossZ));
  DrawNoise(renderer, boxX[1], boxY[row], boxW, boxH, GetNoise(SlumNoiseType::WhiteNoise, acrossZ));
  ++row;

  ////////////////////////////////////////
  // Run until window is closed

  SDL_RenderPresent(renderer);

  while (true) {
    SDL_Event event;
    if (SDL_PollEvent(&event)) {
      if (event.type == SDL_QUIT)
        break;

      if (event.type == SDL_WINDOWEVENT) {
        if (event.window.event == SDL_WINDOWEVENT_EXPOSED)
          SDL_RenderPresent(renderer);
        continue;
      }
    }
  }

  SDL_DestroyRenderer(renderer);
  SDL_DestroyWindow(window);
  SDL_Quit();
  return 0;
}
