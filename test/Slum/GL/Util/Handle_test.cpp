
#include <Slum/GL/Util/Handle.h>

#include <catch.hpp>

#include <type_traits>

using namespace Slum;

TEST_CASE("GL::Handle<int>") {
  static_assert(std::is_default_constructible<GL::Handle<int>>(), "");
  static_assert(!std::is_copy_constructible<GL::Handle<int>>(), "");
  static_assert(!std::is_copy_assignable<GL::Handle<int>>(), "");
  static_assert(std::is_nothrow_move_constructible<GL::Handle<int>>(), "");
  static_assert(std::is_nothrow_move_assignable<GL::Handle<int>>(), "");

  SECTION("move construction clears source") {
    GL::Handle<int> a = 1;
    GL::Handle<int> b(std::move(a));
    REQUIRE(a == 0);
    REQUIRE(b == 1);
  }

  SECTION("move assignment swaps values") {
    GL::Handle<int> a = 1;
    GL::Handle<int> b = 2;
    b = std::move(a);
    REQUIRE(a == 2);
    REQUIRE(b == 1);
  }
}
