
#include <Slum/GL/Util/ArrayView.h>

#include <catch.hpp>

#include <array>
#include <vector>
#include <type_traits>

using namespace Slum;

TEST_CASE("GL::ArrayView<float>") {
  static_assert(!std::is_default_constructible<GL::ArrayView<float>>(), "");
  static_assert(std::is_copy_constructible<GL::ArrayView<float>>(), "");
  static_assert(std::is_copy_assignable<GL::ArrayView<float>>(), "");
  static_assert(std::is_nothrow_move_constructible<GL::ArrayView<float>>(), "");
  static_assert(std::is_nothrow_move_assignable<GL::ArrayView<float>>(), "");

  SECTION("construct from pointer and size") {
    const float values[5] = { 1, 2, 3, 4, 5 };
    const GL::ArrayView<float> array(values, 5);
    REQUIRE(array.data() == values);
    REQUIRE(array.size() == 5);
  }
  SECTION("construct from null pointer") {
    const GL::ArrayView<float> array(nullptr);
    REQUIRE(array.data() == nullptr);
    REQUIRE(array.size() == 0);
  }
  SECTION("construct from single value") {
    const float value = 5;
    const GL::ArrayView<float> array(value);
    REQUIRE(array.data() == &value);
    REQUIRE(array.size() == 1);
  }
  SECTION("construct from c-style array") {
    const float values[5] = { 1, 2, 3, 4, 5 };
    const GL::ArrayView<float> array(values);
    REQUIRE(array.data() == values);
    REQUIRE(array.size() == 5);
  }
  SECTION("construct from std::array") {
    const std::array<float, 5> values = {{ 1, 2, 3, 4, 5 }};
    const GL::ArrayView<float> array(values);
    REQUIRE(array.data() == values.data());
    REQUIRE(array.size() == values.size());
  }
  SECTION("construct from std::vector") {
    const std::vector<float> values = { 1, 2, 3, 4, 5 };
    const GL::ArrayView<float> array(values);
    REQUIRE(array.data() == values.data());
    REQUIRE(array.size() == values.size());
  }
}

TEST_CASE("GL::ArrayView<void>") {
  static_assert(!std::is_default_constructible<GL::ArrayView<void>>(), "");
  static_assert(std::is_copy_constructible<GL::ArrayView<void>>(), "");
  static_assert(std::is_copy_assignable<GL::ArrayView<void>>(), "");
  static_assert(std::is_nothrow_move_constructible<GL::ArrayView<void>>(), "");
  static_assert(std::is_nothrow_move_assignable<GL::ArrayView<void>>(), "");

  SECTION("construct from pointer and size") {
    const float values[5] = { 1, 2, 3, 4, 5 };
    const GL::ArrayView<void> array(values, 5 * sizeof(float));
    REQUIRE(array.data() == values);
    REQUIRE(array.size() == sizeof(values));
  }
  SECTION("construct from null pointer") {
    const GL::ArrayView<void> array(nullptr);
    REQUIRE(array.data() == nullptr);
    REQUIRE(array.size() == 0);
  }
  SECTION("construct from single value") {
    const float value = 5;
    const GL::ArrayView<void> array(value);
    REQUIRE(array.data() == &value);
    REQUIRE(array.size() == sizeof(float));
  }
  SECTION("construct from c-style array") {
    const float values[5] = { 1, 2, 3, 4, 5 };
    const GL::ArrayView<void> array(values);
    REQUIRE(array.data() == values);
    REQUIRE(array.size() == 5 * sizeof(float));
  }
  SECTION("construct from std::array") {
    const std::array<float, 5> values = {{ 1, 2, 3, 4, 5 }};
    const GL::ArrayView<void> array(values);
    REQUIRE(array.data() == values.data());
    REQUIRE(array.size() == values.size() * sizeof(float));
  }
  SECTION("construct from std::vector") {
    const std::vector<float> values = { 1, 2, 3, 4, 5 };
    const GL::ArrayView<void> array(values);
    REQUIRE(array.data() == values.data());
    REQUIRE(array.size() == values.size() * sizeof(float));
  }
  SECTION("construct from GL::ArrayView<float>") {
    const float values[5] = { 1, 2, 3, 4, 5 };
    const GL::ArrayView<float> source(values);
    const GL::ArrayView<void> array(source);
    REQUIRE(array.data() == values);
    REQUIRE(array.size() == sizeof(values));
  }
}
