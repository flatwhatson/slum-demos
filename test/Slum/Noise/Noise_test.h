#pragma once

#include <Slum/Noise/Factory.h>

#include <FastNoise.h>
#include <FastNoiseSIMD.h>

#include <boost/simd/memory/allocator.hpp>

#include <cstddef>
#include <memory>
#include <stdexcept>
#include <string>
#include <vector>

namespace {

class UnsupportedNoiseType : public std::runtime_error {
  using std::runtime_error::runtime_error;
};

struct Config {
  std::string name;
  int seed = 1337;
  float frequency = 0.01f;
  int octaves = 3;
  float lacunarity = 2.0f;
  float gain = 0.5f;
  Slum::Noise::IntVec3 dims;
  Slum::Noise::IntVec3 offs;
};

struct FastNoiseSetDeleter {
  void operator()(float* data) {
    FastNoiseSIMD::FreeNoiseSet(data);
  }
};

class FastNoiseSet {
 public:
  FastNoiseSet(Slum::Noise::IntVec3 dims)
      : size_(dims.x * dims.y * dims.z),
        data_(FastNoiseSIMD::GetEmptySet(dims.x, dims.y, dims.z)) {}

  size_t size() const {
    return size_;
  }

  float* data() {
    return data_.get();
  }

  float& operator[](int i) {
    return data_.get()[i];
  }

  float operator[](int i) const {
    return data_.get()[i];
  }

 private:
  size_t size_;
  std::unique_ptr<float, FastNoiseSetDeleter> data_;
};

using SlumNoiseSet = std::vector<float, boost::simd::allocator<float>>;

////////////////////////////////////////

FastNoise::NoiseType GetFastNoiseNoiseType(FastNoiseSIMD::NoiseType type) {
  switch (type) {
    case FastNoiseSIMD::Value:          return FastNoise::Value;
    case FastNoiseSIMD::Cubic:          return FastNoise::Cubic;
    case FastNoiseSIMD::Perlin:         return FastNoise::Perlin;
    case FastNoiseSIMD::Simplex:        return FastNoise::Simplex;
    case FastNoiseSIMD::Cellular:       return FastNoise::Cellular;
    case FastNoiseSIMD::WhiteNoise:     return FastNoise::WhiteNoise;
    case FastNoiseSIMD::ValueFractal:   return FastNoise::ValueFractal;
    case FastNoiseSIMD::CubicFractal:   return FastNoise::CubicFractal;
    case FastNoiseSIMD::PerlinFractal:  return FastNoise::PerlinFractal;
    case FastNoiseSIMD::SimplexFractal: return FastNoise::SimplexFractal;
  }
  throw UnsupportedNoiseType("unsupported noise type");
}

FastNoise::FractalType GetFastNoiseFractalType(FastNoiseSIMD::FractalType type) {
  switch (type) {
    case FastNoiseSIMD::FBM:        return FastNoise::FBM;
    case FastNoiseSIMD::Billow:     return FastNoise::Billow;
    case FastNoiseSIMD::RigidMulti: return FastNoise::RigidMulti;
  }
  throw UnsupportedNoiseType("unsupported fractal type");
}

std::unique_ptr<FastNoise> MakeFastNoiseEngine(const Config& conf, FastNoise::NoiseType nType, FastNoise::FractalType fType = FastNoise::FBM) {
  auto fns = std::make_unique<FastNoise>();
  fns->SetNoiseType(nType);
  fns->SetSeed(conf.seed);
  fns->SetFrequency(conf.frequency);
  fns->SetFractalType(fType);
  fns->SetFractalOctaves(conf.octaves);
  fns->SetFractalLacunarity(conf.lacunarity);
  fns->SetFractalGain(conf.gain);
  fns->SetInterp(FastNoise::Quintic);
  return fns;
}

std::unique_ptr<FastNoise> MakeFastNoiseEngine(const Config& conf, FastNoiseSIMD::NoiseType nType, FastNoiseSIMD::FractalType fType = FastNoiseSIMD::FBM) {
  return MakeFastNoiseEngine(conf, GetFastNoiseNoiseType(nType), GetFastNoiseFractalType(fType));
}

FastNoiseSet MakeFastNoiseSet(const Config& conf) {
  return FastNoiseSet(conf.dims);
}

template<class Engine, class Set>
void FillFastNoiseSet(const Config& conf, Engine& eng, Set& set) {
  size_t i = 0;
  for (int z = 0; z < conf.dims.z; ++z)
    for (int y = 0; y < conf.dims.y; ++y)
      for (int x = 0; x < conf.dims.x; ++x)
        set[i++] = eng->GetNoise(x + conf.offs.x, y + conf.offs.y, z + conf.offs.z);
}

FastNoiseSet GetFastNoiseSet(const Config& conf, FastNoise::NoiseType nType, FastNoise::FractalType fType = FastNoise::FBM) {
  auto eng = MakeFastNoiseEngine(conf, nType, fType);
  auto set = MakeFastNoiseSet(conf);
  FillFastNoiseSet(conf, eng, set);
  return set;
}

FastNoiseSet GetFastNoiseSet(const Config& conf, FastNoiseSIMD::NoiseType nType, FastNoiseSIMD::FractalType fType = FastNoiseSIMD::FBM) {
  return GetFastNoiseSet(conf, GetFastNoiseNoiseType(nType), GetFastNoiseFractalType(fType));
}

////////////////////////////////////////

std::unique_ptr<FastNoiseSIMD> MakeFastNoiseSIMDEngine(const Config& conf, FastNoiseSIMD::NoiseType nType, FastNoiseSIMD::FractalType fType = FastNoiseSIMD::FBM) {
  std::unique_ptr<FastNoiseSIMD> fns(FastNoiseSIMD::NewFastNoiseSIMD());
  fns->SetNoiseType(nType);
  fns->SetSeed(conf.seed);
  fns->SetFrequency(conf.frequency);
  fns->SetFractalType(fType);
  fns->SetFractalOctaves(conf.octaves);
  fns->SetFractalLacunarity(conf.lacunarity);
  fns->SetFractalGain(conf.gain);
  return fns;
}

FastNoiseSet MakeFastNoiseSIMDSet(const Config& conf) {
  return FastNoiseSet(conf.dims);
}

template<class Engine, class Set>
void FillFastNoiseSIMDSet(const Config& conf, Engine& eng, Set& set) {
  eng->FillNoiseSet(set.data(), conf.offs.x, conf.offs.y, conf.offs.z, conf.dims.x, conf.dims.y, conf.dims.z);
}

FastNoiseSet GetFastNoiseSIMDSet(const Config& conf, FastNoiseSIMD::NoiseType nType, FastNoiseSIMD::FractalType fType = FastNoiseSIMD::FBM) {
  auto eng = MakeFastNoiseSIMDEngine(conf, nType, fType);
  auto set = MakeFastNoiseSIMDSet(conf);
  FillFastNoiseSIMDSet(conf, eng, set);
  return set;
}

////////////////////////////////////////

std::unique_ptr<Slum::Noise::Engine> MakeSlumNoiseEngine(const Config& conf, FastNoiseSIMD::NoiseType nType, FastNoiseSIMD::FractalType fType = FastNoiseSIMD::FBM) {
  Slum::Noise::NoiseType noiseType;
  bool fractal = false;

  switch (nType) {
    case FastNoiseSIMD::WhiteNoise:     noiseType = Slum::Noise::NoiseType::WhiteNoise; break;
    case FastNoiseSIMD::ValueFractal:   fractal = true; [[fallthrough]];
    case FastNoiseSIMD::Value:          noiseType = Slum::Noise::NoiseType::ValueNoise; break;
    case FastNoiseSIMD::CubicFractal:   fractal = true; [[fallthrough]];
    case FastNoiseSIMD::Cubic:          noiseType = Slum::Noise::NoiseType::CubicNoise; break;
    case FastNoiseSIMD::PerlinFractal:  fractal = true; [[fallthrough]];
    case FastNoiseSIMD::Perlin:         noiseType = Slum::Noise::NoiseType::PerlinNoise; break;
    case FastNoiseSIMD::SimplexFractal: fractal = true; [[fallthrough]];
    case FastNoiseSIMD::Simplex:        noiseType = Slum::Noise::NoiseType::SimplexNoise; break;
    default:
      throw UnsupportedNoiseType("unsupported noise type");
  }

  if (!fractal) {
    auto noise = Slum::Noise::MakeNoiseEngine(noiseType);
    noise->setSeed(conf.seed);
    noise->setFrequency(conf.frequency);
    return noise;
  }

  Slum::Noise::FractalType fractalType;

  switch (fType) {
    case FastNoiseSIMD::FBM:        fractalType = Slum::Noise::FractalType::FbmFractal; break;
    case FastNoiseSIMD::Billow:     fractalType = Slum::Noise::FractalType::BillowFractal; break;
    case FastNoiseSIMD::RigidMulti: fractalType = Slum::Noise::FractalType::RigidMultiFractal; break;
    default:
      throw UnsupportedNoiseType("unsupported fractal type");
  }

  auto noise = Slum::Noise::MakeFractalEngine(noiseType, fractalType);
  noise->setSeed(conf.seed);
  noise->setFrequency(conf.frequency);
  noise->setOctaves(conf.octaves);
  noise->setLacunarity(conf.lacunarity);
  noise->setGain(conf.gain);
  return noise;
}

SlumNoiseSet MakeSlumNoiseSet(const Config& conf) {
  SlumNoiseSet set;
  set.resize(conf.dims.x * conf.dims.y * conf.dims.z);
  return set;
}

template<class Engine, class Set>
void FillSlumNoiseSet(const Config& conf, Engine& eng, Set& set) {
  eng->fill(set, conf.offs, conf.dims);
}

SlumNoiseSet GetSlumNoiseSet(const Config& conf, FastNoiseSIMD::NoiseType nType, FastNoiseSIMD::FractalType fType = FastNoiseSIMD::FBM) {
  auto eng = MakeSlumNoiseEngine(conf, nType, fType);
  auto set = MakeSlumNoiseSet(conf);
  FillSlumNoiseSet(conf, eng, set);
  return set;
}

} // anonymous namespace
