#include "Noise_test.h"

#include <catch.hpp>

namespace {

using namespace Slum;

enum ConfigName {
  CUBE_AROUND_ORIGIN,
};

Config GetConfig(ConfigName name) {
  Config conf;
  switch (name) {
    case CUBE_AROUND_ORIGIN:
      conf.dims = {  64,  64,  64 };
      conf.offs = { -32, -32, -32 };
      break;
  }
  return conf;
}

void REQUIRE_NOISE_SET_MATCH(ConfigName name, FastNoiseSIMD::NoiseType nType, FastNoiseSIMD::FractalType fType = FastNoiseSIMD::FBM) {
  auto config = GetConfig(name);
  auto fastdata = GetFastNoiseSIMDSet(config, nType, fType);
  auto slumdata = GetSlumNoiseSet(config, nType, fType);
  REQUIRE(fastdata.size() == slumdata.size());

  size_t i, size = fastdata.size();
  for (i = 0; i < size; ++i) {
    if (fastdata[i] != Approx(slumdata[i]).epsilon(0.01))
      break;
  }

  if (i != size) {
    CAPTURE(size);
    CAPTURE(i);
    REQUIRE(fastdata[i] == Approx(slumdata[i]).epsilon(0.01));
  }

  REQUIRE(true);
}

} // anonymous namespace

TEST_CASE("Noise::ValueNoise") {
  SECTION("single output 2D") {
    REQUIRE(0.0f != Noise::ValueNoise().at(0.1f, 0.2f));
  }
  SECTION("single output 3D") {
    REQUIRE(0.0f != Noise::ValueNoise().at(0.1f, 0.2f, 0.3f));
  }
  SECTION("fill output matches FastNoiseSIMD") {
    REQUIRE_NOISE_SET_MATCH(CUBE_AROUND_ORIGIN, FastNoiseSIMD::Value);
  }
}

TEST_CASE("Noise::CubicNoise") {
  SECTION("single output 2D") {
    REQUIRE(0.0f != Noise::CubicNoise().at(0.1f, 0.2f));
  }
  SECTION("single output 3D") {
    REQUIRE(0.0f != Noise::CubicNoise().at(0.1f, 0.2f, 0.3f));
  }
  SECTION("fill output matches FastNoiseSIMD") {
    REQUIRE_NOISE_SET_MATCH(CUBE_AROUND_ORIGIN, FastNoiseSIMD::Cubic);
  }
}

TEST_CASE("Noise::PerlinNoise") {
  SECTION("single output 2D") {
    REQUIRE(0.0f != Noise::PerlinNoise().at(0.1f, 0.2f));
  }
  SECTION("single output 3D") {
    REQUIRE(0.0f != Noise::PerlinNoise().at(0.1f, 0.2f, 0.3f));
  }
  SECTION("fill output matches FastNoiseSIMD") {
    REQUIRE_NOISE_SET_MATCH(CUBE_AROUND_ORIGIN, FastNoiseSIMD::Perlin);
  }
}

TEST_CASE("Noise::SimplexNoise") {
  SECTION("single output 2D") {
    REQUIRE(0.0f != Noise::SimplexNoise().at(0.1f, 0.2f));
  }
  SECTION("single output 3D") {
    REQUIRE(0.0f != Noise::SimplexNoise().at(0.1f, 0.2f, 0.3f));
  }
  SECTION("fill output matches FastNoiseSIMD") {
    REQUIRE_NOISE_SET_MATCH(CUBE_AROUND_ORIGIN, FastNoiseSIMD::Simplex);
  }
}

TEST_CASE("Noise::WhiteNoise") {
  SECTION("single output 2D") {
    REQUIRE(0.0f != Noise::WhiteNoise().at(0.1f, 0.2f));
  }
  SECTION("single output 3D") {
    REQUIRE(0.0f != Noise::WhiteNoise().at(0.1f, 0.2f, 0.3f));
  }
  SECTION("fill output matches FastNoiseSIMD") {
    REQUIRE_NOISE_SET_MATCH(CUBE_AROUND_ORIGIN, FastNoiseSIMD::WhiteNoise);
  }
}

TEST_CASE("Noise::FbmFractal<ValueNoise>") {
  SECTION("single output 2D") {
    REQUIRE(0.0f != Noise::FbmFractal<Noise::ValueNoise>().at(0.1f, 0.2f));
  }
  SECTION("single output 3D") {
    REQUIRE(0.0f != Noise::FbmFractal<Noise::ValueNoise>().at(0.1f, 0.2f, 0.3f));
  }
  SECTION("fill output matches FastNoiseSIMD") {
    REQUIRE_NOISE_SET_MATCH(CUBE_AROUND_ORIGIN, FastNoiseSIMD::ValueFractal, FastNoiseSIMD::FBM);
  }
}

TEST_CASE("Noise::FbmFractal<CubicNoise>") {
  SECTION("single output 2D") {
    REQUIRE(0.0f != Noise::FbmFractal<Noise::CubicNoise>().at(0.1f, 0.2f));
  }
  SECTION("single output 3D") {
    REQUIRE(0.0f != Noise::FbmFractal<Noise::CubicNoise>().at(0.1f, 0.2f, 0.3f));
  }
  SECTION("fill output matches FastNoiseSIMD") {
    REQUIRE_NOISE_SET_MATCH(CUBE_AROUND_ORIGIN, FastNoiseSIMD::CubicFractal, FastNoiseSIMD::FBM);
  }
}

TEST_CASE("Noise::FbmFractal<PerlinNoise>") {
  SECTION("single output 2D") {
    REQUIRE(0.0f != Noise::FbmFractal<Noise::PerlinNoise>().at(0.1f, 0.2f));
  }
  SECTION("single output 3D") {
    REQUIRE(0.0f != Noise::FbmFractal<Noise::PerlinNoise>().at(0.1f, 0.2f, 0.3f));
  }
  SECTION("fill output matches FastNoiseSIMD") {
    REQUIRE_NOISE_SET_MATCH(CUBE_AROUND_ORIGIN, FastNoiseSIMD::PerlinFractal, FastNoiseSIMD::FBM);
  }
}

TEST_CASE("Noise::FbmFractal<SimplexNoise>") {
  SECTION("single output 2D") {
    REQUIRE(0.0f != Noise::FbmFractal<Noise::SimplexNoise>().at(0.1f, 0.2f));
  }
  SECTION("single output 3D") {
    REQUIRE(0.0f != Noise::FbmFractal<Noise::SimplexNoise>().at(0.1f, 0.2f, 0.3f));
  }
  SECTION("fill output matches FastNoiseSIMD") {
    REQUIRE_NOISE_SET_MATCH(CUBE_AROUND_ORIGIN, FastNoiseSIMD::SimplexFractal, FastNoiseSIMD::FBM);
  }
}

TEST_CASE("Noise::BillowFractal<ValueNoise>") {
  SECTION("single output 2D") {
    REQUIRE(0.0f != Noise::BillowFractal<Noise::ValueNoise>().at(0.1f, 0.2f));
  }
  SECTION("single output 3D") {
    REQUIRE(0.0f != Noise::BillowFractal<Noise::ValueNoise>().at(0.1f, 0.2f, 0.3f));
  }
  SECTION("fill output matches FastNoiseSIMD") {
    REQUIRE_NOISE_SET_MATCH(CUBE_AROUND_ORIGIN, FastNoiseSIMD::ValueFractal, FastNoiseSIMD::Billow);
  }
}

TEST_CASE("Noise::BillowFractal<CubicNoise>") {
  SECTION("single output 2D") {
    REQUIRE(0.0f != Noise::BillowFractal<Noise::CubicNoise>().at(0.1f, 0.2f));
  }
  SECTION("single output 3D") {
    REQUIRE(0.0f != Noise::BillowFractal<Noise::CubicNoise>().at(0.1f, 0.2f, 0.3f));
  }
  SECTION("fill output matches FastNoiseSIMD") {
    REQUIRE_NOISE_SET_MATCH(CUBE_AROUND_ORIGIN, FastNoiseSIMD::CubicFractal, FastNoiseSIMD::Billow);
  }
}

TEST_CASE("Noise::BillowFractal<PerlinNoise>") {
  SECTION("single output 2D") {
    REQUIRE(0.0f != Noise::BillowFractal<Noise::PerlinNoise>().at(0.1f, 0.2f));
  }
  SECTION("single output 3D") {
    REQUIRE(0.0f != Noise::BillowFractal<Noise::PerlinNoise>().at(0.1f, 0.2f, 0.3f));
  }
  SECTION("fill output matches FastNoiseSIMD") {
    REQUIRE_NOISE_SET_MATCH(CUBE_AROUND_ORIGIN, FastNoiseSIMD::PerlinFractal, FastNoiseSIMD::Billow);
  }
}

TEST_CASE("Noise::BillowFractal<SimplexNoise>") {
  SECTION("single output 2D") {
    REQUIRE(0.0f != Noise::BillowFractal<Noise::SimplexNoise>().at(0.1f, 0.2f));
  }
  SECTION("single output 3D") {
    REQUIRE(0.0f != Noise::BillowFractal<Noise::SimplexNoise>().at(0.1f, 0.2f, 0.3f));
  }
  SECTION("fill output matches FastNoiseSIMD") {
    REQUIRE_NOISE_SET_MATCH(CUBE_AROUND_ORIGIN, FastNoiseSIMD::SimplexFractal, FastNoiseSIMD::Billow);
  }
}

TEST_CASE("Noise::RigidMultiFractal<ValueNoise>") {
  SECTION("single output 2D") {
    REQUIRE(0.0f != Noise::RigidMultiFractal<Noise::ValueNoise>().at(0.1f, 0.2f));
  }
  SECTION("single output 3D") {
    REQUIRE(0.0f != Noise::RigidMultiFractal<Noise::ValueNoise>().at(0.1f, 0.2f, 0.3f));
  }
  SECTION("fill output matches FastNoiseSIMD") {
    REQUIRE_NOISE_SET_MATCH(CUBE_AROUND_ORIGIN, FastNoiseSIMD::ValueFractal, FastNoiseSIMD::RigidMulti);
  }
}

TEST_CASE("Noise::RigidMultiFractal<CubicNoise>") {
  SECTION("single output 2D") {
    REQUIRE(0.0f != Noise::RigidMultiFractal<Noise::CubicNoise>().at(0.1f, 0.2f));
  }
  SECTION("single output 3D") {
    REQUIRE(0.0f != Noise::RigidMultiFractal<Noise::CubicNoise>().at(0.1f, 0.2f, 0.3f));
  }
  SECTION("fill output matches FastNoiseSIMD") {
    REQUIRE_NOISE_SET_MATCH(CUBE_AROUND_ORIGIN, FastNoiseSIMD::CubicFractal, FastNoiseSIMD::RigidMulti);
  }
}

TEST_CASE("Noise::RigidMultiFractal<PerlinNoise>") {
  SECTION("single output 2D") {
    REQUIRE(0.0f != Noise::RigidMultiFractal<Noise::PerlinNoise>().at(0.1f, 0.2f));
  }
  SECTION("single output 3D") {
    REQUIRE(0.0f != Noise::RigidMultiFractal<Noise::PerlinNoise>().at(0.1f, 0.2f, 0.3f));
  }
  SECTION("fill output matches FastNoiseSIMD") {
    REQUIRE_NOISE_SET_MATCH(CUBE_AROUND_ORIGIN, FastNoiseSIMD::PerlinFractal, FastNoiseSIMD::RigidMulti);
  }
}

TEST_CASE("Noise::RigidMultiFractal<SimplexNoise>") {
  SECTION("single output 2D") {
    REQUIRE(0.0f != Noise::RigidMultiFractal<Noise::SimplexNoise>().at(0.1f, 0.2f));
  }
  SECTION("single output 3D") {
    REQUIRE(0.0f != Noise::RigidMultiFractal<Noise::SimplexNoise>().at(0.1f, 0.2f, 0.3f));
  }
  SECTION("fill output matches FastNoiseSIMD") {
    REQUIRE_NOISE_SET_MATCH(CUBE_AROUND_ORIGIN, FastNoiseSIMD::SimplexFractal, FastNoiseSIMD::RigidMulti);
  }
}
