
#include <Slum/Noise/Noise_test.h>
#include <Slum/Util/Timer.h>

#include <cstddef>
#include <cstdio>
#include <vector>

namespace {

struct Stats {
  size_t count;
  double timeMs;
};

template<class Set, class Timer>
Stats MakeStats(const Set& set, const Timer& timer) {
  Stats s;
  s.count = set.size();
  s.timeMs = timer.milliseconds();
  return s;
}

template<class Func>
void ShowStats(const char* name, Func func) {
  printf(" %-13s ...", name);
  auto stats = func();
  printf(" %10.6fms (%7d/ms)\n", stats.timeMs, int(stats.count/stats.timeMs));
}

Stats BenchFastNoise(const Config& conf, FastNoiseSIMD::NoiseType nType, FastNoiseSIMD::FractalType fType = FastNoiseSIMD::FBM) {
  Slum::Util::Timer timer;
  auto eng = MakeFastNoiseEngine(conf, nType, fType);
  auto set = MakeFastNoiseSet(conf);
  timer.benchmark(10, [&]{ FillFastNoiseSet(conf, eng, set); });
  return MakeStats(set, timer);
}

Stats BenchFastNoiseSIMD(const Config& conf, FastNoiseSIMD::NoiseType nType, FastNoiseSIMD::FractalType fType = FastNoiseSIMD::FBM) {
  Slum::Util::Timer timer;
  auto eng = MakeFastNoiseSIMDEngine(conf, nType, fType);
  auto set = MakeFastNoiseSIMDSet(conf);
  timer.benchmark(10, [&]{ FillFastNoiseSIMDSet(conf, eng, set); });
  return MakeStats(set, timer);
}

Stats BenchSlumNoise(const Config& conf, FastNoiseSIMD::NoiseType nType, FastNoiseSIMD::FractalType fType = FastNoiseSIMD::FBM) {
  Slum::Util::Timer timer;
  auto eng = MakeSlumNoiseEngine(conf, nType, fType);
  auto set = MakeSlumNoiseSet(conf);
  timer.benchmark(10, [&]{ FillSlumNoiseSet(conf, eng, set); });
  return MakeStats(set, timer);
}

} // anonymous namespace

int main(int argc, char** argv) {
  std::vector<Config> confs;

  confs.push_back([]{
    Config conf;
    conf.name = "Cube";
    conf.dims = { 128, 128, 128 };
    conf.offs = { -64, -64, -64 };
    return conf;
  }());

  /*
  confs.push_back([]{
    Config conf;
    conf.name = "Square";
    conf.dims = { 1024, 1024, 1 };
    conf.offs = { -512, -512, 0 };
    return conf;
  }());
  */

  std::vector<FastNoiseSIMD::NoiseType> noiseTypes =
    { FastNoiseSIMD::Value, FastNoiseSIMD::Cubic,
      FastNoiseSIMD::Perlin, FastNoiseSIMD::Simplex,
      FastNoiseSIMD::WhiteNoise };

  std::vector<const char*> noiseTypeNames =
    { "Value Noise", "Cubic Noise",
      "Perlin Noise", "Simplex Noise",
      "White Noise" };

  std::vector<FastNoiseSIMD::NoiseType> fractalNoiseTypes =
    { FastNoiseSIMD::ValueFractal, FastNoiseSIMD::CubicFractal,
      FastNoiseSIMD::PerlinFractal, FastNoiseSIMD::SimplexFractal };

  std::vector<const char*> fractalNoiseTypeNames =
    { "Fractal Value Noise", "Fractal Cubic Noise",
      "Fractal Perlin Noise", "Fractal Simplex Noise" };

  std::vector<FastNoiseSIMD::FractalType> fractalTypes =
    { FastNoiseSIMD::FBM, FastNoiseSIMD::Billow, FastNoiseSIMD::RigidMulti };

  std::vector<const char*> fractalTypeNames =
    { "FBM", "Billow", "RigidMulti" };

  ////////////////////////////////////////

  for (const auto& conf : confs) {
    if (confs.size() > 1) {
      printf("=== %s\n", conf.name.c_str());
    }

    for (size_t i = 0; i < noiseTypes.size(); ++i) {
      printf("==== %s\n", noiseTypeNames.at(i));
      //ShowStats("FastNoise", [&]{ return BenchFastNoise(conf, noiseTypes[i]); });
      ShowStats("FastNoiseSIMD", [&]{ return BenchFastNoiseSIMD(conf, noiseTypes[i]); });
      ShowStats("Slum::Noise", [&]{ return BenchSlumNoise(conf, noiseTypes[i]); });
    }

    for (size_t j = 0; j < fractalTypes.size(); ++j) {
      for (size_t i = 0; i < fractalNoiseTypes.size(); ++i) {
        printf("==== %s (%s)\n", fractalNoiseTypeNames.at(i), fractalTypeNames.at(j));
        //ShowStats("FastNoise", [&]{ return BenchFastNoise(conf, fractalNoiseTypes[i], fractalTypes[j]); });
        ShowStats("FastNoiseSIMD", [&]{ return BenchFastNoiseSIMD(conf, fractalNoiseTypes[i], fractalTypes[j]); });
        ShowStats("Slum::Noise", [&]{ return BenchSlumNoise(conf, fractalNoiseTypes[i], fractalTypes[j]); });
      }
    }
  }

  return 0;
}
