(use-modules (guix packages)
             (guix build-system cmake)
             (guix build utils)
             (guix gexp)
             (guix git)
             (guix git-download)
             ((guix licenses) #:prefix license:)
             (gnu packages boost)
             (gnu packages gl)
	     (gnu packages pkg-config)
             (gnu packages python)
             (gnu packages sdl)
             (ice-9 popen)
             (ice-9 textual-ports)
             (srfi srfi-1))

(define %source-dir
  (dirname (current-filename)))

(define %git-commit
  (with-directory-excursion %source-dir
    (get-line (open-input-pipe "git rev-parse HEAD"))))

(define slum-demos
  (package
    (name "slum-demos")
    (version (git-version "0.1" "HEAD" %git-commit))
    (source (local-file %source-dir
                        #:recursive? #t
                        #:select? (git-predicate %source-dir)))
    (build-system cmake-build-system)
    (arguments '(#:tests? #f))
    (native-inputs (list pkg-config python))
    (inputs (list boost glew sdl2))
    (home-page "https://github.com/flatwhatson/slum-demos")
    (synopsis "An abandoned half-implemented C++ game engine, and its demos")
    (description "An abandoned half-implemented C++ game engine, and its demos")
    (license #f)))

slum-demos
