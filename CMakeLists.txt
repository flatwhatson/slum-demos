cmake_minimum_required(VERSION 3.0)

project(slum)

option(USE_LTO "Use link-time optimization" off)
option(USE_CCACHE "Use compiler cache" off)
option(USE_NATIVE "Compile with march=native" off)

set(SLUM_EXTERNAL_DIR "${CMAKE_SOURCE_DIR}/external")
set(SLUM_SOURCE_DIR "${CMAKE_SOURCE_DIR}/src")
set(SLUM_TEST_DIR "${CMAKE_SOURCE_DIR}/test")
set(SLUM_UTIL_DIR "${CMAKE_SOURCE_DIR}/util")

########################################
# Compiler configuration

set(CMAKE_CXX_STANDARD 17)

if(CMAKE_CXX_COMPILER_ID STREQUAL "GNU")
  set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wall -fdiagnostics-color=always")
  set(CMAKE_CXX_FLAGS_DEBUG "-Og -g")
  set(CMAKE_CXX_FLAGS_RELEASE "-Ofast -DNDEBUG")
  set(CMAKE_CXX_FLAGS_RELWITHDEBINFO "${CMAKE_CXX_FLAGS_RELEASE} -g")
  if(USE_LTO)
    set(CMAKE_CXX_FLAGS_RELEASE "${CMAKE_CXX_FLAGS_RELEASE} -flto")
    find_program(GCC_AR_PATH "gcc-ar")
    set(CMAKE_AR ${GCC_AR_PATH})
    find_program(GCC_RANLIB_PATH "gcc-ranlib")
    set(CMAKE_RANLIB ${GCC_RANLIB_PATH})
  endif()
elseif(CMAKE_CXX_COMPILER_ID STREQUAL "Clang")
  set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Qunused-arguments -Wall -fcolor-diagnostics")
  set(CMAKE_CXX_FLAGS_DEBUG "-O0 -g")
  set(CMAKE_CXX_FLAGS_RELEASE "-Ofast -DNDEBUG")
  set(CMAKE_CXX_FLAGS_RELWITHDEBINFO "${CMAKE_CXX_FLAGS_RELEASE} -g")
endif()

if(USE_CCACHE)
  find_program(CCACHE ccache)
  if(CCACHE)
    set_property(GLOBAL PROPERTY RULE_LAUNCH_COMPILE ${CCACHE})
    set_property(GLOBAL PROPERTY RULE_LAUNCH_LINK ${CCACHE})
  else()
    message(WARNING "Couldn't find ccache executable")
  endif()
endif()

if(USE_NATIVE)
  set(NATIVE_FLAGS "-march=native")
endif()

########################################
# Dependencies

find_package(PythonInterp 3.4 REQUIRED)
find_package(PkgConfig REQUIRED)

find_package(Boost 1.60 REQUIRED)
find_package(GLEW REQUIRED)
find_package(OpenGL REQUIRED)
find_package(Threads REQUIRED)

pkg_search_module(SDL2 REQUIRED IMPORTED_TARGET sdl2)

add_library(GLM_GLM INTERFACE)
target_include_directories(GLM_GLM INTERFACE "${SLUM_EXTERNAL_DIR}/glm")

add_library(Catch_Catch INTERFACE)
target_include_directories(Catch_Catch INTERFACE "${SLUM_EXTERNAL_DIR}/Catch/single_include")

add_library(Boost_simd INTERFACE)
target_include_directories(Boost_simd INTERFACE "${SLUM_EXTERNAL_DIR}/boost.simd/include")

set(IMGUI_LIBRARY "imgui")
set(IMGUI_INCLUDE_DIR "${SLUM_EXTERNAL_DIR}/imgui")
file(GLOB IMGUI_SOURCES "${IMGUI_INCLUDE_DIR}/*.cpp")
if(CMAKE_CXX_COMPILER_ID STREQUAL "GNU")
  set(IMGUI_FLAGS "-Wno-maybe-uninitialized")
endif()
set_property(SOURCE ${IMGUI_SOURCES}
  PROPERTY COMPILE_FLAGS "${IMGUI_FLAGS} ${NATIVE_FLAGS}")
add_library(${IMGUI_LIBRARY} STATIC ${IMGUI_SOURCES})
target_include_directories(${IMGUI_LIBRARY} PUBLIC ${IMGUI_INCLUDE_DIR})
if(WIN32)
  target_link_libraries(${IMGUI_LIBRARY} imm32)
endif()

set(FASTNOISE_LIBRARY "FastNoise")
set(FASTNOISE_INCLUDE_DIR "${SLUM_EXTERNAL_DIR}/FastNoise")
file(GLOB FASTNOISE_SOURCES "${FASTNOISE_INCLUDE_DIR}/*.cpp")
if(CMAKE_CXX_COMPILER_ID STREQUAL "GNU")
  set(FASTNOISE_FLAGS "-Wno-unused-function -Wno-maybe-uninitialized -Wno-strict-aliasing")
elseif(CMAKE_CXX_COMPILER_ID STREQUAL "Clang")
  set(FASTNOISE_FLAGS "-Wno-unused-function")
endif()
set_property(SOURCE ${FASTNOISE_SOURCES}
  PROPERTY COMPILE_FLAGS "${FASTNOISE_FLAGS} ${NATIVE_FLAGS}")
add_library(${FASTNOISE_LIBRARY} STATIC ${FASTNOISE_SOURCES})
target_include_directories(${FASTNOISE_LIBRARY} PUBLIC ${FASTNOISE_INCLUDE_DIR})

set(FASTNOISESIMD_LIBRARY "FastNoiseSIMD")
set(FASTNOISESIMD_INCLUDE_DIR "${SLUM_EXTERNAL_DIR}/FastNoiseSIMD/FastNoiseSIMD")
file(GLOB FASTNOISESIMD_SOURCES "${FASTNOISESIMD_INCLUDE_DIR}/*.cpp")
list(REMOVE_ITEM FASTNOISESIMD_SOURCES "${FASTNOISESIMD_INCLUDE_DIR}/FastNoiseSIMD_internal.cpp")
if(CMAKE_CXX_COMPILER_ID STREQUAL "GNU")
  set(FASTNOISESIMD_FLAGS "-Wno-unused-function -Wno-maybe-uninitialized -Wno-unused-result")
elseif(CMAKE_CXX_COMPILER_ID STREQUAL "Clang")
  set(FASTNOISESIMD_FLAGS "-Wno-unused-function -Wno-sometimes-uninitialized")
endif()
set_property(SOURCE ${FASTNOISESIMD_SOURCES}
  PROPERTY COMPILE_FLAGS "${FASTNOISESIMD_FLAGS} ${NATIVE_FLAGS}")
set_property(SOURCE "${FASTNOISESIMD_INCLUDE_DIR}/FastNoiseSIMD_avx2.cpp"
  PROPERTY COMPILE_FLAGS "${FASTNOISESIMD_FLAGS} -march=core-avx2")
set_property(SOURCE "${FASTNOISESIMD_INCLUDE_DIR}/FastNoiseSIMD_sse2.cpp"
  PROPERTY COMPILE_FLAGS "${FASTNOISESIMD_FLAGS} -msse2")
set_property(SOURCE "${FASTNOISESIMD_INCLUDE_DIR}/FastNoiseSIMD_sse41.cpp"
  PROPERTY COMPILE_FLAGS "${FASTNOISESIMD_FLAGS} -msse4.1")
add_library(${FASTNOISESIMD_LIBRARY} STATIC ${FASTNOISESIMD_SOURCES})
target_include_directories(${FASTNOISESIMD_LIBRARY} PUBLIC ${FASTNOISESIMD_INCLUDE_DIR})

########################################
# Targets

file(GLOB_RECURSE ALL_SLUM_SOURCES "${SLUM_SOURCE_DIR}/*.cpp" "${SLUM_TEST_DIR}/*.cpp")
set_property(SOURCE ${ALL_SLUM_SOURCES}
  PROPERTY COMPILE_FLAGS "-Wextra -Wno-unused-parameter -Wno-unused-function ${NATIVE_FLAGS}")

set(SLUM_LIBRARY slum)
file(GLOB_RECURSE SLUM_LIBRARY_SOURCES "${SLUM_SOURCE_DIR}/Slum/*.cpp")

set(SLUM_DEMO_CUBE demo-cube)
file(GLOB_RECURSE SLUM_DEMO_CUBE_SOURCES "${SLUM_SOURCE_DIR}/Slum/Demo/Cube/*.cpp")
list(APPEND SLUM_DEMO_CUBE_SOURCES "${SLUM_SOURCE_DIR}/demo_cube.cpp")
list(REMOVE_ITEM SLUM_LIBRARY_SOURCES ${SLUM_DEMO_CUBE_SOURCES})

set(SLUM_DEMO_NOISE demo-noise)
file(GLOB_RECURSE SLUM_DEMO_NOISE_SOURCES "${SLUM_SOURCE_DIR}/Slum/Demo/Noise/*.cpp")
list(APPEND SLUM_DEMO_NOISE_SOURCES "${SLUM_SOURCE_DIR}/demo_noise.cpp")
list(REMOVE_ITEM SLUM_LIBRARY_SOURCES ${SLUM_DEMO_NOISE_SOURCES})

set(SLUM_DEMO_TURTLE demo-turtle)
file(GLOB_RECURSE SLUM_DEMO_TURTLE_SOURCES "${SLUM_SOURCE_DIR}/Slum/Demo/Turtle/*.cpp")
list(APPEND SLUM_DEMO_TURTLE_SOURCES "${SLUM_SOURCE_DIR}/demo_turtle.cpp")
list(REMOVE_ITEM SLUM_LIBRARY_SOURCES ${SLUM_DEMO_TURTLE_SOURCES})

set(SLUM_GL_ENUM_SCRIPT "${SLUM_UTIL_DIR}/generate_gl_enums.py")
set(SLUM_GL_ENUM_OUTPUT "${SLUM_SOURCE_DIR}/Slum/GL/Enums.h")
file(GLOB SLUM_GL_ENUM_FILES "${SLUM_SOURCE_DIR}/Slum/GL/Enum/*.txt")
list(APPEND SLUM_LIBRARY_SOURCES ${SLUM_GL_ENUM_OUTPUT})

add_custom_command(OUTPUT ${SLUM_GL_ENUM_OUTPUT}
  COMMAND ${PYTHON_EXECUTABLE} ${SLUM_GL_ENUM_SCRIPT} ${SLUM_GL_ENUM_FILES} -o ${SLUM_GL_ENUM_OUTPUT}
  DEPENDS ${SLUM_GL_ENUM_FILES})

add_library(${SLUM_LIBRARY} STATIC ${SLUM_LIBRARY_SOURCES})
target_include_directories(${SLUM_LIBRARY} PUBLIC ${SLUM_SOURCE_DIR})
target_link_libraries(${SLUM_LIBRARY}
  ${IMGUI_LIBRARY}
  ${FASTNOISE_LIBRARY}
  ${FASTNOISESIMD_LIBRARY}
  Boost::boost
  Boost_simd
  PkgConfig::SDL2
  GLEW::GLEW
  OpenGL::GL
  Threads::Threads
  GLM_GLM)

add_executable(${SLUM_DEMO_CUBE} ${SLUM_DEMO_CUBE_SOURCES})
target_link_libraries(${SLUM_DEMO_CUBE} ${SLUM_LIBRARY})

add_executable(${SLUM_DEMO_NOISE} ${SLUM_DEMO_NOISE_SOURCES})
target_link_libraries(${SLUM_DEMO_NOISE} ${SLUM_LIBRARY})

add_executable(${SLUM_DEMO_TURTLE} ${SLUM_DEMO_TURTLE_SOURCES})
target_link_libraries(${SLUM_DEMO_TURTLE} ${SLUM_LIBRARY})

install(TARGETS ${SLUM_DEMO_CUBE} ${SLUM_DEMO_NOISE} ${SLUM_DEMO_TURTLE})

########################################
# Tests

set(TEST_SLUM test-slum)
file(GLOB_RECURSE TEST_SLUM_SOURCES "${SLUM_TEST_DIR}/Slum/*.cpp")
list(APPEND TEST_SLUM_SOURCES "${SLUM_TEST_DIR}/test_slum.cpp")

add_executable(${TEST_SLUM} ${TEST_SLUM_SOURCES})
target_include_directories(${TEST_SLUM} PRIVATE ${SLUM_TEST_DIR})
target_link_libraries(${TEST_SLUM} ${SLUM_LIBRARY} Catch_Catch)

set(TEST_NOISE test-noise)
set(TEST_NOISE_SOURCES "${SLUM_TEST_DIR}/test_noise.cpp")

add_executable(${TEST_NOISE} ${TEST_NOISE_SOURCES})
target_link_libraries(${TEST_NOISE} ${SLUM_LIBRARY})

set(BENCH_NOISE bench-noise)
set(BENCH_NOISE_SOURCES "${SLUM_TEST_DIR}/bench_noise.cpp")

add_executable(${BENCH_NOISE} ${BENCH_NOISE_SOURCES})
target_include_directories(${BENCH_NOISE} PRIVATE ${SLUM_TEST_DIR})
target_link_libraries(${BENCH_NOISE} ${SLUM_LIBRARY})
