#!/usr/bin/env bash

PROJECT_DIR=$(dirname $(readlink -f $0))

die() { echo "error: $@"; exit 1; }

########################################

CLEAN=
VERBOSE=
BUILD_TYPE=RelWithDebInfo

for arg in "$@"; do
  case $arg in
    clean) CLEAN=1 ;;
    verbose) VERBOSE=1 ;;
    debug) BUILD_TYPE=Debug ;;
    release) BUILD_TYPE=Release ;;
    reldebug) BUILD_TYPE=RelWithDebInfo ;;
    clang) CXX=clang++ ;;
    asan) ASAN=1 ;;
    lsan) LSAN=1 ;;
    tsan) TSAN=1 ;;
    usan) USAN=1 ;;
    *) die "unknown option '$arg'" ;;
  esac
done

CMAKE_OPTIONS="-DCMAKE_EXPORT_COMPILE_COMMANDS=on"
CMAKE_OPTIONS+=" -DUSE_CCACHE=on"
CMAKE_OPTIONS+=" -DUSE_NATIVE=on"
CMAKE_OPTIONS+=" -DCMAKE_BUILD_TYPE=$BUILD_TYPE"

CMAKE_CXX_FLAGS=
[[ -n $ASAN ]] && CMAKE_CXX_FLAGS+=" -fsanitize=address"
[[ -n $LSAN ]] && CMAKE_CXX_FLAGS+=" -fsanitize=leak"
[[ -n $TSAN ]] && CMAKE_CXX_FLAGS+=" -fsanitize=thread"
[[ -n $USAN ]] && CMAKE_CXX_FLAGS+=" -fsanitize=undefined"
[[ -n $CMAKE_CXX_FLAGS ]] && CMAKE_OPTIONS+=" -DCMAKE_CXX_FLAGS='$CMAKE_CXX_FLAGS'"

NINJA_OPTIONS=
[[ -n $VERBOSE ]] && NINJA_OPTIONS="-v"

export CCACHE_COMPRESS=1
export CXX

########################################

WORK_DIR=build
if [ -n "$MSYSTEM" ]; then
  if [ "$MSYSTEM" = "MINGW32" ]; then
    WORK_DIR=build32
  elif [ "$MSYSTEM" = "MINGW64" ]; then
    WORK_DIR=build64
  else
    die "unsupported MSYSTEM: $MSYSTEM"
  fi
fi

cd $PROJECT_DIR
[[ -n "$CLEAN" ]] && rm -rf $WORK_DIR
mkdir -p $WORK_DIR

script build.log -c "
cd $WORK_DIR
set -ex
cmake .. -G Ninja $CMAKE_OPTIONS
ninja $NINJA_OPTIONS
"
