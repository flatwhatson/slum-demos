((nil
  (indent-tabs-mode . nil))
 (sh-mode
  (sh-basic-offset . 2))
 (c++-mode
  (c-basic-offset . 2)
  (eval . (progn
            (c-set-style "gnu")
            (c-set-offset 'innamespace 0)
            (c-set-offset 'inclass '+)
            (c-set-offset 'inlambda 0)
            (c-set-offset 'access-label -1)
            (c-set-offset 'case-label '+)
            (c-set-offset 'member-init-intro '++)
            (c-set-offset 'inher-intro '++)
            (c-set-offset 'arglist-intro '++)
            (c-set-offset 'arglist-cont-nonempty '++)
            (c-set-offset 'brace-list-intro '+)
            ))))
